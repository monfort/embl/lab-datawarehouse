# based on http://www.jeffknupp.com/blog/2013/08/16/open-sourcing-a-python-project-the-right-way/
from __future__ import print_function
from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import io
import codecs
import os
import sys

import lab_warehouse

here = os.path.abspath(os.path.dirname(__file__))

def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)

long_description = read('README.md')

class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest
        errcode = pytest.main(self.test_args)
        sys.exit(errcode)

setup(
    entry_points={
        'console_scripts': [
            'datawarehouse_manager=lab_warehouse.datawarehouse_manager:main',
             'datawarehouse_admin=lab_warehouse.datawarehouse_admin:main'
        ]
    },
    name='lab_warehouse',
    version=lab_warehouse.__version__,
    url='https://git.embl.de/monfort/lab_warehouse',
    license='NA',
    author='Matthias Monfort\nCharles Girardot',
    maintainer='Matthias Monfort',
    author_email='Matthias Monfort\tmonfort@embl.de\n'
                 'Charles Girardot\tgirardot@embl.de',
    install_requires=[
           'tabulate==0.8.2',
           'terminaltables==3.1.0'
        ],
    #tests_require=[
    #    'pytest-cov',
    #    'coverage<4.0a1',
    #    'cov-core',
    #    'pytest',
    #    'py'
    #    ],
    #cmdclass={'test': PyTest},
    description='Administration package for creating datawarehouse to store '
                'biologically related data. Handles a data warehouse '
                'deployment from scratch, data transfer, accompanied by '
                'appropriate metadata',
    long_description=long_description,
    packages=find_packages(),
    include_package_data=True,
    package_data = {
        'lab_warehouse': ['lab_warehouse/config/main/*.ini',
                          'lab_warehouse/config/metadata/*.txt'],
    },
    platforms='any',
    #test_suite='lab_warehouse.pytest_unit_tests',
    classifiers=[
        'Programming Language :: Python',
        'Development Status :: ' + str(lab_warehouse.__version__),
        'Natural Language :: English',
        'Environment :: File Server Environment',
        'Intended Audience :: Biologists, Wet and Dry lab',
        'License :: NA',
        'Operating System :: UNIX',
        ]
)

import os
import errno
from stat import S_IWUSR, S_IWGRP, S_IWOTH
import tabulate  # WARN may need to install it $> sudo pip install tabulate
import time
import hashlib
import grp
import pwd
import types
import shutil
import functools

try: import simplejson as json
except ImportError as sjsonh_imp_err: import json as json

import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()


def init_logger(lvl, output_json, is_admin=False):
    global logger
    logger = custom_logger.get_logger(__name__, lvl, output_json, is_admin)


##########################
#### CONSTANT VARIABLES ####
##########################

DSFILES=[".DS_Store", "._.DS_Store"]

readme_file_template = "> IMPORTANT:\n Further layers on the fly creation from this point must follow defined rules\n\n" + \
                                      "To classify data in the best way possible variability is introduced in the datawarehouse structure. However " + \
                                      "these variable layers still strictly follow a predefined pattern that ones need to reproduce to keep structure " + \
                                      "consistency.\n" + \
                                      "Patterns are experiment specific, i.e. for ChIP-seq  it is __tissue__/__epitope__/__condition__ whereas for " + \
                                      "CAGE-seq sublevels are __tissue__/__experiment__/__condition|all__.\n\n" + \
                                      "As an example, one will create the subsequent mesoderm/bin/6-8h for ChIP-seq data concerning a mesoderm" + \
                                      "sample with an antibody directed against biniou at 6-8h" + \
                                      "The pipe charachter here ('|') in a pattern indicate several solution for the layer creation." + \
                                      "\n\n" + \
                                      "> AT THIS ENDPOINT, THE EXPECTED PATTERN IS :\n"


##########################
##   USEFUL  FUNCTiONS  ##
##########################

def dump_pretty_json(json_data, indentation=4, sort=True):
    ''' Dump pretty json '''
    return json.dumps(json_data, indent=indentation, sort_keys=sort)


def is_string(something):
    if isinstance(something, types.StringTypes):
        return True
    else:
        return False

# def get_list_product(prototype):
#     '''
#     Returns the product of 2 or more lists

#     prototype is a list of list [[a,b], [c,d]]
#     Following this patter it would return [[a,c], [a,d], [b,c], [b,d]]
#     '''
#     for idx_elem, elem in enumerate(prototype[:]):
#         if is_string(elem):
#             prototype[idx_elem] = [ elem ]
#     for association in product(*prototype):
#         yield association


def get_executing_username():
    return pwd.getpwuid(os.getuid()).pw_name or ""


def grant_user_access(usr='', authorised_groups=[]):
    ''' Check the *usr* username belongs to the list of *authorised_groups*
    '''
    access_granted = False
    if usr:
        user_groups = set(get_user_groups(usr))
        allowed_user = authorised_groups & user_groups
        if allowed_user: #rwefdews
        # dewds
            access_granted = True
            logger.info("[init] granted access for {}.".format(usr))
        elif not allowed_user:
            logger.error("[init] {} does not belong authorised groups [{}]."
                         .format(usr, ", ".join(authorised_groups)))
    return access_granted


def get_user_groups(user=''):
    ''' Return the list of UNIX groups to which *user* belongs to.
    '''
    if not user:
        return []
    # Get *user*'s secondary groups
    group_names = [g.gr_name for g in grp.getgrall() if user in g.gr_mem]
    # Get *user*'s primary groups
    gid = pwd.getpwnam(user).pw_gid
    group_names.append(grp.getgrgid(gid).gr_name)
    return group_names


def check_rights(file_stats, file_owner, file_group, exec_user):
    ''' Check if executing user has write rights on file '''
    usr_w_rights = False
    grp_w_rights = False
    oth_w_rights = False
    logger.debug("exec_user {} -- file_owner :: {}" .format(exec_user, file_owner))
    if exec_user == file_owner:
        usr_w_rights = bool(file_stats & S_IWUSR)
    if file_group in get_user_groups(exec_user):
        grp_w_rights = bool(file_stats & S_IWGRP)
    oth_w_rights = bool(file_stats & S_IWOTH)
    logger.debug("usr_w_rights {}, grp_w_rights {}, oth_w_rights {}"
                 .format(usr_w_rights, grp_w_rights, oth_w_rights))
    logger.debug("(usr_w_rights or grp_w_rights or oth_w_rights) {} ".format(
        (usr_w_rights or grp_w_rights or oth_w_rights)))
    return (usr_w_rights or grp_w_rights or oth_w_rights)


def get_timestamp():
    return time.strftime("%Y.%m.%d-%H.%M.%S")


def get_datestamp():  # machine readable
    return time.strftime("%Y%m%d_%H%M%S")


def get_abs_path(tpath):
    if tpath:
        if not os.path.isabs(tpath):
            return os.path.normpath(os.path.join(os.getcwd(), tpath))
        else:
            return os.path.normpath(tpath)


def norm_abspath(path):
    if not os.path.isabs(path):
        path = os.path.join(os.getcwd(), path)
    return os.path.normpath(path)


def change_mod_rec(path, mode=0o750):
    os.chmod(path, mode)
    for root, dirs, _ in os.walk(path):
        for d in dirs:
            os.chmod(os.path.join(root, d), mode)


# Checksums
#############


def get_hasher():
    # here change the hasher
    return hashlib.md5()


def hashfile(afile, blocksize=65536):
    ''' Block size directly depends on the block size of your filesystem
    to avoid performances issues. Here I have blocks of 65536 octets
    hashfile() taken from
    http://stackoverflow.com/questions/3431825/generating-a-md5-checksum-of-a-file
    '''
    hasher = get_hasher()
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.digest()


def get_checksum(pfile):
    checksum = ''
    # try:
    with open(pfile, 'rb') as f:
        checksum = hashfile(f)
    # except IOError as checksum_ex:
    #     raise
    return checksum


def get_file_lines(file_path):
    lines = []
    try:
        with open(file_path, 'rU') as f:
            lines = f.readlines()
    # except IOError:
    #     with open(file_path, 'r') as f:
    #         lines = f.readlines()
    except UnicodeDecodeError as uc_err:
        raise IOError(uc_err)
    return lines


# @custom_logger.log_it(logger)
def get_info_file_dic(file_path):
    info_file_dic = {}
    try:
        lines = get_file_lines(file_path)
    except IOError as get_if_lines_exc:
        # logger.debug()
        raise get_if_lines_exc
    else:
        ignored_line = 0
        for line in lines:
            try:
                splitted_line = line.rstrip(os.linesep).split('=', 1)
            except Exception as spexc:
                logger.info("Could not process line of {}: {} [{}]"
                            .format(os.path.basename(file_path), line, spexc))
                ignored_line += 1
            else:
                if len(splitted_line) > 1:
                    splitted_line[0] = splitted_line[0].strip().replace(' ', '_')
                    splitted_line[1] = splitted_line[1].strip()
                    info_file_dic[splitted_line[0]] = splitted_line[1]
        if ignored_line > 1:
            logger.warn("Ignored {} line".format(ignored_line) +
                        "{}".format("s" if ignored_line > 2 else "") +
                        "in {}".format(file_path))
    return info_file_dic


def create_dictionary_from_info_file(info_file_path):
    file_dic = {}
    lines = []
    with open(info_file_path, 'r') as info_file:
        lines = info_file.readlines()
    for line in lines:
        infos = line.split('=', 1)
        infos = [x.strip() for x in infos]
        file_dic[infos[0]] = infos[1:]
    return file_dic


def write_sorted_info_file(info_file_path, key_values_info_dic):
    '''
    Write key:values in all_info_dic to the infofile (at info_file_path)
    '''
    sorted_attr_keys = sorted(key_values_info_dic.keys())
    with open(info_file_path, 'w+') as info_file:
        for attr in sorted_attr_keys:
            info_file.write("{0}={1}\n".format(attr, key_values_info_dic[attr]))


def valid_file(path):
    valid = False
    if path:
        exists = os.path.exists(path)
        is_file = os.path.isfile(path)
        if exists and is_file :
            valid = True
            logger.debug("\'{0}\' is a valid file".format(path))
        elif not exists or not is_file:
            logger.warn("\'{0}\' is not a valid file".format(path))
        logger.debug("valid == %s", valid)
    return valid


def validate_abs_file_path(fpath):
    if not os.path.isfile(fpath):
        raise IOError('{0} does not exist.'.format(fpath))


### Better printing
###
from terminaltables import AsciiTable
from terminaltables.terminal_io import terminal_size
from textwrap import wrap

def format_grid_table_2(table, header):
    '''
    TODO Make header a list of tuple with (HeauderElement, Index)
    '''
    #header h_header = ['Filename', 'Attribute', 'Requested', 'Error', 'Details'] # Human readable
    idx_fn = header.index('Filename')
    idx_attr = header.index('Attribute')
    idx_rq = header.index('Requested')
    idx_err = header.index('Error')
    idx_dt = header.index('Details')
    tw, _ = terminal_size()
    # last_idx = len(header)-1
    max_col_w = tw / len(header)
    table.insert(0, header)
    t = AsciiTable(table)
    t.inner_row_border = True

    if t.table_width < tw:
        return t.table
    else: t.table_data = \
        wrap_text_in_table(t.table_data, [idx_dt], max_col_w)
    if t.table_width < tw:
        return t.table
    else:
        t.table_data = wrap_text_in_table(t.table_data, [idx_fn], max_col_w)
    if t.table_width < tw:
        return t.table
    else:
        t.table_data = wrap_text_in_table(t.table_data,
                                          range(idx_rq, idx_err),
                                          max_col_w / 2)
    if t.table_width < tw:
        return t.table
    else:
        t.table_data = wrap_text_in_table(t.table_data, [idx_attr], max_col_w)
    if t.table_width < tw:
        return t.table
    else:
        t.table_data = wrap_text_in_table(t.table_data, [idx_fn], max_col_w)
    return t.table


def wrap_text_in_table(table_data, col_indices=[], max_col_w=20):
    for nrow in range(len(table_data)):
        for i_col in col_indices:
            wrapped = '\n'.join(wrap(table_data[nrow][i_col],
                                     int(max_col_w)))
            table_data[nrow][i_col] = wrapped
    return table_data


def format_grid_table(table, header):
    return tabulate.tabulate(table, header, tablefmt="grid")


def decompose_directory_structure_into_single_path_list(root):
    single_paths = []
    for root_dir, directories, files in os.walk(root):
        is_empty = False if files else True
        if not directories:
            single_paths.append( (is_empty, root_dir) )
    return single_paths

###############
## I/O Stuff ##
###############

# @custom_logger.log_it(logger)
# def make_dir(path, mode = 0o750):
#     '''
#     0777 is the default of the makedirs command
#     For the Datawarehosue itself we will use 0750 to allow user to visit it and copy files
#     For the Clone we set it to 0770 to allow user to make structural modification
#     '''
#     try:
#         os.makedirs(path, mode)
#     except OSError as oserr:
#         if not (oserr.errno == errno.EEXIST): #EEXIST = file/dir exists
#             raise
#     except IOError:
#         raise
#     else:
#         return os.path.isdir(path)


# def mkdir(root, foldernames, mkmode = 0o755):
#     # Could check if "is iterable", but only list and sets are used so far
#     if not isinstance(foldernames, list) and not isinstance(foldernames, set):
#         try:
#             # logger.info("Creating %s", foldernames)
#             os.makedirs(os.path.join(root, foldernames), mkmode)
#         except OSError as err:
#             logger.debug(err)
#         except Exception as mkdir_exc:
#             logger.debug(mkdir_exc)
#     elif isinstance(foldernames, list) or isinstance(foldernames, set):
#         for fd in foldernames: mkdir(root, fd)


def clear_umask(func):
    """Decorator which clears the umask for a method.
    The umask is a permissions mask that gets applied
    whenever new files or folders are created. For I/O methods
    that have a permissions parameter, it is important that the
    umask is cleared prior to execution, otherwise the default
    umask may alter the resulting permissions

    :type func: function

    from https://www.programcreek.com/python/example/278/os.umask
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        # set umask to zero, store old umask
        old_umask = os.umask(0)
        try:
            # execute method payload
            return func(*args, **kwargs)
        finally:
            # set mask back to previous value
            os.umask(old_umask)

    return wrapper

@custom_logger.log_it(logger)
def makedirs(paths, mkmode=0o755):
    '''
    0777 is the default of the makedirs command
    For the Datawarehosue itself we will use 0750 to allow user to visit it and copy files
    For the Clone we set it to 0770 to allow user to make structural modification
    '''

    # Could check if "is iterable", but only list and sets are used so far
    if not isinstance(paths, list) and not isinstance(paths, set):
        makedir(paths, mkmode)
        chmod(paths, mkmode)
    else:
        for p in paths: makedirs(p, mkmode)


@clear_umask
def chmod(paths, mkmode):
    os.chmod(paths, mkmode)


@clear_umask
@custom_logger.log_it(logger)
def makedir(path, mode=0o755):
    try:
        os.makedirs(path, mode)
    except OSError as oserr:
        # EEXIST = file/dir exists. Raise error only if
        # the dir does not already exist
        if not (oserr.errno == errno.EEXIST):
            raise


# @custom_logger.log_it(logger)
def mkdir(path, mode=0o755):
    logger.debug(path)
    try:
        os.mkdir(path, mode)
    except OSError as oserr:
        if not (oserr.errno == errno.EEXIST):
            dirn = os.path.dirname(path)
            basn = os.path.basename(dirn)
            if not os.path.isdir(dirn):
                # logger.debug("path :: {}".format(path))
                # logger.debug("basn :: {}".format(basn))
                logger.error("Attempt at creating {} failed. ".format(path) +
                             "Parent directory '{}' does not exist."
                             .format(basn))

            raise

# It's OK, we handle folders with DS files case by case
# out remove dir function now implement a way to first remove annoying DS files

# def clean_DS_Store_files_from_dirs(root):
#     logger.debug("Cleaning .DS_Store files")
#     for r, _, _ in os.walk(root):
#         rm_DS_Store_files(r)


# @custom_logger.log_it(logger)
# def rm_DS_Store_files(path):
#     try:
#         dir_files = os.listdir(path)
#     except OSError as oserr_exc:
#         raise
#     else:
#         try:
#             if '.DS_Store' in dir_files:
#                 os.remove(os.path.join(path, '.DS_Store'))
#             if '._.DS_Store' in dir_files:
#                 os.remove(os.path.join(path, '._.DS_Store'))
#         except Exception as rm_DS_sexc:
#             logger.debug("Could not remove DS Stores. Error : {0}.".format(rm_DS_sexc))

# TODO Should get rid of that and implement safer function where
# i explicitely remove files before removing dir. rmtree is "dangerous"


def remove_dir_tree_from_root(dir_root):
    shutil.rmtree(dir_root, True)


@custom_logger.log_it(logger)
def is_empty(dir, ignore_ds=True):
    try:
        dir_content = os.listdir(dir)
    except IOError:  # as ioerr:
        raise
    else:
        if ignore_ds:
            dir_content = [
                f for f in dir_content if not f.lower()
                in [lf.lower() for lf in DSFILES]
            ]
        return len(dir_content) == 0


def delete_nested_directories(dir_path, stop_root=""):
    dir_path = os.path.normpath(dir_path)
    stop_root = os.path.normpath(stop_root)
    if stop_root == "":  # safe check
        stop_root = os.path.dirname(dir_path)
    if not (dir_path == stop_root):
        rmdir(dir_path)
        delete_nested_directories(os.path.dirname(dir_path), stop_root)


# @custom_logger.log_it(logger)
def silent_rm_nested_dir(dir_path, stop_root=""):
    dir_path = os.path.normpath(dir_path)
    stop_root = os.path.normpath(stop_root)
    if stop_root == "":  # safe check
        stop_root = os.path.dirname(dir_path)
    if not (dir_path == stop_root):
        silent_rmdir(dir_path)
        silent_rm_nested_dir(os.path.dirname(dir_path), stop_root)

#
# def silent_rm_nested_dir(dir_path):
    # dir_path = os.path.normpath(dir_path)
    # os.removedirs(dir_path)


# @custom_logger.log_it(logger)
def silent_rmdir(dir_path):
    try:
        rm_dsstore_files(dir_path)
        rmdir(dir_path)
    except OSError as sil_rmdir_err:
        if not (sil_rmdir_err.errno == errno.ENOTEMPTY):
            # raise error except if dir is not empty
            print("-------RAISE")
            raise
        elif (sil_rmdir_err.errno == errno.ENOTEMPTY):
            logger.debug("{} is not empty and wil not be removed."
                         .format(dir_path))

# @custom_logger.log_it(logger)
def rmdir(dir_path):
    rm_dsstore_files(dir_path)
    try:
        os.rmdir(dir_path)
    except FileNotFoundError:
        pass


@custom_logger.log_it(logger)
def rm_file(file_path):
    os.remove(file_path)


@custom_logger.log_it(logger)
def silent_rm_file(file_path):
    try:
        os.remove(file_path)
    except OSError as oserr:
        if not (oserr.errno == errno.ENOENT):
            # raise error except if file does not exist
            raise oserr


def rm_dsstore_files(dir_path):
    for dsfile in DSFILES:
        silent_rm_file(os.path.join(dir_path, dsfile))


# @custom_logger.log_it(logger)
def copy_file(src, dest):
    logger.debug("Copy {0} -> {1}".format(src, dest))
    shutil.copy2(src, dest)


def format_variable_layers_string(pattern):
    times = 1
    string_pattern = "\n"
    pattern = list(reversed(pattern))
    while pattern != []:
        p = pattern.pop()
        if isinstance(p, list):
            if p and p[0].startswith('__'):  # variable layer
                for var_l in p:
                    if '|' in var_l:
                        var_l = var_l.split('|')
                        for v in var_l:
                            v = '__' + v.replace("__", "") + '__'
                            string_pattern += "\t" * times + v + '\n'
                    else:
                        string_pattern += "\t" * times + var_l + '\n'
                    times += 1
            elif p and isinstance(p, list):
                for v in p:
                    string_pattern += "\t" * times + v + '\n'
                times += 1
        else:
            string_pattern += "\t" * times + p + "\n"
            times += 1
    return string_pattern


def format_a_list_header(hstring):
    len_string = len(hstring)
    header = "{0}\n".format('#' * len_string)
    header += "{0}\n".format(hstring)
    header += "{0}\n".format('#' * len_string)
    return header


def print_indexed_list(lop, short):
    if isinstance(lop, set):
        lop = list(lop)
    if len(lop) == 0: return "None"
    list_of_path = lop[:]
    list_of_path = sorted(list_of_path)
    indexed_line = ''
    if list_of_path:
        for i, path in enumerate(list_of_path):
            if short and len(list_of_path) - 10 > i > 10:
                continue
            elif short and i == len(list_of_path) - 10:
                indexed_line += "..."
            else:
                indexed_line += "\n{0:<4} - {1}".format(str(i + 1), path)
        indexed_line += "\n"
    return indexed_line


def validate_usr_yes_answer(raw_usr_in):
    if raw_usr_in in ['yes', 'Yes', 'YES', 'y', 'Y',]:
        logger.debug("You said yes.")
        return True
    else:
        logger.debug("You said no.")
        return False


'''
Took convert_st_mode() function from stackoverflow http://stackoverflow.com/questions/10741580/using-pythons-stat-function-to-efficiently-get-owner-group-and-other-permissio
'''

# def bit2int(bit):
#     return int(oct(bit))

# def convert_st_mode(st_mode):
#     '''
#     Transfrom st_mode from os.stat(file) in typical UNIX 3 digit
#     permission format (e.g. 750, 644, etc. )
#     '''
#     bits = (S_IRUSR, S_IWUSR, S_IXUSR, S_IRGRP, S_IWGRP, S_IXGRP,
#             S_IROTH, S_IWOTH, S_IXOTH)
#     mode = "%03d" % sum(int(bool(st_mode & bit)) * bit2int(bit) for bit in bits)
#     return mode
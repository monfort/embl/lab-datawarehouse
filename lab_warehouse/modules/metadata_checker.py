import os
import shutil
import re
import copy

from pkg_resources import Requirement
from pkg_resources import resource_filename  # @UnresolvedImport

import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()


def init_logger(lvl, output_json, is_admin=False):
    global logger
    logger = custom_logger.get_logger(__name__, 'info', output_json, is_admin)
    # logger = custom_logger.get_logger(__name__, lvl, output_json)


# Attribute infos:
#     'update_comment' : 'A file with the same name already exists. A new version should have an update_comment field',
#     'owner_name' : 'The name of the owner of the file should be added to the info file',
#     'organism' : 'The name of the organism associated to file should be added to the info file',
#     'buildversion' : 'The build version of the genome that was used to generate or remap the data file is requested in the info file',
#     'original_buildversion' : 'If the coordinate differs from the orignal that the file was processed for, the originally used build version is requested',
#     'description' : 'Short free text description',
#     'analysis_info' : 'Analysis info is requested',
#     'status' : 'A published flag is requested (published/unpublished) according to the publish state of the data',
#     'pubmed_id' : 'The status has been set as published, the pubmed_id of the publication is requested',
#     'score_type' : 'The data file contains a score, the nature of this score is requested in the info file',
#     'signal_type' : 'The nature of the signal available in the data file is requested in the info file',
#     'normalisation_method' : 'The nature of the method used to normalise the data is requested in the info file',
#     'smoothing_method' : 'The nature of the method used to smooth the data is requested in the info file',
#     'bin_size' : 'A build version is requested in the info file',
#     '_default_error_' : "The value corresponding to this field is missing or invalid."


'''
TODO Move the next parsing function to config parsers
'''


# def check_individual_info_file(file_instance, existing_annotation,
#                                applying_attr, add_all_missing):
#     ''' Check an individual info file dictionary to add missing information.
#     A missing piece of the info file is added as a new key:value pair in the *
#     existing_annotation* which value is extracted from the *file_instance*
#     if it is programmatically identified (i.e. from the hierarchy e.g. the
#     buildversion, the organism), or is the validation regex found in
#     *applying_attr*. The missing information is identified
#     with the *add_all_missing* flag.
#     If True, then non mandatory (mandatory == '0') attributes will be
#     considered as missing,
#     If False only mandatory attributes (mandatory == '1') will be considered
#     as missing.
#     Conditionnaly mandatory attributes (mandatory == 'K=V') will be first
#     added (since it is not possible to discriminate mandatory ones a priori).
#     The check_attr_dependencies() target those to check a posteriori
#     knowing the whole set of needed attribute.
#     '''
#     # Gather information that is present in the info file but not part of the sufficient and minimum information asked
#     # i.e. Extra key=value written by user in the info file but not part of the info file validation policy.
#     extra_info_dic = pop_extra_info(existing_annotation, applying_attr.keys())
#     # double check now that we have all the attributes in our dictionary
#     conditional_attr = pop_conditional_rules(applying_attr)
#     # Conditional attribute have been extracted from applying_attr,
#     # The name of the dictionary is changed to reflect more what it contains
#     # *non conditional* as in mandatory values is '1' or '0'
#     non_conditional_attr = applying_attr
#     # Here add *non conditional* attributes
#     # "add_all_missing" means non mandatory are also added
#     add_missing_info_file_attributes_in_infofile_dic(file_instance,
#                                                      existing_annotation,
#                                                      non_conditional_attr,
#                                                      add_all_missing)
#     # Now that we have added all *non conditional* we double check the conditional rules
#     # Change conditional to mandatory/non mandatory according to k=v already known
#     checked_conditional_attr = check_attr_dependencies(existing_annotation,
#                                                        conditional_attr,
#                                                        non_conditional_attr)
#     # Here add CONDITIONAL attributes based on the conditional_applying_attr
#     add_missing_info_file_attributes_in_infofile_dic(file_instance, existing_annotation,
#                                                      checked_conditional_attr,
#                                                      add_all_missing)
#     # Add the checked conditional attribute back to  the orignal object of all applying attributes
#     non_conditional_attr.update(checked_conditional_attr)
#     applying_attr = non_conditional_attr
#     # Update extra info dic to add all info file k=v. Write all of them to the info file
#     extra_info_dic.update(existing_annotation)
#     all_info_dic = extra_info_dic  # Changing dict name now that it contains all the information
#     return all_info_dic


def refine_metadata(known_metadata, annotation_rules, file_props,
                    add_all_missing):
    ''' Check an individual info file dictionary to add missing information.
    A missing piece of the info file is added as a new key:value pair in the *
    existing_annotation* which value is extracted from the *file_instance*
    if it is programmatically identified (i.e. from the hierarchy e.g. the
    buildversion, the organism), or is the validation regex found in
    *annotation_rules*. The missing information is identified
    with the *add_all_missing* flag.
    If True, then non mandatory (mandatory == '0') attributes will be
    considered as missing,
    If False only mandatory attributes (mandatory == '1') will be considered
    as missing.
    Conditionnaly mandatory attributes (mandatory == 'K=V') will be first
    added (since it is not possible to discriminate mandatory ones a priori).
    The check_attr_dependencies() target those to check a posteriori
    knowing the whole set of needed attribute.
    '''
    # Gather information that is present in the info file but not part of the sufficient and minimum information asked
    # i.e. Extra key=value written by user in the info file but not part of the info file validation policy.

    # extra_meta = {}
    # for attr in known_metadata.keys():
    #     if attr not in attribute_to_validate_keys:
    #         extra_meta[attr] = known_metadata.pop(attr)
    # return extra_meta
    refined_metadata = copy.deepcopy(known_metadata)
    refined_annotation_rules = copy.deepcopy(annotation_rules)

    known_meta = refined_metadata.keys()
    logger.debug("known_meta :::: {}".format(known_meta))
    rules_keys = refined_annotation_rules.keys()
    logger.debug("rules_keys :::: {}".format(rules_keys))
    # Isolate metadata key:value for which there is no defined annotation rule
    # Create a new dict by ***popping*** key:val from known metadata
    # So metadata where annotation rules exist will remain in
    # *refined_metadata*
    logger.debug("refined_metadata :: {}".format(refined_metadata))
    known_metadata_norules = {
        attr: refined_metadata.pop(attr)  # will directly remove it from known
        for attr in [
            attr
            for attr in known_meta
            if attr not in rules_keys
        ]
    }
    logger.debug("refined_metadata after pop :: {}".format(refined_metadata))

    # double check now that we have all the attributes in our dictionary
    cond_rules, non_cond_rules = \
        pop_conditional_rules(refined_annotation_rules)
    logger.debug("cond_rules {}\n non_cond_rules {}".format(cond_rules,
                                                            non_cond_rules))
    # Conditional attribute have been extracted from refined_annotation_rules,
    # The name of the dictionary is changed to reflect more what it contains
    # *non conditional* as in mandatory values is '1' or '0'
    # Here add *non conditional* attributes
    # "add_all_missing" means non mandatory are also added
    add_file_properties_attributes(refined_metadata, non_cond_rules,
                                   file_props, add_all_missing)
    # Now that we have added all *non conditional* we double check the
    # conditional rules. Change conditional to mandatory/non mandatory
    # according to k=v already known
    checked_rules_deps = check_attr_dependencies(refined_metadata,
                                                 cond_rules,
                                                 non_cond_rules)
    # refined_annotation_rules.update(checked_rules_deps)
    # Don't think I need to to it twice anymore. Since I am now working
    # on very specific file properties
    add_file_properties_attributes(refined_metadata, checked_rules_deps,
                                   file_props, add_all_missing)

    refined_metadata.update(known_metadata_norules)
    # Add the checked conditional attribute back to  the orignal object of
    # all applying attributes
    refined_annotation_rules.update(checked_rules_deps)
    # return known_metadata_norules  # Changing dict name now that it contains all the information
    return refined_metadata, refined_annotation_rules


def add_file_properties_attributes(known_metadata, annotation_rules,
                                   file_props, add_all_missing=False):
    ''' Add a file property name and value (or value validation regex) to
    *known_metadata* if it is part of the *annotation_rules* AND is not
    already in *known_metadata* (avoid resetting annotation that would have
    been written by user).
    '''
    known_keys = known_metadata.keys()
    for attr, constraints in annotation_rules.items():
        # not known_metadata.get(attr, "") == constraints.get('validation', "") :
        if attr in known_keys:
            # we keep its value, which might have been written by user
            # except if it's actually the validation regex
            val_kn = known_metadata.get(attr, "")
            val_re = constraints.get('validation', "")
            default_val = (val_kn and val_re) == ""
            eq_reg = (val_kn == val_re)
            if not (eq_reg and not default_val):
                continue
        # default value is the validation regex of attr, if not found
        # in file properties
        prop_val = file_props.get(attr, constraints.get('validation'))
        mandatoriness = constraints.get('mandatory', '0')
        # Add if mandatory attr., conditional attr. or nt mandatory
        # but add_all flag set is True
        add_attr = True                                             \
            if (mandatoriness == '1') or ('=' in mandatoriness)     \
            or (mandatoriness == '0' and add_all_missing is True)   \
            else False

        if add_attr is True:
            known_metadata.update({attr: prop_val})


def check_attr_dependencies(existing_metadata_dict,
                            conditional_attrs_props,
                            non_conditional_attrs_props={}):
    ''' Refine our object of relevant metadata by checking existing annotations
    in *existing_metadata_dict* to see if conditionnaly mandatory attributes
    # apply here.
    For example: The pubmed_id matadata is mandatory if status=published, so
    the *status* key is looked for in the existing annotation and its value
    checked. If the value is found to be *published* the mandatoriness of
    pubmed_id is set to 1 (instead of status=published)
    '''
    # deepcopy because the original dictionary might be altered (i.e. pop key)
    checked_relevant_annot = copy.deepcopy(conditional_attrs_props)
    existing_meta_keys = existing_metadata_dict.keys()
    if not existing_meta_keys:
        # TODO Double check if deep copy is needed here even when we do nothing
        return checked_relevant_annot

    for cond_key, cond_constraints in conditional_attrs_props.items():
        mandatoriness = cond_constraints.get('mandatory', '')
        if '=' in mandatoriness:
            cond_attr, cond_value = mandatoriness.split("=", 1)

            if cond_attr not in existing_meta_keys:
                # The condition attribute is not in the existing info file
                # The associated value cannot be tested. Instead:
                # If the associated attribute is mandatory keep it, else pop it
                # FIXME : what about a chain of conditional attributes i.e.
                # A conditional attribute A that depends on a conditional
                # attribute B With B depending on a 'normal' attribute
                # cond_attr_constraints = \
                    # conditional_attrs_props.get(cond_attr, {})
                mandatory_cond =                 \
                    conditional_attrs_props      \
                    .get(cond_attr, {})          \
                    .get('mandatory', False) == '1'
                if not mandatory_cond:
                    checked_relevant_annot.pop(cond_key)
                continue
                # I know I could use else/elif, but using continue minimises
                # the indentation level of the next bit of code

            # elif cond_attr is in existing_meta_keys
            # Get the actual value indicated for mandatory condition attribute
            existing_attr_value = existing_metadata_dict[cond_attr]
            # Check is this value pass validation
            validation_regex =              \
                non_conditional_attrs_props \
                .get(cond_attr, {})         \
                .get('validation', "")
            logger.debug("validation_regex :: {}".format(validation_regex))
            re_validated = test_regex(cond_attr, validation_regex)
            # If re_validated is True then we have to check if it is equal
            # to cond_value
            if (re_validated is True) and \
                    (cond_value.lower() == existing_attr_value.lower()):
                # If the conditional mandatory attribute value is set to the
                # value for which it becomes mandatory, set it as so.
                checked_relevant_annot[cond_key].update({'mandatory': '1'})
            else:
                # It is popped because the value is validated by regex
                # but different from the condition that makes is mandatory
                checked_relevant_annot.pop(cond_key)
    return checked_relevant_annot


# def format_model_prototype_elements(model_prototype):
#     ''' Iterate on each element of a model prototype dict. For each of these
#     elements, if its a string, replace it by a one element list containing
#     the string at index 0. This is to be able to apply get_lists_product()
#     (using the itertools.product)
#     TODO: WRITE A UNIT TEST
#     '''
#     for p_name, p_val in model_prototype.items():
#         if not isinstance(p_val, list):
#             model_prototype[p_name] = [p_val]
#     return [
#         model_prototype['platform'],
#         model_prototype['experiment_type'],
#         model_prototype['data_type'],
#         model_prototype['format']
#     ]


def check_update_property(needed_annotations, new_file=False):
    ''' Define if an update comment is needed in the annotation. It's needed
    if user tries to update a version of a file that already exists in the
    warehouse
    '''
    if new_file:
        needed_annotations.pop('update_comment', None)
    else:  # file is not new
        # if needed_annotations.has_key('update_comment'):
        needed_annotations['update_comment']['mandatory'] = '1'
    return


def pop_extra_info(known_metadata, attribute_to_validate_keys):
    '''
    Pop extra information from the info file k:v dictionary.
    These extra information does not need to be check for validation, it refers to
    extra key=values indicated by user to give more information but that are not part
    of the validation policy. So we pop it, it is merged back later on.
    '''
    extra_meta = {}
    for attr in known_metadata.keys():
        if attr not in attribute_to_validate_keys:
            extra_meta[attr] = known_metadata.pop(attr)
    return extra_meta


def pop_conditional_rules(annotation_rules):
    non_conditional_rules = copy.deepcopy(annotation_rules)
    conditional_rules = {}
    for attr, attr_info in annotation_rules.items():
        if '=' in attr_info['mandatory']:
            conditional_rules[attr] = non_conditional_rules.pop(attr)
    return conditional_rules, non_conditional_rules


def add_missing_info_file_attributes_in_infofile_dic(file_instance, existing_info_file_dic,
                                                     applying_attributes_properties, add_all_missing_attributes):
    '''
    Add attribute:attribute_value to the info file dictionary correspoding to file_instance
    If the attribute is already present (indicated by user) it is *NOT* modified.
    If the attribute_value cannot be determined programatically (i.e. is not an attribute
    of the file instance like can be e.g. the organism, the buildversion (i.e. every
    piece of information that has been identified using the warehouse path to the file)
    the validation regex corresponding to the attribute is added instead.
    '''
    existing_info_file_dic_keys = existing_info_file_dic.keys()
    for attr, attr_info in applying_attributes_properties.items():
        if attr not in existing_info_file_dic_keys:
            # attr_value is either the known attribute from the instance or the validation regex
            attr_value = str(getattr(file_instance, attr, attr_info['validation']))
            # logger.debug('%s :: %s:', attr, attr_value)
            # logger.debug('attr_info %s', attr_info)
            if attr_info['mandatory'] == '0':
                if add_all_missing_attributes:  # add it
                    existing_info_file_dic.update({attr: attr_value})
            elif attr_info['mandatory'] == '1':
                # mandatory, add it
                existing_info_file_dic.update({attr: attr_value})
            elif attr_info['mandatory'] not in ['0', '1']:
                existing_info_file_dic.update({attr: attr_value})


def get_missing_mandatory_attribute_names_list(applying_attributes_properties, existing_info_file_dic_keys):
    '''
    Get the list of attribute names that are considered as mandatory and still
    missing in the info file
    '''
    missing_mandatory_attribute_names = []
    for attr, attr_constraints in applying_attributes_properties.items():
        attr_mandatory = attr_constraints.get('mandatory', None)
        if (attr_mandatory == '1') and (attr not in existing_info_file_dic_keys):
            missing_mandatory_attribute_names.append(attr)
    logger.debug("missing_mandatory_attribute_names :: ".format(missing_mandatory_attribute_names))
    return missing_mandatory_attribute_names


def get_missing_attr(known_annotation_keys, metadata_rules, mandatory=False):
    ''' Return the keys of a dictionary that contains missing attribute names as keys and their constraint as values if names is not in the *known_annotation_keys* list. The value of the 'mandatory' field in the attr. constraint dict has to be equal to reqbit (string)
    '''
    reqbit = '0' if not mandatory else '1'
    return {
        attr: attr_constraints
        for (attr, attr_constraints) in metadata_rules.items()
        if (attr_constraints.get('mandatory', '0') == reqbit) and
        attr not in known_annotation_keys
    }.keys()


def validate_annotation_with_regex(known_annotation, metadata_rules):
    ''' Compare values of alattributes to the constraints regex.
    Return a dictionary which keys are the attribute names and the values
    are booleans flag equal to True if the validation was successful.
    '''
    re_flagged_annotations = {}
    for attr, attr_value in known_annotation.items():
        attr_constraints = metadata_rules.get(attr, {})
        attr_regex = attr_constraints.get('validation', None)
        # Init as "regex_valid" = False. i.e. By default fail regex validation
        if attr_regex:
            # logger.debug("{:.<20} :: {}".format(attr, attr_regex))
            re_flagged_annotations[attr] = test_regex(attr_regex, attr_value)
        else:  # the attr is in the file but has nothing to do here.
            re_flagged_annotations[attr] = False
    return re_flagged_annotations


def test_regex(regex, attr_value):
    ''' Test the value *attr_value* of a certain attribute against the regular
    expression *regex* (defined and read from the metadata rules file) to see
    if there is a match.

    NB: If the attribute value is *actually* the regex (happen when the
    attr=value couple has been added programatically to the metadata file
    it is not validated.
    '''
    valid = False
    regex = regex.lower()
    attr_value = attr_value.lower()
    logger.debug("regex :: {}".format(regex))
    logger.debug("attr_value :: {}".format(attr_value))
    if not (regex == attr_value):
        # The regex should not be the validated text.
        # (.*) cannot "wrongly" validate "(.*)".
        # Not wrong but we do not want that behavior
        try:
            compiled_regex = re.compile(regex, flags=re.IGNORECASE)
        except re.error as cregex_exc:
            # This will happen for all attribute values that are actual strings
            # No need to raise this error.
            logger.debug("Cannot compile regex from metadata rules file." +
                         " Err: {}".format(cregex_exc))
        else:
            matching_value = compiled_regex.match(attr_value)
            if matching_value:
                valid = True
    return valid


def validate_annotation_with_file_props(known_annotation, file_properties):
    ''' Compare the values of *knwon_annotation* (annotation read from the
    metadata file) with values of *file_properties* (obtained programmatically
    by analysisng the file path) and returns a dictionary with compared
    properties associated with booleans which are True if both annotation
    values are identical
    '''
    logger.debug("known_annotation ::: {}".format(known_annotation))
    logger.debug("file_properties :::: {}".format(file_properties))
    props_flagged_annotations = {}
    for prop, prop_val in file_properties.items():
        annot_val = known_annotation.get(prop)
        corresponding_values = (prop_val.lower() == annot_val.lower())
        logger.debug("{} == {} ? {}".format(prop_val, annot_val, corresponding_values))
        props_flagged_annotations[prop] = corresponding_values
    logger.debug("props_flagged_annotations {} ".format(props_flagged_annotations))
    return props_flagged_annotations


def format_info_file_error_dictionary(applying_attr_dictionary,
                                      existing_info_file_dic,
                                      missing_mandatory_attr_list,
                                      missing_non_mandatory_attr_list,
                                      location_attr_valid,
                                      regex_attr_valid):
    error_dictionary = copy.deepcopy(applying_attr_dictionary)
    for attr in applying_attr_dictionary.keys():
        if attr in missing_mandatory_attr_list:
            error_dictionary[attr].update(
                {
                    'error_type': "Missing",
                    'error_desc': "Requested attribute is missing"
                })
        elif attr in missing_non_mandatory_attr_list:
            # if full_report:
            #     error_dictionary[attr].update(
            #         {
            #             'error_type': "Missing",
            #             'error_desc': "Attribute is missing (not mandatory)"
            #         })
            # else:
            # do not report error about missing non mandatory attribute
            error_dictionary.pop(attr)
            continue

        elif regex_attr_valid.get(attr, True) is not True:
            error_dictionary[attr].update(
                {
                    'error_type': "Regex-Invalid",
                    'error_desc': "Attribute value *{0}* is not valid"
                                  .format(existing_info_file_dic.get(attr, ''))
                })
        elif location_attr_valid.get(attr, True) is not True:
            error_dictionary[attr].update(
                {
                    'error_type': "Location-Invalid",
                    'error_desc': "Attribute value *{0}* does not correspond to file location.".format(existing_info_file_dic[attr])
                })
        else:
            # No associated error, it is popped from the error dictionary
            error_dictionary.pop(attr)
    return error_dictionary


def compare_infofile_attr_objects(all_attr_flagged_dic, initial_dic):
    '''
    Compare to know if an update is needed. Compare first keys to know if some have
    been added. If no differences in keys, compare values of each key to know if some
    have been modified.
    return update = bool [True|False]
    '''
    update = False
    all_attr_flagged_dic_keys = all_attr_flagged_dic.keys()
    initial_dic_keys = initial_dic.keys()
    if not len(all_attr_flagged_dic) == len(initial_dic_keys):
        update = True
        return update
    added_keys = [ k for k in all_attr_flagged_dic_keys if k not in initial_dic_keys ]
    if added_keys:
        update = True
        return update
    elif not added_keys:
        # check if all the values are the same
        bool_identical = [ True if all_attr_flagged_dic[attr]["value"] == initial_dic[attr] else False for attr in all_attr_flagged_dic_keys ]
        if not all(bool_identical):
            update = True
            return update
    return update

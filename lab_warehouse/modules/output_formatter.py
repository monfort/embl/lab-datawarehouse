import sys
import os
import json
import lab_warehouse.modules.utils as utils

# Logger
import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()


def init_logger(lvl, output_json, is_admin=False):
    global logger
    logger = custom_logger.get_logger(__name__, lvl, output_json, is_admin)

##############################
# REPORT ERRORS ABOUT FILES ##
##############################

properties_errors_dic = {
    'location': {
        'id':       'Location',
        'details':  'The file is not located in a folder which follows the ' +
                    'datawarehouse storage rules defined in configuration.'
    },
    'extension': {
        'id':       'Format',
        'details':  'The file extension is not admitted at this location.'
    },
    'update': {
        'id':       'Update',
        'details':  'The file is a new version of an existing one. Allow ' +
                    'updates in command line (see -u)'
    },
    'metadata': {
        'id':       'Metadata',
        'details':  'The info file does not exist.'
    },
    'metadata-complete': {
        'id':       'Metadata Keys',
        'details':  'A mandatory attribute is missing in the associated ' +
                    'info file.'
    },
    'metadata-valid': {
        'id':       'Metadata Values',
        'details':  'An attribute value in the associated info file does ' +
                    'not pass the validation policy.'
    },
    'rights': {
        'id':       'Rights',
        'details':  'The files cannot be removed from clone after ' +
                    'transfer. Change the rights to allow group write ' +
                    'rights or allow transfer without clone removal with ' +
                    'the --no-rm flag'
    },
    'default': {
        'id':       'error',
        'details': 'error'
    }
}


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


def dump_json_to_stdout(python_dict={}):
    ''' Dumps some json with sorted keys to stdout '''
    print(json.dumps(python_dict, indent=4, sort_keys=True, cls=SetEncoder))


def format_files_errors_report(global_errors):
    ''' Format the stdout table report with *format_grid_table()* from utils

    The file error report sums up global errors for all files. It also prints a table row for files with no associated errors.
    '''
    logger.debug(global_errors)
    report = ''
    # Code friendly header
    c_header = \
        [
            'Filename',
            'location',
            'extension',
            'update',
            'rights',
            'metadata',
            'metadata-complete',
            'metadata-valid'
        ]
    h_header =  \
        ['Filename'] + \
        [
            properties_errors_dic[ch_id]['id']
            for ch_id in c_header[1:]
        ]

    files_errors_lines, existing_error_keys = \
        format_std_out_global_report_lines(global_errors, c_header)
    report = \
        "{}\n\n".format("[ transfer-files ] Global Error Report:")\
        + "{}\n".format(utils.format_grid_table(files_errors_lines, h_header))
    report += "\nMore information: (empty cells in = NO ERROR)\n"
    for err_k in sorted(existing_error_keys):
        error_detail = properties_errors_dic.get(err_k, {})
        err_k_id = error_detail.get('id', '')
        err_details = error_detail.get('details', '')
        report += "- {0:.<17}: {1}\n".format(err_k_id, err_details)
    report += "\n"
    return report


def format_std_out_global_report_lines(global_errors, error_line_template):
    '''
    Format list of list as expected by tabulate lib for table generation
    '''
    error_lines = []
    existing_error_keys = set()
    # iterate on case insensitive sorted keys
    for file_path in sorted(global_errors.keys(),
                            key=lambda s: os.path.basename(s).lower()):
        file_errors = global_errors.get(file_path, {})
        file_error_line = [os.path.basename(file_path)]
        for err_key in error_line_template[1:]:
            # logger.debug("error :: {} :: {}"
                         # .format(err_key, file_errors.get(err_key)))
            # if not file_errors.get(err_key, True):
            if file_errors.get(err_key, True):
                file_error_line.append('Error')
                existing_error_keys.add(err_key)
            else:
                file_error_line.append('')
        error_lines.append(file_error_line)
    return error_lines, existing_error_keys


###############################
## INFO FILE REPORT FUNCTION ##
###############################

def format_info_file_error_report(all_info_files_errors):
    '''
    Format an explicit report for each error related to an info file.
    '''
    report = ''
    # Human readable header
    h_header = \
        [
            'Filename',
            'Attribute',
            'Requested',
            'Error',
            'Details'
        ]
    # Code friendly header
    c_header = \
        [
            'info_file_name',
            'attribute_name',
            'mandatory',
            'error_type',
            'error_desc'
        ]

    error_report_lines = \
        format_std_out_info_file_attribute_report_lines(all_info_files_errors,
                                                        c_header)
    report = \
        "{}\n\n".format("[info files] Detailed Info File Error Report")\
        + "{}\n".format(utils.format_grid_table_2(error_report_lines,
                                                  h_header))
    return report


# def format_metadata_table(info_files_info_dict):
def format_metadata_table(files_collec, add_all):
    mandatory_kset = set()
    other_kset = set()
    # all_keys_set = set()
    logger.debug(files_collec[:1])
    for f in files_collec:
        # filter relevant annotation depending opn full report
        f_mandatory_k = []
        f_other_k = []
        for attr, attr_info in f.relevant_annotation.items():
            logger.debug("{}, {}".format(attr, attr_info))
            f_mandatory_k.append(attr)          \
            if (attr_info['mandatory'] == '1')  \
            else f_other_k.append(attr)
        mandatory_kset = mandatory_kset.union(set(f_mandatory_k))
        other_kset = other_kset.union(set(f_other_k))

    logger.debug("mandatory_kset {}".format(mandatory_kset))
    logger.debug("other_kset {}".format(other_kset))
    all_ks = \
        sorted((mandatory_kset.union(other_kset))   \
        if add_all                                  \
        else mandatory_kset)                        \
    # line string template
    tabstring = \
        "{{filename}}\t{allkeys}{linesep}".format(
            allkeys="\t".join(["{{{key}}}"
                              .format(key=key) for key in all_ks]),
            linesep=os.linesep
        )
    # list of lines
    all_lines = [
        tabstring.replace("{", "").replace("}", "")
    ]

    logger.debug(files_collec[:1])
    for f in files_collec:
        relevant = f.relevant_annotation
        logger.debug("fname :: " + str(f.info_src_abspath))
        logger.debug("f.metadata :: " + str(f.metadata))
        logger.debug("relevant :: " + str(relevant))
        missing_keys = [k for k in all_ks if k not in f.metadata.keys()]
        logger.debug("finfo.keys :::: " + str( sorted(set(f.metadata.keys())) ))
        logger.debug("all keys :::::: " + str(sorted(all_ks)))
        logger.debug("missing keys: : " + str(sorted(missing_keys)))
        all_attr = {
            k: "N/A"
            for k in all_ks
            if k not in f.metadata.keys()
        }
        logger.debug(all_attr)
        all_attr.update(f.metadata.items())
        logger.debug(all_attr)
        thisfilestring = tabstring.format(
            filename=f.info_src_abspath,
            linesep=os.linesep,
            **all_attr
        )

        logger.debug(thisfilestring)
        all_lines.append(thisfilestring)
    return all_lines



def format_std_out_info_file_attribute_report_lines(all_info_files_errors,
                                                    annot_error_line_template):
    '''
    '''
    table_formated_err_lines = []
    # iterate on case insensitive sorted keys
    for info_file_path in sorted(all_info_files_errors.keys(),
                                 key=lambda s: os.path.basename(s.lower())):
        infofile_errors = all_info_files_errors[info_file_path]
        if infofile_errors:
            sorted_attribute_keys = sorted(infofile_errors.keys())
            if sorted_attribute_keys:
                # all lines related to the same file in the final table
                info_file_errors_lines = []
                for attr in sorted_attribute_keys:
                    attr_error_line = []  # one line in the final table
                    attr_info = infofile_errors[attr]
                    reqbit = attr_info['mandatory']
                    attr_info['mandatory'] = 'X' if reqbit == '1' else ''
                    # Or just kee the format 'x=z' for attribute dependencies
                    attr_error_line = \
                        [
                            os.path.basename(info_file_path)
                            if not info_file_errors_lines
                            else '', attr
                        ]
                    # add the remaining details of the error
                    attr_error_line += \
                        [
                            attr_info[header_col_id]
                            if header_col_id in attr_info
                            else ''
                            for header_col_id in annot_error_line_template[2:]
                        ]
                    info_file_errors_lines.append(attr_error_line)
                table_formated_err_lines += info_file_errors_lines
        else:  # All OK
            err_line = \
                ['' for i in range(len(annot_error_line_template))]
            err_line[0] = os.path.basename(info_file_path)
            err_line[annot_error_line_template.index('error_type')] = 'None'
            err_line[annot_error_line_template.index('error_desc')] = 'OK'
            table_formated_err_lines += [err_line]
    return table_formated_err_lines


def format_transfer_report(files_collec, root=""):
    report = ""

    transferd = []
    untransferd = []
    for f in files_collec:
        rel_fpath = os.path.relpath(f.data_dst_abspath, root)
        if f.transfered is True:
            transferd.append(rel_fpath)
        if f.transfered is False:
            untransferd.append((rel_fpath, f.reason_str))

    if transferd:
        head_string = \
            "[transfer-files] Transfered files list ({})".format(root)
        transfered_filenames = ["\t... {0}".format(f) for f in transferd]
        report += \
            "{}\n\n".format(head_string) + \
            "{}\n".format(
                utils.print_indexed_list(list(transfered_filenames), False))

    if untransferd:
        head_string = \
            "[transfer-files] The following files from" + \
            " have not been transfered to {}".format(root)
        untransfered_filenames = \
            [
                "\t... [reason: {}] {}".format(reason, f)
                for (f, reason) in untransferd
            ]
        report += \
            "{}\n\n".format(head_string) + \
            "{}\n".format(
                utils.print_indexed_list(list(untransfered_filenames), False))

    return report


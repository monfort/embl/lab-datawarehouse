import os
import logging.handlers
import functools

logging.basicConfig()
__default_lvl = 'INFO'

logLevels_strk = {
    'CRITICAL': 50,
    'ERROR': 40,
    'WARNING': 30,
    'INFO': 20,
    'DEBUG': 10,
    'NOTSET': 0
}

logLevels_intk = {
    int_level: str_level
    for (str_level, int_level) in logLevels_strk.items()
}


def log_it(logger):
    def decorated(f):
        @functools.wraps(f)
        def wrapped(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                if logger:
                    logger.exception(e)
                raise
        return wrapped
    return decorated


def get_default():
    return logging.getLogger("default")


def get_logger(name='', lvl='', output_json=False, is_admin=False):
    if not name: name = "default"
    # create logger
    logger = logging.getLogger(name)
    # set level
    lvl = get_level_from_string(lvl)
    logger.setLevel(lvl)
    logger.propagate = False

    # create formatter
    stdout_formatter = get_custom_log_formatter_stdout()
    file_formatter = get_custom_log_formatter_file()

    # Two types of file handler, one for admin, one for manager
    # Manager is group write OK, admin not
    # In any case, create a file handler
    # create file handler and set level to INFO
    handler_class = None
    log_file_name = ""

    if is_admin is True:
        handler_class = logging.handlers.TimedRotatingFileHandler
        log_file_name = 'datawarehouse_admin.log.txt'
    else:
        handler_class = GroupWriteTimedRotatingFileHandler
        log_file_name = 'datawarehouse_manager.log.txt'


    rfile_handler = handler_class(log_file_name, when='D', backupCount=5)
    rfile_handler.setLevel(logging.INFO)
    rfile_handler.setFormatter(file_formatter)
    logger.addHandler(rfile_handler)

    # Create a stdout handler only if no requested JSON output
    if not output_json:
        # create console handler and set level to level indicated
        # by user in command line
        console_handler = logging.StreamHandler()
        console_handler.setLevel(lvl)
        # add formatter to console_handler
        console_handler.setFormatter(stdout_formatter)
        # add console_handler to logger
        logger.addHandler(console_handler)

    logger.debug("[ Logger level for {0:.<80} set to {1: ^8} ]"
                 .format(name, logLevels_intk[logger.getEffectiveLevel()]))
    return logger


def get_level_from_string(lvl=""):
    if not lvl:
        lvl = logLevels_strk[__default_lvl]
    if lvl:
        if isinstance(lvl, str):
            lvl = lvl.upper()
            lvl = logLevels_strk.get(lvl)
        if lvl not in (list(logLevels_strk.values()) +
                       list(logLevels_intk.values())):
            lvl = logLevels_strk[__default_lvl]
    return lvl


def get_custom_log_formatter_stdout():
    debug_format = "[%(levelname)7s] %(message)s (%(filename)s:%(lineno)s)"
    info_format = "[%(levelname)7s] %(message)s"
    warning_format = "[%(levelname)7s] [%(asctime)s] %(message)s"
    error_format = \
        "[%(levelname)7s] [%(asctime)s] %(message)s"
    # "[%(levelname)7s] %(message)s [%(asctime)s] (%(filename)s:%(lineno)s)"
    return CustomLevelDepFormatter(debug_format, info_format, warning_format,
                                   error_format)


def get_custom_log_formatter_file():
    debug_format = \
        "[%(asctime)s] %(levelname)7s] %(message)s (%(filename)s:%(lineno)s)"
    info_format = "[%(asctime)s] [%(levelname)7s] %(message)s"
    warning_format = "[%(asctime)s] [%(levelname)7s] %(message)s"
    error_format = \
        "[%(asctime)s]  [%(levelname)7s] %(message)s"
    return CustomLevelDepFormatter(debug_format, info_format, warning_format,
                                   error_format)



class GroupWriteTimedRotatingFileHandler(logging.handlers.TimedRotatingFileHandler):
    """ Overwrite the _open method of TimedRotatingFileHandler to create a group
    writable file. This is done by temporarily unseting the process umask to
    002 instead of the default 022 (permission should now be 664 instead of 644)
    Solution from
    https://stackoverflow.com/questions/1407474/does-python-logging-handlers-rotatingfilehandler-allow-creation-of-a-group-writa
    """
    def _open(self):
        old_umask = os.umask(0o002)
        #os.fdopen(os.open('/path/to/file', os.O_WRONLY, 0600))
        rtv = logging.handlers.RotatingFileHandler._open(self)
        os.umask(old_umask)
        return rtv


class CustomLevelDepFormatter(logging.Formatter):
    '''
    Custom formatter that changes according to the level

    For most message we do not want a complex format (e.g. INFO messages)
    But for DEBUG and ERROR we want e.g. the line number in addition of
    the other pieces information
    '''

    # Python 2
    # def __init__(self, dbg_fmt, info_fmt, wrn_fmt, err_fmt,
    #              fmt="%(levelno)s: %(msg)s"):
    #     self.err_fmt = err_fmt
    #     self.dbg_fmt = dbg_fmt
    #     self.info_fmt = info_fmt
    #     self.wrn_fmt = wrn_fmt
    #     time_format = "%Y-%m-%d %H:%M:%S"
    #     logging.Formatter.__init__(self, fmt, time_format)

    def __init__(self, dbg_fmt, info_fmt, wrn_fmt, err_fmt):
        self.dbg_fmt = dbg_fmt
        self.info_fmt = info_fmt
        self.wrn_fmt = wrn_fmt
        self.err_fmt = err_fmt
        dt_fmt = "%Y-%m-%d %H:%M:%S"
        super().__init__(fmt="%(levelno)d: %(msg)s",
                         datefmt=dt_fmt)

    def format(self, record):
        # Save the original format configured by the user
        # when the logger formatter was instantiated
        # format_orig = self._fmt  # python 2
        original_style = self._style  # python 3
        # Replace the original format with one customized by logging level
        if record.levelno == logging.DEBUG:
            # self._fmt = self.dbg_fmt  # python 2
            self._style = logging.PercentStyle(self.dbg_fmt)  # python 3
        elif record.levelno == logging.INFO:
            self._style = logging.PercentStyle(self.info_fmt)
        elif record.levelno == logging.WARNING:
            self._style = logging.PercentStyle(self.wrn_fmt)
        elif record.levelno == logging.ERROR:
            self._style = logging.PercentStyle(self.err_fmt)
        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)
        # Restore the original format configured by the user
        # self._fmt = format_orig  # python 2
        self._style = original_style  # python 3

        return result

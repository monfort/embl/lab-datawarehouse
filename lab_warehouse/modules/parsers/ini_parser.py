import os.path
import configparser as ConfigParser
import copy
import re

from itertools import product
from pkg_resources import Requirement
from pkg_resources import resource_filename  # @UnresolvedImport

import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()


def init_logger(lvl, output_json, is_admin):
    global logger
    logger = custom_logger.get_logger(__name__, lvl, output_json, is_admin=False)

# For metadata rule file
##########################

# def get_metadata_rules_file():
#     metadata_rules = \
#         resource_filename(
#             Requirement.parse("lab_warehouse"),
#             "lab_warehouse/lab_storage_warehouse/config/" +
#             "metadata-rules/warehouse_metadata_infofile.txt"
#         )
#     if not os.path.isfile(metadata_rules):
#         raise IOError("Did not find annotation rules ({0}) does not exists."
#                       .format(metadata_rules))
#     return metadata_rules


def parse_rules(rules_filepath):
    conf_rules = []
    split_conf_rules =[]
    cleaned_split_conf_rules = []
    logger.debug("Reading metadata rules file : ...")
    try:
        with open(rules_filepath, 'rU') as rules_file:
            conf_rules = rules_file.readlines()
    except (OSError, IOError) as r_rule_files:
        logger.warn("Error when reading metadata rules file. Err: {0}"
                    .format(r_rule_files))
    else:
        logger.info("Reading metadata rules file : Done")

        split_conf_rules = [
            re.split('\s+', line.strip())
            for line in conf_rules if line
        ]

        for crule in split_conf_rules:
            cleaned_split_conf_rules.append(
                [elem.strip() for elem in crule]
            )

        logger.debug("Clean meta rules: {}"
                     .format(cleaned_split_conf_rules))
    return cleaned_split_conf_rules




def get_metadata_rules_dictionary(rules_filepath):
    ''' Parse the tab delimited metadata rules file. Expects a header '''
    infofile_rules = {}
    # Exctract filepath from package resources
    # rules_filepath = get_metadata_rules_file()
    # Raw parse file
    conf_table = parse_rules(rules_filepath)
    # Pop header
    hd = conf_table.pop(0)

    expected_hder_kw = \
        set([
                'attribute', 'platform', 'exp_type', 'data_type',
                'file_format', 'mandatory', 'validation'
            ])

    if not expected_hder_kw.issubset(set(hd)):
        raise ValueError(
            "Metadata rules header ({0}) appears to be wrong."
            .format(', '.join(hd))
        )
    else:
        for line in conf_table:
            attr_name = extract_column(hd, 'attribute', line)
            platform = \
                split_strip_cell_content(
                    extract_column(hd, 'platform', line))
            exp_type = \
                split_strip_cell_content(
                    extract_column(hd, 'exp_type', line))
            data_type = \
                split_strip_cell_content(
                    extract_column(hd, 'data_type', line))
            file_format = \
                split_strip_cell_content(
                    extract_column(hd, 'file_format', line))
            # constraints are the other information associated
            # to each rule (i.e. mandatory field, field validation
            # regex...)
            validation_re = \
                split_strip_cell_content(
                    extract_column(hd, 'validation', line))
            is_mandatory = \
                split_strip_cell_content(
                    extract_column(hd, 'mandatory', line))
            constraints = {
                'mandatory': is_mandatory[0],
                'validation': validation_re[0]
            }
            attr_rule_list = \
                [
                    [
                        platform,
                        exp_type,
                        data_type,
                        file_format
                    ],
                    constraints
                ]
            # make a dictionary, with attribute name as key
            infofile_rules.setdefault(attr_name, []).append(attr_rule_list)
    return infofile_rules


def extract_column(header, column_id, metadata_rule_line):
    logger.debug("metadata_rule_line {}".format(metadata_rule_line))
    logger.debug("extract id {}".format(column_id))
    column_value = metadata_rule_line[header.index(column_id)]
    return column_value.strip()


def split_strip_cell_content(cell_content):
    # platform, exp_type, data_type and file_format are splitted
    # into lists when applying to more than one e.g.
    # if attribute applies to platform "sequencing, microarrays"
    # it will become [ sequencing, microarrays ]
    return [
        cell_val.strip()
        for cell_val in cell_content.split(',')
    ]


def get_annotation_attributes(model, meta_rules):
    ''' Read the complete set of *meta_rules* and select the ones that apply specifically for the file described by *model*
    Iterate on *meta_rules* and take the 1st applying one (most stringent)

    model       -- combination of properties describing a file
                    e.g. [sequencing, ChIP-seq, signal, bigwig]
    meta_rules  -- the whole set of rules formated as a dictionary

    Return the dictionary of attribute rules applying for this file
    The format is as so:
        {
            attribute_name1: {
                "mandatory": '1',  or '0'
                "validation": regex used for validation
            },
            attribute_name2 : {...},
            ...
        }
    '''
    annotation_collect = {}
    sorted_attributes = sorted(meta_rules.keys())
    for attr in sorted_attributes:
        # Each rule associated with each of the known attribute
        # to test sequencially
        for r_proto, r_constraints in yield_rule_prototypes(meta_rules[attr]):
            applying_attr_rule = {}
            for rule_desc in yield_rule(r_proto):
                # Test each to select the first rule that apply for our case
                apply_vector = \
                    [
                        True if rule_elem == '*' or rule_elem == model_elem
                        else False
                        for rule_elem, model_elem in zip(rule_desc, model)
                    ]
                if all(apply_vector):
                    # Here we find the first applying rule for our model
                    applying_attr_rule[attr] = copy.deepcopy(r_constraints)
                    break  # stop testing other later rules
            if applying_attr_rule:
                # The rule is added to the collection of apllying rules for that model
                annotation_collect.update(applying_attr_rule)  # add info for the attr to collection
                break  # found applying rule for attribute, go to next attribute in loop
    return annotation_collect


def yield_rule_prototypes(meta_attr_rule_object):
    for rule_proto, rule_constraints in meta_attr_rule_object:
        yield rule_proto, rule_constraints


def yield_rule(r_proto):
    # In case the rule prototype concern more than one platform | technology |
    # datatype | format
    # i.e. a rule that applies to two formats will become two distinct rules
    # e.g. for experiment_type applies for sequencing,microarrays - but not
    # for features
    # becomes two distinct rules, one for each platform
    # 1. experiment_type    sequencing  [...constaints...]
    # 2. experiment_type    microarrays  [...constaints...]
    all_valid_rules = \
        list(get_lists_product(format_rule_prototype_elements(r_proto)))
    for rule in all_valid_rules:
        yield rule


def get_lists_product(prototype):
    '''
    Return all possible combination of a list of list
    e.g.    [ [a], [b], [c], [d,e] ] gives
            [ [a], [b], [c], [d] ] and [ [a], [b], [c], [e]]
    '''
    for association in product(*prototype):
        yield list(association)


def format_rule_prototype_elements(rule):
    ''' Iterate on each element of a rule (each rule is a list).
    For each of these elements, if its a string, replace it by a one element
    list containing the string at index 0. This is to be able to apply
    get_lists_product() (using the itertools.product)
    TODO WRITE A UNIT TEST
    '''
    for rule_idx, rule_elem in enumerate(rule[:]):
        if not isinstance(rule_elem, list):
            rule[rule_idx] = [rule_elem]
    return rule


# For the .ini config files
#############################

DEFAULT_CONFIG_SEPARATOR = ","


# def get_configuration_file_normpath(configuration_file_path):
#     ''' Returns a normalised path to the ini configuration file '''
#     return os.path.abspath(configuration_file_path)    \
#         if not os.path.isabs(configuration_file_path)  \
#         else os.path.normpath(configuration_file_path)


def init_config(configuration_file=""):
    ''' Instantiates ConfigParser object and read the configuration file (either from param or default).
    '''
    ini_config_parser = None
    # configuration_file = get_abs_path(configuration_file)
    # configuration_file = get_configuration_file_normpath(configuration_file)
    ini_config_parser = ConfigParser.SafeConfigParser()
    logger.info("[init] Read configuration {0}"
                .format(configuration_file))
    ini_config_parser.read(configuration_file)
    return ini_config_parser


def get_config_values(section, opt, config_parser):
    opt_value = []
    # opt_value = config_parser.get(section, opt)
    try:
        opt_value = config_parser.get(section, opt)
    # except ConfigParser.NoSectionError as nosex:
        # logger.debug("no section for {}".format(nosex))
        # logger.debug("vars exc {}".format(vars(nosex)))
    except ConfigParser.NoOptionError as nopot:
        if nopot.section != "structure" \
                and not nopot.option.endswith('.origins'):
            raise ValueError
    return opt_value


def get_config_values_as_string(section, opt, config_parser=None):
    ''' Always return the a string - can be an empty string. If opt_value is a list, returns the first element of this list (which) is always a string because it is read from a txt file
    '''
    opt_value = get_config_values(section, opt, config_parser)
    ret_value = ""
    if opt_value:  # check if not empty
        list_values = \
            opt_value.replace(' ', '').split(DEFAULT_CONFIG_SEPARATOR)
        if len(list_values) >= 1:
            ret_value = list_values[0]
    else:
        logger.debug("No configuration value found for {1} at section: {0}"
                     .format(section, opt))
    return ret_value


def get_config_values_as_list(section, opt, config_parser=None):
    ''' Always return a list, can be an empty list '''
    ret_value = []
    opt_value = get_config_values(section, opt, config_parser)
    if opt_value: # check if not empty
        list_values = opt_value.replace(' ','').split(DEFAULT_CONFIG_SEPARATOR)
        ret_value = list_values
    return ret_value

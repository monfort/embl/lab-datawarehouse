import os

import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()


def init_logger(lvl, output_json, is_admin=False):
    global logger
    logger = custom_logger.get_logger(__name__, lvl, output_json, is_admin)


# Globals
###########

# a list of suffixes of files we do not want to consider
INFOFILE_SUFFIX = ['.info']
ABORTED_COPY_SUFFIX = ['_copy']
INDICES_SUFFIX = ['.bai', '.fai']
TEMPLATE_SUFFIXES = [
    "_template.info",
    '_info_file_template.info',
    "_infofile_template.info"
]

# when reccursively searching for datasets in a folder
OTHER_FILES_SUFFIXES = INFOFILE_SUFFIX + ABORTED_COPY_SUFFIX + INDICES_SUFFIX
NON_DATASETS_PREFIXES = ['.']


def walk_and_find_all_files(wlkd_root):
    logger.debug("Walking to directories to find files from {0}."
                 .format(wlkd_root))
    all_files = []
    for root, _, files in os.walk(wlkd_root):
        all_files += [os.path.join(root, filename) for filename in files]
    logger.debug("all_files len {}".format(len(all_files)))
    return all_files


def select_datasets_files_only(file_paths_list=[],
                               non_datasets_prefixes=NON_DATASETS_PREFIXES,
                               non_datasets_suffixes=OTHER_FILES_SUFFIXES):
    file_paths_list = filter_out_files_with_prefix(file_paths_list,
                                                   NON_DATASETS_PREFIXES)
    file_paths_list = filter_out_files_with_suffix(file_paths_list,
                                                   non_datasets_suffixes)
    return file_paths_list


def select_new_datasets_only(file_paths_list, version_dir_suffixes=['.old']):
    return filter_out_files_from_dir_with_suffix(file_paths_list,
                                                 version_dir_suffixes)


def select_files_with_suffix(file_paths_list=[], suffixes=[]):
    ''' select files with suffixes in SUFFIXES '''
    suffixes = tuple([sf.lower() for sf in suffixes])
    return \
        [
            tf for tf in file_paths_list
            if string_ends_with_suffix(tf.lower(), suffixes)
        ]


def filter_out_files_with_suffix(file_paths_list=[], suffixes=[]):
    ''' filter files with suffixes in SUFFIXES '''
    suffixes = tuple([sf.lower() for sf in suffixes])
    return \
        [
            tf for tf in file_paths_list
            if not string_ends_with_suffix(tf.lower(), suffixes)
        ]


def filter_out_files_from_dir_with_suffix(file_paths_list=[], dir_suffixes=[]):
    dir_suffixes = tuple([d_sf.lower() for d_sf in dir_suffixes])
    return \
        [
            pth for pth in file_paths_list
            if not string_ends_with_suffix(os.path.dirname(pth).lower(),
                                           dir_suffixes)
        ]


def filter_out_files_with_prefix(file_paths_list=[], prefixes=[]):
    ''' filter files with prefix in PREFIXES in their file *name* '''
    prefixes = tuple([pf.lower() for pf in prefixes])
    return \
        [
            tf for tf in file_paths_list
            if not string_starts_with_prefix(os.path.basename(tf.lower()),
                                             prefixes)
        ]


def string_ends_with_suffix(ts_string, suffixes=tuple()):
    ''' catch non string exceptions '''
    return ts_string.endswith(suffixes)


def string_starts_with_prefix(ts_string, prefixes=tuple()):
    ''' catch non string exceptions '''
    return ts_string.startswith(prefixes)

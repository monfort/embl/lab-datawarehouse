#!/usr/bin/env python3
import argparse
import os
import sys
import copy

import lab_warehouse.classes.DatawarehouseFile as DatawarehouseFile
import lab_warehouse.classes.DatawarehouseStructure as DatawarehouseStructure
import lab_warehouse.modules.file_hunter as file_hunter
import lab_warehouse.modules.metadata_checker as info_file_checker
import lab_warehouse.modules.output_formatter as output_formatter
import lab_warehouse.modules.utils as utils
import lab_warehouse.modules.parsers.ini_parser as conf_parser


# Logger
##########

import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()


def init_logger(lvl, output_json=False, is_admin=False):
    global logger
    logger = custom_logger.get_logger(__name__, lvl, output_json, is_admin)


def init_logging_modules(lvl, output_json=False, is_admin=False):
    DatawarehouseFile.init_logger(lvl, output_json, is_admin)
    DatawarehouseStructure.init_logger(lvl, output_json, is_admin)
    file_hunter.init_logger(lvl, output_json, is_admin)
    output_formatter.init_logger(lvl, output_json, is_admin)
    info_file_checker.init_logger(lvl, output_json, is_admin)
    conf_parser.init_logger(lvl, output_json, is_admin)
    utils.init_logger(lvl, output_json, is_admin)


def exit_dump(err_str="", err_json={}, json=False):
    logger.error("[init] {}".format(str(err_str)))
    # logger.debug(err_str)
    err_json['ERROR'].update({"DESC": str(err_str)})
    if json: output_formatter.dump_json_to_stdout(err_json)


# Utils
#########

def get_auth_groups(config):
    return set(conf_parser.get_config_values_as_list('main_datawarehouse',
                                                     'authorised_groups',
                                                     config))


def norm_command_line_file(cl_file_path, should_exists=True):
    norm_file_path = utils.norm_abspath(cl_file_path)
    if should_exists is True:
        try:
            utils.validate_abs_file_path(norm_file_path)
        except IOError as read_conf_ioerr:
            # raised to improve msg
            err_str = "{cl_file_path} is not a valid file. {err}" \
                      .format(cl_file_path=cl_file_path, err=read_conf_ioerr)
            raise read_conf_ioerr
    return norm_file_path




def get_metadata_rules(parser_arguments):
    metadata_rules_file = parser_arguments['meta_rules']
    if metadata_rules_file is None:
        err_str = "Metadata rules file not given in command line. See -m."
        raise argparse.ArgumentError(None, err_str)
    else:
        try:
            norm_meta_rules_file = norm_command_line_file(metadata_rules_file)
            meta_rules = \
                conf_parser.get_metadata_rules_dictionary(norm_meta_rules_file)
        except IOError as read_conf_ioerr:
            err_str = "Could not parse configuration. {0}".format(read_conf_ioerr)
    return meta_rules

# Main
########

def check_struct(id_struct, struct):
    logger.info("[check {0}] Searching {0} invalid directories..."
                .format(id_struct))
    empty_invalid = set()
    non_empty_invalid = set()
    for inv_dir in struct.invalid_storage_location:
        all_files = file_hunter.walk_and_find_all_files(inv_dir)
        prog_files = \
            file_hunter.filter_out_files_with_prefix(
                all_files,
                file_hunter.NON_DATASETS_PREFIXES)
        prog_files = \
            file_hunter.filter_out_files_with_suffix(
                prog_files,
                file_hunter.TEMPLATE_SUFFIXES)

        if len(prog_files) > 0:  # not empty
            non_empty_invalid.add(inv_dir)
        else:
            empty_invalid.add(inv_dir)
    return non_empty_invalid, empty_invalid


def warn_struct(id_struct, non_empty_invalid, empty_invalid, json_output):

    struct_json = {}
    json_output['LOG'].update({id_struct: struct_json})
    struct_json.update({"empty_invalid": empty_invalid})
    struct_json.update({"non_empty_invalid": non_empty_invalid})
    struct_json.update({"DESC": ""})

    len_emp = len(empty_invalid)
    len_nonemp = len(non_empty_invalid)
    warn = (len_emp > 0 or len_nonemp > 0)

    if warn is False:
        log_str = "No invalid path in {}: OK".format(id_struct)
        logger.info("[check {}] {}".format(id_struct, log_str))
        struct_json["DESC"] += log_str

    if len_emp > 0:
        warn = True
        log_str = "{} empty invalid storage".format(len_emp) + \
                  " location{} in {}. ".format("s" if len_emp > 0 else "",
                                               id_struct)
        struct_json["DESC"] += log_str
        logger.info("[check {}] {}:\n{}"
                    .format(id_struct, log_str,
                            utils.print_indexed_list(empty_invalid,
                                                     False)))
    if len_nonemp > 0:
        warn = True
        log_str = "{} non empty invalid storage".format(len_nonemp) + \
                  " location{} in {}. ".format("s"if len_nonemp > 0 else "",
                                               id_struct)
        struct_json["DESC"] += log_str
        plog = struct_json.get('DESC', "")
        struct_json.update({'DESC': plog + log_str})
        logger.info("[check {}] {}:\n{}"
                    .format(id_struct, log_str,
                            utils.print_indexed_list(non_empty_invalid,
                                                     False)))


def clean_struct(id_struct, struct, dir_to_clean):
    logger.info("[check {}] Empty invalid storage ".format(id_struct) +
                "locations are about to be removed")
    remaining_dirs = set()
    for d in dir_to_clean:
        rmpath = os.path.join(struct.root, d)
        utils.silent_rm_nested_dir(rmpath, struct.root)
        if os.path.isdir(rmpath): remaining_dirs.add(d)

    len_rem = len(remaining_dirs)
    if len_rem > 0:
        logger.info("[check {}] The following ".format(id_struct) +
                    "directories have not been removed because they are "
                    "not empty:\n{}".format(
                        utils.print_indexed_list(remaining_dirs,
                                                 False)))


def check_clone(warehouse, clone, config, parser_arguments, json_output):
    non_empty_invalid, empty_invalid = check_struct("clone", clone)
    warn_struct("clone", non_empty_invalid, empty_invalid, json_output)
    clean = parser_arguments.get("clean_structure", False)
    if clean is True:
        set_to_clean = non_empty_invalid | empty_invalid
        clean_struct("clone", clone, set_to_clean)
        log_str = "Invalid storage directories have been removed from clone"
        log_dic = json_output['LOG'].get("clone", {})
        exdesc = log_dic.get('DESC')
        log_dic.update({'DESC': exdesc + log_str})
    else:
        if parser_arguments.get("is_admin", False) is False \
                and (non_empty_invalid or empty_invalid):
            logger.info("[check clone] Contact an admin for removing those "
                        "invalid storage locations")


def check_warehouse(warehouse, clone, config, parser_arguments, json_output):
    non_empty_invalid, empty_invalid = check_struct("warehouse", warehouse)
    warn_struct("warehouse", non_empty_invalid, empty_invalid, json_output)
    clean = parser_arguments.get("clean_structure", False)
    if clean is True:
        set_to_clean = non_empty_invalid | empty_invalid
        clean_struct("warehouse", warehouse, set_to_clean)
        log_str = "Invalid storage directories have been removed " \
                  "from warehouse"
        log_dic = json_output['LOG'].get("warehouse", {})
        exdesc = log_dic.get('DESC')
        log_dic.update({'DESC': exdesc + log_str})
    else:
        if parser_arguments.get("is_admin", False) is False \
                and (non_empty_invalid or empty_invalid):
            logger.info("[check warehouse] Contact an admin for removing "
                        "those invalid storage locations")


# List datasets
#################

def list_datasets(warehouse, clone, config, parser_arguments, json_output):
    usrname = parser_arguments.get("unix_name", False)
    admin = parser_arguments.get("is_admin", False)
    list_wh = parser_arguments.get("list_warehouse", False)
    list_clone = parser_arguments.get("list_clone", False)
    # print list_wh, list_clone, (list_wh or list_clone)
    if (list_wh or list_clone) is False:
        raise argparse.ArgumentError(
            None,  # should be argument object
            'Explicitely use "-c" and/or "-w" flag(s) to list ' +
            'data from clone and/or warehouse')

    if list_clone:
        logger.info("[list data] Searching clone...")
        list_clone_data(clone, usrname, admin, json_output)
    # For the warehosue we give no username because they all belong to
    # the admin user. Most likely users want to list all files
    if list_wh:
        logger.info("[list data] Searching warehouse...")
        list_warehouse_data(warehouse, json_output)


def list_warehouse_data(warehouse, json_output):
    ''' List the relative paths of datasets in the warehouse '''
    warehouse_datasets = get_user_file_instances(warehouse.root, "", True)
    warehouse_datasets_path = [
        os.path.relpath(f.data_src_abspath, warehouse.root)
        for f in warehouse_datasets
    ]
    json_output['LOG'].update({"warehouse_datasets": warehouse_datasets_path})

    nfiles = len(warehouse_datasets_path)
    if nfiles < 1:
        log_str = "No file found in warehouse. "
        logger.info("[list data] {}".format(log_str))
    else:
        log_str = "Found {} files in warehouse. ".format(nfiles)
        logger.info("[list data] {}".format(log_str))
        logger.info("[list data] Datasets: {}"
                    .format(utils.print_indexed_list(warehouse_datasets_path,
                                                     False)))
    plog = json_output['LOG'].get('DESC', "")
    json_output['LOG'].update({'DESC': plog + log_str})


def list_clone_data(clone, usrname, is_admin, json_output):
    ''' List the relative paths of datasets in the clone '''
    clone_datasets = get_user_file_instances(clone.root, usrname, True)
    clone_datasets_path = [
        os.path.relpath(f.data_src_abspath, clone.root)
        for f in clone_datasets
    ]
    json_output.update({"clone_datasets": clone_datasets_path})

    nfiles = len(clone_datasets_path)
    if nfiles < 1:
        log_str = "No file found in clone for {}. ".format(usrname)
        logger.info("[list data] {}".format(log_str))
    else:
        log_str = "Found {} files in clone for {}. ".format(nfiles, usrname)
        logger.info("[list data] {}".format(log_str))
        logger.info("[list data] Datasets: {}"
                    .format(utils.print_indexed_list(clone_datasets_path,
                                                     False)))
    plog = json_output['LOG'].get('DESC', "")
    json_output['LOG'].update({'DESC': plog + log_str})


# Templates directories
#########################

def add_template_directories(warehouse, clone, config,
                             parser_arguments, json_output):
    ''' Add template directories to clone.

    Note:
        This function is available in the manager because it is possible for a user to manually rmeove a folder in the clone (they have write rights on them), and the user should therefore be able to rebuild template folders if he removed some by mistake
    '''
    add_conf_tpts = parser_arguments.get("bool_add_conf_repos")
    clone_tmplts = set()
    clone_tmplts = clone.get_template_dirs(add_conf_tpts)
    ndir = len(clone_tmplts)
    if ndir < 1:
        log_str = "No templates directory to create in clone"
        logger.info("[add templates] {}".format(log_str))
        json_output['LOG'].update({'DESC': log_str})
        return
    logger.info("[add templates] Creating {} template director{} in clone"
                .format(ndir, "ies" if ndir > 1 else "y"))
    for d in clone_tmplts:
        utils.makedirs(d, 0o775)
    else:
        log_str = "Added {} clone templates directory".format(ndir)
        json_output['LOG'].update({'DESC': log_str})
        # json_output['LOG']['DESC'] += log_str
        logger.info("[add templates] {}".format(log_str))


def find_formats(platform, exp_type, data_type, config):
    section, options = "", ""
    if platform == 'features':
        section = platform
        options = platform + ".exptypes.formats"
    else:
        section = exp_type
        options = ".".join([platform, exp_type, data_type, "formats"])
    return conf_parser.get_config_values_as_list(section, options, config)


def add_template_files(warehouse, clone, config,
                       parser_arguments, json_output):
    ''' Create info files template in each helper directory in clone. '''
    template_file_suffix = file_hunter.TEMPLATE_SUFFIXES[0]  # "_template.info"

    ntdirs = len(clone.existing_helpers_paths)
    if ntdirs < 1:
        log_str = "No template directories found in clone. Nothing to " + \
                  "do. See 'datawarehouse_admin template-dirs'. " + \
                  "Template files will be added to existing template " + \
                  "directories"
        json_output['LOG'].update({'DESC': log_str})
        logger.warn("[add templates] {}".format(log_str))
        return

    # metadata_rules = conf_parser.get_metadata_rules_dictionary()
    metadata_rules = get_metadata_rules(parser_arguments)
    logger.info("[add templates] Creating metadata files templates")

    for tdir_path in clone.existing_helpers_paths:
        tdir_path_prefix = os.path.join(clone.root, tdir_path)
        # For each path that will welcome templates
        _, _, organism, platform, experiment_type, sublevels, data_type, \
            build_version = \
            DatawarehouseFile.identify_a_path_pattern(config, tdir_path)
        format_extensions = find_formats(platform, experiment_type, data_type,
                                         config)
        # Iterate on each format
        for extens in format_extensions:
            desc_combin = [platform, experiment_type, data_type, extens]
            template_attrs = \
                conf_parser.get_annotation_attributes(desc_combin,
                                                      metadata_rules)
            template_dict = {}
            for attr, attr_constraints in template_attrs.items():
                # create a human readable string for the validation regex
                validation = attr_constraints.get('validation', '')
                validation_human_string = 'validated with re: '+validation+''
                # create a human readable string for the mandatory aspect.
                mandatory = attr_constraints.get('mandatory', '')
                # The actual string if it concerns inter attr dependency
                # A wildcard for mandatory attributes
                # nothing in other cases
                mandatory_str = \
                    '[* when ' + mandatory + ']'          \
                    if len(mandatory.split('=', 1)) > 1 \
                    else '[*]' if mandatory == '1'      \
                    else ''
                attr_templ_val = " {0} ({1})".format(mandatory_str,
                                                     validation_human_string)
                template_dict[attr] = attr_templ_val
            # Modify with known information
            template_dict['buildversion'] = organism
            template_dict['organism'] = build_version

            template_name = extens + template_file_suffix
            template_path = os.path.join(tdir_path_prefix, template_name)
            utils.write_sorted_info_file(template_path, template_dict)
    else:
        log_str = "Created metadata files templates in {} director{}" \
                  .format(ntdirs, "ies" if ntdirs > 1 else "y")
        json_output['LOG'].update({'DESC': log_str})
        logger.info("[add templates] {}".format(log_str))

    if parser_arguments.get('helpme', True):  # default show help
        tmplt_pattern = \
            "attribute_name = [ is mandatory? ] (validation strategy)"
        logger.info("[add templates] Template files lines follow " +
                    "this pattern:\t{0}".format(tmplt_pattern))


def rm_template_files(warehouse, clone, config,
                      parser_arguments, json_output):
    # find all template files,
    existing_clone_helpers_abspaths = clone.get_absolute_paths(clone.path_list)

    template_files_paths = file_hunter.walk_and_find_all_files(clone.root)
    template_files_paths = \
        file_hunter.select_files_with_suffix(template_files_paths,
                                             file_hunter.TEMPLATE_SUFFIXES)

    if parser_arguments.get("remove_all", False) is False:
        template_files_paths = [
            template_path
            for template_path in template_files_paths
            if template_path.startswith(tuple(existing_clone_helpers_abspaths))
        ]
    nfiles = len(template_files_paths)
    if nfiles < 1:
        log_str = "No template file to remove from clone"
        json_output['LOG'].update({'DESC': log_str})
        logger.info("[remove templates] {}".format(log_str))
        return

    logger.info("[remove templates] Removing {} template file{} in clone"
                .format(nfiles, "s" if nfiles > 1 else ""))

    for tmpt_pth in template_files_paths:
        utils.rm_file(tmpt_pth)
    else:
        log_str = "Removed {} templates files from clone".format(nfiles)
        json_output['LOG'].update({'DESC': log_str})
        logger.info("[remove templates] {}".format(log_str))


# Create Metadata Files
#########################


def get_user_file_instances(struct_root, usrname, admin):
    dataset_paths = find_dataset_files(struct_root)
    dataset_collection = declare_files(dataset_paths, struct_root)
    dataset_collection = filter_user_files(dataset_collection, usrname, admin)
    return dataset_collection


def find_dataset_files(from_root):
    dataset_files = []
    dataset_files = file_hunter.walk_and_find_all_files(from_root)
    dataset_files = file_hunter.select_new_datasets_only(dataset_files)
    dataset_files = file_hunter.select_datasets_files_only(dataset_files)
    return dataset_files


def declare_files(dataset_paths, root):
    dataset_collection = []
    for dataset_abspath in dataset_paths:
        if os.path.isfile(dataset_abspath):
            file_instance = \
                DatawarehouseFile.DatawarehouseFile(dataset_abspath, root)
            dataset_collection.append(file_instance)
    return dataset_collection


def filter_user_files(files_collection, username, is_admin):
    ''' Select file instances from *collection* where the owner is *username*
    If no username provided returns an empty list UNLESS *is_admin* is True
    in which case the admin gets all files or only the one from *username*
    '''
    if not username:
        if not is_admin:
            files_collection = []
    elif username:
        files_collection = [f for f in files_collection if f.owner == username]
    return files_collection


def init_files_processing(warehouse, clone, config, parser_arguments):
    ''' Find files and trigger the file check (introspection) '''
    allowed_zip_exts = \
        set(conf_parser.get_config_values_as_list('main_datawarehouse',
                                                  'allowed_zip_extensions',
                                                  config))
    files_collection = get_user_file_instances(
        clone.root,
        parser_arguments.get("unix_name", False),
        parser_arguments.get("is_admin", False)
    )

    exec_user = parser_arguments.get("exec_user", "")
    required_removal = parser_arguments.get("required_removal")
    for f in files_collection:
        f.introspect(allowed_zip_exts, warehouse.root,
                     clone.valid_storage_locations,
                     exec_user, required_removal, config)
    return files_collection


def parse_metadata_input_table(input_table_filepath):
    input_table_metadata = {}
    norm_table_path = norm_command_line_file(input_table_filepath)
    ''' not clean at all, parses a tab delim file '''
    input_table_metadata = {}
    try:
        table_lines = utils.get_file_lines(norm_table_path)
    except IOError as read_table_err:
        logger.warning("Cannot read input metadata table. {err}"
                       .format(err=read_table_err))
    else:
        table_content = []
        for line in table_lines:
            spline = line.split("\t")
            processed_line = [
                elem.strip() for elem in spline
            ]
            # for header
            table_content.append(processed_line)
        header = table_content.pop(0)
        header.pop(0) # pop the filename element
        for table_line in table_content:
            filename = table_line.pop(0)
            infofile_dict = {}
            for (header_colname, row_colvalue) in zip(header, table_line):
                infofile_dict[header_colname] = row_colvalue
            input_table_metadata[filename] = infofile_dict
        logger.debug("------ ", input_table_metadata)
    return input_table_metadata


def get_input_table_metadata(input_table_filepath):
    input_table_metadata = {}
    if input_table_filepath is not None:
        try:
            input_table_metadata = \
                parse_metadata_input_table(input_table_filepath)
        except IOError as i_meta_err:
            logger.warning("Error while parsing input table. {}"
                           .format(i_meta_err))
    return input_table_metadata


def make_output_table_metadata(files_collection,
                               output_table_filepath,
                               all_meta=False):
    if output_table_filepath is not None:
        output_table_metadata = \
            output_formatter.format_metadata_table(files_collection, all_meta)
        # make it a function in output formatter
        try:
            norm_table_path = norm_command_line_file(output_table_filepath,
                                                     should_exists=False)
            with open(norm_table_path, 'w+') as out_table:
                for line in output_table_metadata:
                    out_table.write(line)
        except IOError as out_meta_err:
            logger.warning("Error while creating output metadata table. {}"
                           .format(out_meta_err))


def set_files_metadata(files_collection, metadata_rules):
    for f in files_collection:
        f.metadata = f.read_metadata()
        f.relevant_annotation = f.get_relevant_annotation(metadata_rules)
        f.updated_metadata = copy.deepcopy(f.metadata)
    return


def update_metadata_with_input_table(files_collection,
                                     input_table_metadata,
                                     metadata_rules):
    known_metadata_keys = metadata_rules.keys()
    for f in files_collection:
        # f.read_metadata()
        intput_table_f_metadata = input_table_metadata.get(f.info_src_abspath)
        if intput_table_f_metadata is not None:
            f.add_metadata_from_table(f.updated_metadata, intput_table_f_metadata,
                                      known_metadata_keys)
    return


def set_default_metadata(files_collection, metadata_rules, all_meta=False):
    for f in files_collection:
        f.set_properties_metadata(f.updated_metadata, f.relevant_annotation)
        f.set_default_metadata(f.updated_metadata, f.relevant_annotation,
                               metadata_rules, all_meta)
    return


def cleanup_metadata(files_collection, metadata_rules, all_meta):
    for f in files_collection:
        f.cleanup_metadata(f.updated_metadata, f.relevant_annotation, all_meta)


def write_updated_metadata_files(files_collection):
    for f in files_collection:
        try:
            f.write_metadata_to_disk(f.updated_metadata)
        except IOError as input_table_upd_meta_err:
            logger.warning("Failed updating {fname} metadata with input table "
                           .format(fname=self.self.storage_dir_relpath) +
                           "information. {}".format(input_table_upd_meta_err))
    return


def check_metadata(files_collection):
    for f in files_collection:
        # Users can eventually add all kinds of annotations for which
        # no rules are defined. Annotation elements for which no rule
        # is available are kept, but ingored in the check
        missing_mandatory_meta,     \
        regex_validated_meta,       \
        prop_validated_meta =       \
            f.check_metadata({
                attr: val
                for (attr, val) in f.metadata.items()
                if attr in f.relevant_annotation.keys()
            })

        f.missing_meta      = True if missing_mandatory_meta else False
        f.re_invalid_meta   = not all(list(regex_validated_meta.values()))
        f.loc_incalid_meta  = not all(list(prop_validated_meta.values()))


def gather_metadata_errors(files_collection, all_annot_err):
     for f in files_collection:
        all_annot_err.update({f.info_src_abspath: f.metadata_error_details})


def create_info_files(warehouse, clone, config, parser_arguments, json_output):
    '''
    Creates info files associated with real files stored in the clone.
    It also tries to add to those info files the more information it can i.e. key=values
    that can be identified programtically (from the structure) e.g. buildversion, organism
    Finally, it formats a report about info files, to visually help the user to have a global
    obverview on what key information is missing, what value does not pass the validation
    policy (regex), etc.
    '''
    input_table_metadata = \
        get_input_table_metadata(parser_arguments.get("input_table_path"))

    files_collection = \
        init_files_processing(warehouse, clone, config, parser_arguments)

    metadata_rules = get_metadata_rules(parser_arguments)

    set_files_metadata(files_collection, metadata_rules)

    all_meta = parser_arguments.get("add_all_meta", False)

    # update with table if needed
    logger.debug("input_table_metadata :: {}".format(input_table_metadata))

    update_metadata_with_input_table(files_collection,
                                     input_table_metadata,
                                     metadata_rules)

    set_default_metadata(files_collection, metadata_rules, all_meta)
    cleanup_metadata(files_collection, metadata_rules, all_meta)

    write_updated_metadata_files(files_collection)

    output_table_filepath = parser_arguments.get("output_table_path")
    make_output_table_metadata(files_collection, output_table_filepath, all_meta)

    check_metadata(files_collection)

    all_annot_err = {}
    json_output['LOG'].update({"metadata_errors": all_annot_err})
    gather_metadata_errors(files_collection, all_annot_err)

    info_files_formated_report = \
        output_formatter.format_info_file_error_report(all_annot_err)
    logger.info("{0}".format(info_files_formated_report))


def get_structures_roots(cnf_wroot, cnf_croot, parser_args):
    ''' Prioritize clone/warehouse root from command line if given
    '''
    pars_wroot = utils.get_abs_path(parser_args.get("w_root", ""))
    pars_croot = utils.get_abs_path(parser_args.get("c_root", ""))
    cnf_wroot = utils.get_abs_path(cnf_wroot)
    cnf_croot = utils.get_abs_path(cnf_croot)
    chosen_wroot = pars_wroot if pars_wroot is not None else cnf_wroot
    chosen_croot = pars_croot if pars_croot is not None else cnf_croot
    return                              \
        os.path.normpath(chosen_wroot),   \
        os.path.normpath(chosen_croot)


def roots_are_different(r1, r2):
    if r1 == r2:
        raise ValueError("Warehouse and clone cannot have the same root")


def init_templt_dirs_args_parser(non_admin_subparsers, default_func,
                                 admin=False):
    ''' GENERATE TEMPLATE DIRECTORIES
    Special because not only new arguments need to be added for the admin
    but also the help needs to be updated as well as the default function
    to call
    '''

    add_templt_dirs = argparse.ArgumentParser(add_help=False)

    add_templt_dirs.add_argument(
        '-conf-repos',
        action='store_true',
        dest='bool_add_conf_repos',
        default=False,
        help='Creates all directories defined in configuration ' +
             'and add template directories. i.e. By default is ' +
             'adds template directories *only* in existing directories.'
    )

    if admin is True:
        shelp = \
            '[ADMIN] Create variable sublevels template ' + \
            'directories in clone and/or in warehouse (see "w" ' + \
            'and "c" flags).'

        add_templt_dirs.add_argument(
            '-w',
            '--warehouse',
            action='store_true',
            dest='warehouse_tmplts',
            default=False,
            help='Add variable sublevels template directories in warehouse.'
        )

        add_templt_dirs.add_argument(
            '-c',
            '--clone',
            action='store_true',
            dest='clone_tmplts',
            default=False,
            help='Add variable sublevels template directories in clone.'
        )

    else:
        shelp = 'Create variable sublevels template directories in clone.'

    na_add_templt_dirs = \
        non_admin_subparsers.add_parser(
            'template-dirs',
            parents=[add_templt_dirs],
            help=shelp
        )
    na_add_templt_dirs.set_defaults(call_func=default_func)

def init_non_admin_args_parser():
    arg_parser = argparse.ArgumentParser(description='Datawarehouse Manager')

    ''' GLOBAL COMMAND LINE ARGUMENT (APPLYING TO ALL SUBPARSERS) '''
    arg_parser.add_argument(
        '-c',
        '--configuration',
        metavar="PATH",
        required=True,
        action='store',
        dest='conf',
        help='Path to the configuration file')
    arg_parser.add_argument(
        '-m',
        '--metadata-rules',
        metavar="PATH",
        required=False,
        action='store',
        dest='meta_rules',
        help='Path to the metadata rules file. It is required for ' +
             'transfering files, creating metadata files and templates.')
    arg_parser.add_argument(
        # '-wroot',
        '--warehouse',
        metavar="PATH",
        default="",
        action='store',
        dest='w_root',
        help='Path to warehouse root')
    arg_parser.add_argument(
        # '-croot',
        '--clone',
        metavar="PATH",
        default="",
        action='store',
        dest='c_root',
        help='Path to clone root')
    ''' NON ADMIN FUNCTION ARE GATHERED IN THE FOLLOWING SUBPARSER '''
    non_admin_subparsers = arg_parser.add_subparsers(
        dest='subparser',
        help='------------------------------------------------------' +
             '\nChose a functionnality in the ones proposed below, ' +
             'get help using the -h flag with any of them ' +
             '\n------------------------------------------------------')

    # check_warehouse = non_admin_subparsers.add_parser(
    #     'check-warehouse',
    #     help='Check datawarehouse structure.')

    check_clone_invalid = non_admin_subparsers.add_parser(
        'check-clone',
        help='Check datawarehouse clone structure.')

    check_warehouse_invalid = non_admin_subparsers.add_parser(
        'check-warehouse',
        help='Check datawarehouse warehouse structure.')

    list_data = non_admin_subparsers.add_parser(
        'list',
        help='List file in structures.')

    list_data.add_argument(
        '-w',
        '--warehouse',
        action='store_true',
        dest='list_warehouse',
        default=False,
        help='List files in warehouse.')

    list_data.add_argument(
        '-c',
        '--clone',
        action='store_true',
        dest='list_clone',
        default=False,
        help='List files in clone.')

    create_metadata = non_admin_subparsers.add_parser(
        'create-infofiles',
        help='Create datasets info metadata files')

    create_metadata.add_argument(
        '-a',
        '--add-all',
        action='store_true',
        default=False,
        dest='add_all_meta',
        help='Set this flag to add non mandatory attributes to info ' +
             'files when creating them (not added by default).')

    create_metadata_tables = create_metadata.add_mutually_exclusive_group()

    create_metadata_tables.add_argument(
        '-i',
        '--in-table',
        action='store',
        dest='input_table_path',
        help='Define the path to a tab delimited table that will be ' +
             'used to fill information in all the individual info files ' +
             'created by the program.')

    outtbl_destname = 'output_table_path'
    create_metadata_tables.add_argument(
        '-o',
        '--out-table',
        action='store',
        dest=outtbl_destname,
        help='Define the {} '.format(outtbl_destname.upper()) +
             'path to a tab delimited table to be created. This table ' +
             'maps the programatically created info files attributes ' +
             'with their value or the validation regex and can be ' +
             'used to easily fill many info files by giving it as ' +
             'input to the program (see -i) ! BEWARE AS THIS WILL OVERWRITE' +
             'ANY FILE WITH THE GIVEN PATH !')

    ''' ADD METADATA TEMPLATE FILES IN HELPERS DIRECTORY '''
    add_metadata_files = non_admin_subparsers.add_parser(
        'template-files',
        help='Create clone\'s info file templates in templates leaf ' +
             'directories.')

    remove_metadata_files = \
        non_admin_subparsers.add_parser(
            'rm-template-files',
            help="Delete clone's info file templates (i.e. all files " +
                 "with a '{}' suffix) in template leaf directories."
                 .format(file_hunter.TEMPLATE_SUFFIXES[0]))

    remove_metadata_files.add_argument(
        '-a',
        '--all',
        action='store_true',
        dest='remove_all',
        default=False,
        help='Remove all template metadata files. By default, remove ' +
             'files only when they are in template directories.')

    # check_warehouse.set_defaults(call_func=check_structure)
    check_clone_invalid.set_defaults(call_func=check_clone)
    check_warehouse_invalid.set_defaults(call_func=check_warehouse)
    list_data.set_defaults(call_func=list_datasets)
    create_metadata.set_defaults(call_func=create_info_files)
    add_metadata_files.set_defaults(call_func=add_template_files)
    remove_metadata_files.set_defaults(call_func=rm_template_files)

    return                                                      \
        arg_parser, non_admin_subparsers, check_clone_invalid,  \
        check_warehouse_invalid


# Main
########

def get_config(kwarguments, json_output, json):
    config_file = kwarguments.get("conf")
    logger.debug("config_file {} ".format(config_file))
    try:
        norm_config = norm_command_line_file(config_file)
        config = conf_parser.init_config(config_file)
    except IOError as read_conf_ioerr:
        err_str = "Could not parse configuration. {0}".format(read_conf_ioerr)
        return exit_dump(err_str, json_output, json)
    return config


def main(argv=None):
    # parser_arg = init_non_admin_args_parser()[0]

    parser_arg, non_admin_subparsers, _, _ =    \
        init_non_admin_args_parser()

    init_templt_dirs_args_parser(non_admin_subparsers,
                                 add_template_directories,
                                 admin=False)

    kwarguments = {}
    if (len(sys.argv) < 1): parser_arg.parse_args(['-h'])  # Help that guy!
    lab_warehouse_args = parser_arg.parse_args(sys.argv[1:])
    kwarguments = vars(lab_warehouse_args)
    main_func = kwarguments.pop('call_func', None)

    json = kwarguments.get('json_output', False)
    json_output = {
        "INFO": kwarguments,
        'ERROR': {},
        'WARN': {},
        'LOG': {}
    }

    init_logger('info')
    init_logging_modules('info')

    logger.info("[init] datawarehouse_manager ({0})"
                .format(kwarguments.get("subparser")))

    if main_func is None:
        parser_arg.parse_args(['-h'])  # Help that guy!

    config = get_config(kwarguments, json_output, json)

    user = utils.get_executing_username()
    kwarguments.update({"unix_name": user})
    logger.debug("[init] User: {0}".format(user))
    auth_groups = get_auth_groups(config)
    usr_granted = utils.grant_user_access(user, auth_groups)

    if usr_granted:
        conf_wroot = \
            conf_parser.get_config_values_as_string("main_datawarehouse",
                                                    "root",
                                                    config)
        conf_croot = \
            conf_parser.get_config_values_as_string("main_datawarehouse",
                                                    "clone_root",
                                                    config)
        try:
            warehouse_root, clone_root = get_structures_roots(conf_wroot,
                                                              conf_croot,
                                                              kwarguments)
            roots_are_different(warehouse_root, clone_root)
        except ValueError as valerr_stroots:
            logger.debug("valerr_stroots")
            return exit_dump(valerr_stroots, json_output, json)
        else:
            logger.info("{}: {}".format("warehouse root", warehouse_root))
            logger.info("{:.>14}: {}".format("clone root", clone_root))
    else:
        return exit_dump("{} does not belong to an authorised group."
                         .format(user), json_output, json)

    warehouse = DatawarehouseStructure.DatawarehouseStructure("warehouse",
                                                              warehouse_root,
                                                              config)
    clone = DatawarehouseStructure.DatawarehouseStructure("clone",
                                                          clone_root,
                                                          config)
    try:
        logger.debug("main_func :: {}".format(main_func))
        main_func(warehouse, clone, config, kwarguments, json_output)
    # main_func is kwarguments.pop('call_func', None), that is the
    # default call function associated to the subparser
    except argparse.ArgumentError as main_oserr:
        return exit_dump(main_oserr, json_output, json)
    else:
        if json is True: output_formatter.dump_json_to_stdout(json_output)


if __name__ == "__main__":
    sys.exit(main())

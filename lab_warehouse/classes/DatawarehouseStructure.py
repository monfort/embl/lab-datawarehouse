import os
import re
from itertools import product
import lab_warehouse.modules.utils as utils
import lab_warehouse.modules.parsers.ini_parser as ini_conf_parser

import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()

def init_logger(lvl, output_json, is_admin=False):
    global logger
    logger = custom_logger.get_logger(__name__, lvl, output_json, is_admin)


class DatawarehouseStructure:
    ''' *DatawarehouseStructure* defines the structural properties of the datawarehouse and its clone (as Structures objects). Those structures have attribute like valid and invalid locations, and special locations e.g. like helpers directory (directory with the __dirname__ format) that are valid in the sense that they are meant to be preserved, but invalid as users should not be able to store any file into them.
    '''

    def __init__(self, id_struct, root, conf):
        ''' Initiate a DatawarehouseStructure object '''
        logger.info("[init] Initialising {0: ^9} instance : ...".format(id_struct))
        self.id_struct = id_struct
        self.root = root
        self.root_exists = os.path.isdir(self.root)
        # conf_path_list = paths found when reading structural rules from conf
        self.conf_path_list = set(get_configuration_paths(conf))
        # path_list = **existing** path found when walking through filesystem
        self.path_list = get_pathlist_from_root(root)
        # Identify invalid paths in the existing list of paths.
        self.invalid_storage_location,                  \
            self.existing_valid_invariable_prefixes =   \
            identify_invalid_paths(conf, self.path_list)
        # Get all the helpers/template directories that exist in
        # the configuration.
        self.invariable_prefixes_conf, self.helpers_paths = \
            identify_helpers_paths(self.conf_path_list)
        # Get all the helpers/template directories that exist in the
        # existing structure.
        self.invariable_prefixes, self.existing_helpers_paths = \
            identify_helpers_paths(self.path_list -
                                   self.invalid_storage_location)
        self.invariable_prefixes = \
            self.invariable_prefixes_conf | self.invariable_prefixes
        # Identify all the valid storage locations.
        # That is: structure **leaf** directories
        # that are not invalid (e.g. which can be validated using the
        # structural rules in configuration)
        # that do not contain any **variable sublevel** part
        # (__directory__ names flanked double underscore)
        self.valid_storage_locations = \
            self.path_list - (
                self.invalid_storage_location |
                # Not a full valid leaf directory
                self.invariable_prefixes |
                # Paths in conf *all* contain sublevels pattern
                self.conf_path_list |
                # Helpers/template dirs contain variable sublevels pattern
                self.helpers_paths |
                # Helpers/template dirs contain variable sublevels pattern
                self.existing_helpers_paths
            )

    def ensure_folders_permissions(self, perms=0o755):
        all_paths = set()
        all_paths.add(self.root)
        for p in self.valid_storage_locations:
            splitted_path = p.split(os.sep)
            logger.debug("path :: {}".format(splitted_path))
            previous = ""
            for sp_p in splitted_path:
                current = os.path.join(previous, sp_p)
                current = os.path.normpath(current)
                logger.debug("path :: {}".format(current))
                all_paths.add(os.path.join(self.root, current))
                previous = current
        logger.debug(all_paths)
        for ap in all_paths:
            utils.chmod(ap, perms)


    def create_root_dir(self):
        utils.mkdir(self.root, mode=0o755)
        self.root_exists = os.path.isdir(self.root)

    def get_valid_locations(self):
        return self.valid_storage_locations

    def get_invalid_locations(self):
        return self.invalid_storage_location

    def get_helper_patterns(self):
        return self.helpers_paths

    def get_existing_helper_patterns(self):
        return self.existing_helpers_paths

    def get_absolute_paths(self, paths_list):
        # try:
        return [
            os.path.join(self.root, p)
            for p in paths_list
        ]

    def __str__(self):
        str_obj = "{} ".format(self.id_struct)
        str_obj += "contains the following valid locations : "
        + "{}".format(
            utils.print_indexed_list_of_path(
                self.valid_storage_locations, False))
        str_obj += "and the following invalid locations : "
        + "{}".format(
            utils.print_indexed_list_of_path(
                self.invalid_storage_location, True))

        logger.debug(
            "{0} invalid_locations :\n".format(self.id_struct) +
            "{1}".format(
                utils.print_indexed_list_of_path(
                    self.invalid_storage_location, True)))

    def get_template_dirs(self, add_conf_path=False):
        ''' Get template directories (paths containing subdirectories with at least one double-underscore flanked template layer i.e. __layer_name__)

        Args:
            add_conf_path (bool): if set to True, the returned set will contains paths for which the ivariable path prefix does not exists. In the contrary, if set to False, the returned set will only contain path where the invariable path prefix exists

        Returns:
            (set) A set containing relevant template directories paths

        '''
        template_dirs = set()
        for path in self.helpers_paths:
            templt_pattern = split_helper_path(path)
            if templt_pattern and (len(templt_pattern) > 1):
                inv_prfx = templt_pattern[0]
                if add_conf_path is True:
                    template_dirs.add(os.path.join(self.root, path))
                else:  # User is only interested in adding template
                    # dirs in existing roots (invariable path prefixes)
                    inv_prfx_path = os.path.join(self.root, inv_prfx)
                    if os.path.isdir(inv_prfx_path):
                        # if root exists, adds original path to path set
                        template_dirs.add(os.path.join(self.root, path))
        return template_dirs

    def get_template_root_subdir(self):
        ''' Get template directories invariable prefix subdir, that is the path to the firsts template directories contained in a invariable prfix path (if the path is a/b/c/__d__/__e__/f this function will extract the a/b/c/__d__ part of this path)

        Returns:
            (set) A set containing relevant template subdirs

        '''
        tmplt_dir_to_rm = set()
        for path in self.helpers_paths:
            templt_pattern = split_helper_path(path)
            if templt_pattern and len(templt_pattern) >= 2:
                templt_root = os.path.join(templt_pattern[0],
                                           templt_pattern[1])
                # templt_root differs from path. path is a/b/c/__d__/__e__/f
                # whre templt_root is a/b/c/__d__ (invariable prefix and one
                # subdirectory)
                tmplt_dir_to_rm.add(os.path.join(self.root, templt_root))
        return tmplt_dir_to_rm

    def get_conf_locations(self, whelp=False):
        ''' Get all paths that were extracted form configuration file.
        If the boolean whelp (helpers path are needed) is True, returns the
        full paths, in other cases, just return the invariable prefix of
        helpers path (up to the first variable layer [i.e layer containing
        underscores __helper__] in structure)
        '''
        # if whelp:
        helpers = set(self.conf_path_list)
        # else:
        logger.debug("helpers ::::::::::: {}".format(helpers))
        return helpers

    def set_permissions_from_root_recursive(self, pmode=0o750):
        try:
            logger.info("Setting {0} permission to {1} : ..."
                        .format(self.id_struct, pmode))
            os.chmod(self.root, pmode)
            for root, dirs, _ in os.walk(self.root):
                for d in dirs:
                    os.chmod(os.path.join(root, d), pmode)
        except Exception as chmod_exc:
            logger.warn(
                "Could not change permissions of {0}. Err: {1}"
                .format(self.root, chmod_exc))
        else:
            logger.info(
                "Setting {0} permission to {1} : Done"
                .format(self.id_struct, pmode))

    def get_valid_relpaths(self, valid_locs, inv_prfxs,
                           add_helps, help_pths, add_cnfs):
        # It is used only to "copy" valid folder to the clone. Those valid
        # folders would have been created. By the user/admin in the warehouse
        # itself!
        relpth_all = set()
        relpth_sets = [valid_locs]

        if add_cnfs is True and add_helps is True:
            # add all
            relpth_sets.append(self.conf_path_list)
            relpth_sets.append(help_pths)
        if add_cnfs is False and add_helps is True:
            # add only helpers in existing folder
            only_hexisting = {
                p
                for p in self.conf_path_list
                if p.startswith(tuple(inv_prfxs))
            }
            logger.debug("help_pths :: {}".format(help_pths))
            logger.debug("only_hexisting :: {}".format(only_hexisting))
            relpth_sets.append(only_hexisting)
        if add_cnfs is True and add_helps is False:
            relpth_sets.append({
                invariable_help_path
                for invariable_help_path in
                [
                    get_invariable_part_from_path(conf_path)
                    for conf_path in self.conf_path_list
                ]
                if invariable_help_path
            })

        # return the union of the needed sets of paths
        for pset in relpth_sets: relpth_all = relpth_all.union(pset)
        logger.debug("len relpth_all :: " + str(len(relpth_all)))
        return relpth_all



def get_root_abspaths(st_root, relpth_all):
    return [
        os.sep.join(map(str, all_relpth))
        for all_relpth in product([st_root], relpth_all)
    ]


# Path identifier
###################

def get_invariable_part_from_path(complete_conf_path=''):
    ''' Returns the non variable part of a path (path prefix not containing sublevels). For example: a/b/c/__d__/__e__/__f__ will return a/b/c/
    If no variable layer is found in path, the returned *invariable_path* is empty
    '''
    invariable_path = []
    match_helper_pattern = split_helper_path(complete_conf_path)
    if match_helper_pattern and len(match_helper_pattern) > 1:
        invariable_path = match_helper_pattern[0]
    return invariable_path


def split_helper_path(path):
    '''Split a path if it contains double underscores'''
    match_helper_pattern = re.split(r"(__.+?__)", path)
    if match_helper_pattern:
        return match_helper_pattern
    else:
        return []


def identify_helpers_paths(paths_list):
    ''' Identify paths containing at least one layer with the name flanked by double underscores. Those are identified as helpers (or template) directories. They are here to help the user making the warehouse structure evolving according to the structural rules in configuration. The template directories leafs also contain the info file templates.
    '''
    paths_invariable_prefixes = set()
    helpers_paths = set()
    for path in paths_list:
        invariable_path_prefix = get_invariable_part_from_path(path)
        if invariable_path_prefix:
            paths_invariable_prefixes.add(invariable_path_prefix)
            helpers_paths.add(path)
    return paths_invariable_prefixes, helpers_paths


def check_path_repository(path_to_check, datawarehouse_repositories):
    ''' Check if the repository of a path is valid '''
    regex_main_repository = make_regex_with_list(datawarehouse_repositories)
    validated_bool, valid_beginning, repository, end_part = \
        validate_invariable_layers_against_conf(path_to_check,
                                                regex_main_repository,
                                                None)
    return validated_bool, valid_beginning, repository, end_part


def check_path_organism(path_to_check, all_organisms, valid_beginning):
    ''' Check if the organism of a path is valid. '''
    regex_all_organisms = make_regex_with_list(all_organisms)
    validated_bool, valid_beginning, organism, end_part = \
        validate_invariable_layers_against_conf(path_to_check,
                                                regex_all_organisms,
                                                valid_beginning)
    return validated_bool, valid_beginning, organism, end_part


def check_path_platform(path_to_check, all_platform, valid_beginning):
    ''' Check if the platform of a path is valid.
    NB: "Features" are treated as a platform.
    '''
    regex_all_platform = make_regex_with_list(all_platform)
    validated_bool, valid_beginning, platform, end_part = \
        validate_invariable_layers_against_conf(path_to_check,
                                                regex_all_platform,
                                                valid_beginning)
    return validated_bool, valid_beginning, platform, end_part


def check_path_experiment_type(conf_parser, path_to_check, platform,
                               valid_beginning):
    ''' Check if the experiment type of a path is valid.
    NB: "Features types" (e.g. crms...) are treated as "experiment types".
    '''
    associated_exptypes = \
        get_associated_experiment_types_from_conf(conf_parser, platform)
    regex_associated_exptypes = make_regex_with_list(associated_exptypes)
    validated_bool, valid_beginning, experiment_type, end_part = \
        validate_invariable_layers_against_conf(path_to_check,
                                                regex_associated_exptypes,
                                                valid_beginning)
    return validated_bool, valid_beginning, experiment_type, end_part


def check_path_sublevels(conf_parser, path_to_check, all_buildversions,
                         platform, experiment_type, valid_beginning):
    ''' Check if the sublevels of a path are valid. '''
    associated_sublevels = \
        get_associated_sublevels_from_conf(conf_parser, platform,
                                           experiment_type)
    associated_datatypes = \
        get_associated_datatypes_from_conf(conf_parser, platform,
                                           experiment_type)
    if conf_parser.has_section(platform) and not associated_datatypes:
        next_dir = all_buildversions
    elif not conf_parser.has_section(platform):
        next_dir = associated_datatypes

    validated_bool, valid_beginning, sublevels, end_part = \
        validate_sublevel_pattern_against_conf(path_to_check,
                                               associated_sublevels,
                                               next_dir,
                                               valid_beginning)
    return validated_bool, valid_beginning, sublevels, end_part


def check_path_datatype(conf_parser, path_to_check,
                        platform, experiment_type, valid_beginning):
    associated_datatypes = \
        get_associated_datatypes_from_conf(conf_parser, platform,
                                           experiment_type)
    regex_associated_datatypes = make_regex_with_list(associated_datatypes)
    validated_bool, valid_beginning, datatype, end_part = \
        validate_invariable_layers_against_conf(path_to_check,
                                                regex_associated_datatypes,
                                                valid_beginning)
    return validated_bool, valid_beginning, datatype, end_part


def check_path_buildversion(path_to_check, all_buildversions,
                            valid_beginning):
    ''' Check if the buildversion of a path is valid. '''
    all_buildversions = \
        [
            str(bv) + '$'  # BUGFIX the dollar sign is important
            if bv and not str(bv).endswith('$') else str(bv)
            for bv in all_buildversions
        ]
    regex_all_buildversions = make_regex_with_list(all_buildversions)
    expected_end = True
    validated_bool, valid_beginning, buildversion, end_part = \
        validate_invariable_layers_against_conf(path_to_check,
                                                regex_all_buildversions,
                                                valid_beginning,
                                                expected_end,
                                                all_buildversions)
    return validated_bool, valid_beginning, buildversion, end_part


def make_regex_with_list(a_list):
    '''
    Use a list of value (list of keywords) to create a regex e.g. (kw1|kw2|kw3)
    It is used to validate layers. Each layer as a list of accepted keywords defined in configuration e.g. the platform layer can be either sequencing|microarrays|features. The regex is compiled here and return.

    Several parts are extracted:

        - <begining> The beginning of the path up to the layer we want to identify. We keep the beginning in memory and compare it to the beginning isolated with this regex

        - <tested_for_validation> The regex formated list of value we extracted from configuration

        - <end> The end of the path, that is kept and send later to the next function validating the next layer

    '''
    try:
        piped_list = '|'.join(a_list)
    except TypeError:  # as typerr_exc:
        return False
    else:
        complex_regex_string =                              \
            "(?P<begining>.*)" +                            \
            "({0}|)(?P<tested_for_validation>{1})({0}|)"    \
            .format(os.sep, piped_list) +                   \
            "(?P<end>.*)"
        return re.compile(complex_regex_string)


def strip_path(path):
    if path:
        path = path.strip(os.sep)
    return path


def validate_invariable_layers_against_conf(path_checked, regex,
                                            validated_beginning="",
                                            expected_end=False,
                                            authorized_buildversion=[]):
    ''' Validation of layers where we know the available list of keyword extracted from configuration. It uses the previously compiled regex. As parameter we have a "valid_beginning" which comes from previous call to the function. Once a layer is validated by regex, we update this validated beginning adding to it the validated layer. The last layer to be validated is the buildversion. When buildversion layer is tested, "expected_end" is set to True and we provide a list of authorised buildversion (all of them, exctracted from conf)
    '''
    try:
        regex_match_object = regex.search(path_checked)
    except AttributeError as search_exc:
        return False, '', '', ''
    else:
        validated_bool = False
        validated_sub_layer, end_path_part = '', ''
        if regex_match_object:
            regex_beginning = \
                regex_match_object.group('begining').rstrip(os.sep)
            validated_sub_layer = \
                regex_match_object.group('tested_for_validation')
            end_path_part = regex_match_object.group('end')
            if validated_beginning or validated_beginning is None:
                if validated_beginning is not None:
                    if validated_beginning == regex_beginning:
                        if not end_path_part:
                            if expected_end:
                                # Validating the last layer
                                # end_path_part set to None
                                # Used later to know if a path has been
                                # fully validated
                                end_path_part = None
                                validated_bool = True
                        elif end_path_part:
                            # end_path_part is not null and it is not
                            # the end of the validation process
                            if not expected_end:
                                validated_bool = True
                    validated_beginning = \
                        os.path.join(regex_beginning, validated_sub_layer)
                elif validated_beginning is None:
                    # Testing the first layer (repositories)
                    validated_bool = True  # double check of the truth
                    validated_beginning = \
                        os.path.join(regex_beginning, validated_sub_layer)
        return                                  \
            validated_bool,                     \
            strip_path(validated_beginning),    \
            strip_path(validated_sub_layer),    \
            strip_path(end_path_part)


def validate_sublevel_pattern_against_conf(path_checked, sublvl_pattern,
                                           next_dir, valid_beginning=""):
    ''' Validation of sublevels layers

    Sublevels are validated by checking the length is equal to the length of one of the patterns from configuration and that it does not contain a layer which contains double underscores

    Args:
        valid_beginning (str): path containing all the valid sublayers so far
        sublvl_pattern (str): pattern of sublevels extracted from configuration. Contains all the sublevels possibilities for a specific combination (platform, experiment type)
        next_dir (str): Either all buildversion (used when the platform == "features") or all data types (when platform == microarrays or platform == sequencing).

    Returns:
        (set) A set containing relevant template directories paths
    '''
    sublvl_pattern = list(sublvl_pattern)
    authorized_sublevel_length = \
        set(
            [
                len(sublevels_pattern.rstrip(os.sep).split(os.sep))
                for sublevels_pattern in sublvl_pattern
            ])
    validated_sublevels = ''
    validated_bool = False
    end_path_part = ''
    sublevel_validation_regex = \
        "(?P<begining>{})".format(valid_beginning) +                    \
        "{}(?P<matching_sublevels>.*)".format(os.sep) +                 \
        "{0}(?P<next_expected>{1})({0}|)".format(
            os.sep, "|".join(next_dir)) +                               \
        "(?P<end_path_part>.*)"

    sublevel_validation_compiled_regex = re.compile(sublevel_validation_regex)
    if sublevel_validation_compiled_regex:
        matching_sublevel_pattern = \
            sublevel_validation_compiled_regex.search(path_checked)
        if matching_sublevel_pattern:
            matching_begining = \
                matching_sublevel_pattern.group('begining').strip(os.sep)

            if matching_begining == valid_beginning:
                matching_sublevels = \
                    matching_sublevel_pattern.group('matching_sublevels')
                has_2undsc = get_invariable_part_from_path(matching_sublevels)
                if len(matching_sublevels.split(os.sep))    \
                        in authorized_sublevel_length       \
                        and not has_2undsc:
                    validated_sublevels = matching_sublevels
                    valid_beginning = \
                        os.path.join(matching_begining, validated_sublevels)
                    next_expected = \
                        matching_sublevel_pattern.group('next_expected')
                    end_path_part = \
                        matching_sublevel_pattern.group('end_path_part')
                    end_path_part = os.path.join(next_expected, end_path_part)
                    validated_bool = True
    return \
        validated_bool,                     \
        strip_path(valid_beginning),        \
        strip_path(validated_sublevels),    \
        strip_path(end_path_part)


def is_invalid_path(path, valid_bool, end_part):
    ''' Analyse what the checking functions return to discriminate invalid paths from others
    '''
    is_invalid = False
    if end_part is None:
        # If end part is None: the path has been fully validated
        # We check if the last layer has been validated
        if not valid_bool:
            is_invalid = True
    elif end_part is not None:
        # If the layer is not valid, the path is directly added
        # to the invalid set
        if not end_part and not valid_bool:
            is_invalid = True
    elif not valid_bool:
        is_invalid = True
    return is_invalid


def identify_invalid_paths(conf, list_of_paths_to_validate):
    ''' Identifies paths that do not follow an expected pattern according to configuration file.
    Each part (or layer) is individually tested to check if it's value is equal to one of the solution available in the configuration, e.g. the first layer (repository) is checked and expected to be equal to one of the repository of the repository list in configuration.
    For each layer, the value is tested taking into account the values of parent layers, i.e. if the data type is 'sequencing' then the experiment type layer is tested against a list of sequencing experiments (that is, exluding microarrays and other type of experiment)

    Returns a set containing all path identified as invalid
    '''
    datawarehouse_repositories = get_repositories_from_conf(conf)
    all_organisms = get_all_organisms_from_conf(conf)
    all_platform = get_all_platforms_from_conf(conf)
    all_buildversions = get_all_buildversions_from_conf(conf)
    invalid_paths_set = set()
    valid_invariable_layers = set()
    for path_to_check in list_of_paths_to_validate:
        path_to_check = os.path.normpath(path_to_check)
        # check repository
        regex_valid, valid_beginning, repository, end_part = \
            check_path_repository(path_to_check, datawarehouse_repositories)
        is_invalid = is_invalid_path(path_to_check, regex_valid, end_part)
        if is_invalid:
            invalid_paths_set.add(path_to_check)
            continue
        # check organism
        regex_valid, valid_beginning, organism, end_part = \
            check_path_organism(path_to_check, all_organisms, valid_beginning)
        is_invalid = is_invalid_path(path_to_check, regex_valid, end_part)
        if is_invalid:
            invalid_paths_set.add(path_to_check)
            continue
        # check platform
        regex_valid, valid_beginning, platform, end_part = \
            check_path_platform(path_to_check, all_platform, valid_beginning)
        is_invalid = is_invalid_path(path_to_check, regex_valid, end_part)
        if is_invalid:
            invalid_paths_set.add(path_to_check)
            continue
        # check experiment type
        regex_valid, valid_beginning, experiment_type, end_part = \
            check_path_experiment_type(conf, path_to_check,
                                       platform, valid_beginning)
        is_invalid = is_invalid_path(path_to_check, regex_valid, end_part)
        if regex_valid:
            # So far valid. This is an invariable sub-path. i.e. path up to before the first variable sublevel.
            valid_invariable_layers.add(valid_beginning)
        elif is_invalid:
            invalid_paths_set.add(path_to_check)
            continue
        # check sublevels
        regex_valid, valid_beginning, sublevels, end_part = \
            check_path_sublevels(conf, path_to_check, all_buildversions,
                                 platform, experiment_type, valid_beginning)
        is_invalid = is_invalid_path(path_to_check, regex_valid, end_part)
        if is_invalid:
            invalid_paths_set.add(path_to_check)
            continue
        # check datatype (only if it is not a feature path)
        if not conf.has_section(platform):
            # it's not feature, should validate datatypes
            regex_valid, valid_beginning, datatype, end_part = \
                check_path_datatype(conf, path_to_check, platform,
                                    experiment_type, valid_beginning)
            is_invalid = is_invalid_path(path_to_check, regex_valid, end_part)
            if is_invalid:
                invalid_paths_set.add(path_to_check)
                continue
        # check buildversion
        regex_valid, valid_beginning, buildversion, end_part = \
            check_path_buildversion(path_to_check, all_buildversions,
                                    valid_beginning)
        is_invalid = is_invalid_path(path_to_check, regex_valid, end_part)
        if is_invalid:
            invalid_paths_set.add(path_to_check)
            continue
    # If it went through the path will be identified as valid later
    return invalid_paths_set, valid_invariable_layers


#################################
# # GET DATAWAREHOUSE STRUCTURE ##
#################################

def get_pathlist_from_root(root):
    ''' Lookup into a built structure to extract all 'endpoints' folders '''
    endpoints = set()
    for r, d, f in os.walk(root):
        if '.DS_Store' in f:
            f.remove('.DS_Store')
        r = os.path.relpath(r, root)
        dir_old = \
            [
                True if directory.endswith('.old') else False
                for directory in d
            ]
        if (d == [] and not r.endswith('.old')) or (d != [] and any(dir_old)):
            endpoints.add(r)
    return endpoints


########################################
## FUNCTIONS THAT PARSE THE CONF FILE ##
########################################

def get_repositories_from_conf(conf):
    ''' Returns a list with the main repositories (i.e. furlong_data or external_data/origins)
    '''
    main_conf_repos = \
        ini_conf_parser.get_config_values_as_list('structure',
                                                  'repositories',
                                                  conf)
    main_repositories_list_of_tuples = []
    if main_conf_repos:
        for repository in main_conf_repos:
            # if not internal data, the repositories may have
            # an explicit external origin like a DB or project
            # name e.g modENCODE, REDfly ...
            exp_origins_conf_key = ".".join([repository, "origins"])
            explicit_origins = \
                ini_conf_parser.get_config_values_as_list(
                    'structure',
                    exp_origins_conf_key,
                    conf)
            if not explicit_origins:
                # get_config_values() returned [] because option
                # didn't exist in conf: It means it's internal data
                main_repositories_list_of_tuples.append(repository)
            elif explicit_origins:
                for explicit_origin in explicit_origins:
                    main_repositories_list_of_tuples.append(
                        os.sep.join([repository, explicit_origin]))
    else:
        logger.warn("No main repositories have been found in configuration.")
    return main_repositories_list_of_tuples


def get_all_organisms_from_conf(conf):
    organisms = \
        ini_conf_parser.get_config_values_as_list('structure',
                                                  'organisms',
                                                  conf)
    if organisms:
        return organisms
    else:
        return []


def get_all_platforms_from_conf(conf):
    all_platform = \
        ini_conf_parser.get_config_values_as_list('structure',
                                                  'datatypes',
                                                  conf)
    if all_platform:
        return all_platform
    else:
        return []


def get_associated_experiment_types_from_conf(conf, platform=''):
    if not platform:
        logger.debug(
            "Cannot find platform in configuration.")
        return []
    else:
        sk_option = ".".join([platform, "exptypes"])
        associated_experiment_types = \
            ini_conf_parser.get_config_values_as_list(
                'structure', sk_option, conf)
        if associated_experiment_types:
            return associated_experiment_types
        else:
            return []


def get_all_platform_technology_combinations_from_conf(conf):
    platform_technology_combinations = []
    conf_platforms = get_all_platforms_from_conf(conf)
    if conf_platforms:
        for conf_platform in conf_platforms:
            associated_experiment_types = \
                get_associated_experiment_types_from_conf(conf, conf_platform)
            if associated_experiment_types:
                for associated_experiment_type in associated_experiment_types:
                    platform_technology_combination = \
                        (conf_platform, associated_experiment_type)
                    platform_technology_combinations.append(
                        platform_technology_combination)
    return platform_technology_combinations


def reformat_with_double_underscore(sublevel_subparts):
    '''be sure to have double underscore at both sides like > __example__ '''
    undersc_reseek = re.compile("^[_?]*(.*?)[_?]*$")
    for idx, sublevel_subpart in enumerate(sublevel_subparts[:]):
        matching_undersc = re.match(undersc_reseek, sublevel_subpart)
        if matching_undersc:
            logger.debug("prototype[i-1] ({0}) MATCH ^[_?]*(.*?)[_?]*$ ? {1}"
                         .format(sublevel_subpart, matching_undersc.group(1)))
            sublevel_subparts[idx] = '__' + matching_undersc.group(1) + '__'
    return sublevel_subparts


def get_associated_sublevels_from_conf(conf, platform='', experiment_type=''):
    if not platform:
        # print info?
        return []
    else:
        sublevel_patterns = ''
        if conf.has_section(platform):
            # it's a feature
            feature_kind_sublevel_key = \
                ".".join([platform, "exptypes", "sublevels"])
            sublevel_patterns = \
                ini_conf_parser.get_config_values_as_list(
                    platform,
                    feature_kind_sublevel_key,
                    conf)
        elif not conf.has_section(platform):
            if conf.has_section(experiment_type):
                # it's microarrays or sequencing
                exptype_sublevel_key = \
                    ".".join([platform, experiment_type, "sublevels"])
                sublevel_patterns = \
                    ini_conf_parser.get_config_values_as_list(
                        experiment_type,
                        exptype_sublevel_key,
                        conf)
            else:
                logger.warning("No configuration section for {}"
                               .format(experiment_type))
    if sublevel_patterns:
        return sublevel_patterns
    else:
        return []


def get_associated_datatypes_from_conf(conf, platform='', experiment_type=''):
    if not platform or not experiment_type:
        return []
    else:
        if conf.has_section(platform):
            # it's features kind, no datatype...
            return []
        elif not conf.has_section(platform):
            if conf.has_section(experiment_type):
                # it's sequencing or microarray
                sk_datatypes_key = \
                    ".".join([platform, experiment_type, "featuretypes"])
                associated_datatypes = \
                    ini_conf_parser.get_config_values_as_list(
                        experiment_type, sk_datatypes_key, conf)
                return associated_datatypes
            else:
                return []


def get_associated_buildversions_from_conf(conf, organism=""):
    if not organism:
        return []
    else:
        sk_buildversion_key = \
            ".".join(["organisms", organism, "buildversions"])
        associated_buildversions = \
            ini_conf_parser.get_config_values_as_list(
                "structure", sk_buildversion_key, conf)
        return associated_buildversions


def get_all_buildversions_from_conf(conf):
    organisms = get_all_organisms_from_conf(conf)
    if not organisms:
        return []
    else:
        all_buildversions = set()
        for organism in organisms:
            associated_buildversions = \
                get_associated_buildversions_from_conf(conf, organism)
            all_buildversions = \
                all_buildversions | set(associated_buildversions)
        return all_buildversions


def format_paths_from_a_unique_model(
        data_repositories, organism, platform, experiment_type,
        associated_sublevels, associated_datatypes, associated_buildversions):
    '''
    We have lists of list, this function create the products of lists thanks to itertools library itertools. product that from [ [a,b], [x,y] ] returns [[ a, x ], [ a, y ], [ b, x ], [ b, y ]].
    It yields each of them and they are joined with default OS separator (using the previous example we get a/x, a/y, b/x, b/y
    '''
    all_path_from_model = []

    if not (data_repositories or organism or platform or experiment_type or
            associated_sublevels or associated_datatypes or
            associated_buildversions):
        logger.warning("One of the path component if empty.")
        logger.debug(
            "data_repositories: {} -- ".format(data_repositories) +
            "organism: {} -- ".format(organism) +
            "platform: {} -- ".format(platform) +
            "experiment_type: {} -- ".format(experiment_type) +
            "associated_sublevels: {} -- ".format(associated_sublevels) +
            "associated_buildversions: {}".format(associated_buildversions)
        )

    if not associated_datatypes:
        # features
        for possible_path_assoc in \
            product(data_repositories, organism, platform, experiment_type,
                    associated_sublevels, associated_buildversions):
            unique_path = os.sep.join(map(str, possible_path_assoc))
            all_path_from_model.append(unique_path)
    if associated_datatypes:
        # not feature
        for possible_path_assoc in \
            product(data_repositories, organism, platform, experiment_type,
                    associated_sublevels, associated_datatypes,
                    associated_buildversions):
            unique_path = os.sep.join(map(str, possible_path_assoc))
            all_path_from_model.append(unique_path)
    return all_path_from_model


def get_configuration_paths(conf):
    ''' Read the configuration file (conf is a ConfigParser object that can be queried) to create the appropriate folder hierarchy
    '''
    all_paths_from_conf = []
    # Invariable parts
    # repositories
    data_repositories = get_repositories_from_conf(conf)
    all_organisms_from_conf = get_all_organisms_from_conf(conf)
    # get all technologies combination, each combination define
    # a variable layer
    all_platform_technology_combinations_from_conf = \
        get_all_platform_technology_combinations_from_conf(conf)
    # Variable layers, according to the "platform" x "technology" couple
    # from combination, build variable layers
    for platform_technology_combination in \
            all_platform_technology_combinations_from_conf:
        platform = platform_technology_combination[0]
        experiment_type = platform_technology_combination[1]
        # Get a list of possible sub-levels that are associated
        # with the combination
        try:
            associated_sublevels = \
                get_associated_sublevels_from_conf(conf, platform,
                                                   experiment_type)
            # logger.debug("got {}".format(associated_sublevels))
        except ValueError as verr:
            logger.warning("Ignoring platform_technology_combination {}, {}"
                           .format(platform, experiment_type))
            continue
        # Find the end-points to associate with the combination as well
        # (including the extra layer build version that we will clean later on)
        associated_datatypes = \
            get_associated_datatypes_from_conf(conf, platform, experiment_type)
        # at that point: all the possible structures associated with each
        # possible "platform" x "technology" couple. so we can build paths
        for organism in all_organisms_from_conf:
            associated_buildversions = \
                get_associated_buildversions_from_conf(conf, organism)
            conf_paths_from_model = \
                format_paths_from_a_unique_model(
                    data_repositories,
                    [organism], [platform], [experiment_type],
                    associated_sublevels, associated_datatypes,
                    associated_buildversions)
            all_paths_from_conf += conf_paths_from_model
    return all_paths_from_conf

import os
import pwd
import grp
import re
import sys
import copy
from itertools import product

import lab_warehouse.classes.DatawarehouseStructure as DatawarehouseStructure
import lab_warehouse.modules.parsers.ini_parser as conf_parser
import lab_warehouse.modules.utils as utils
import lab_warehouse.modules.metadata_checker as info_file_checker

import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()

check_na = re.compile('^[n](|/)[a]$', flags=re.IGNORECASE)


def init_logger(lvl, output_json, is_admin=False):
    global logger
    logger = custom_logger.get_logger(__name__, lvl, output_json, is_admin)


class DatawarehouseFile(object):

    def __init__(self, data_src_abspath, src_root):
        '''
        Initialise a file instance.
        '''
        self.src_root = src_root
        self.data_src_abspath =                 \
            os.path.normpath(data_src_abspath)  \
            if data_src_abspath                 \
            else ''

        # Info file is same path with added ".info" extension
        self.info_src_abspath = self.data_src_abspath + ".info"

        data_src_relpath = os.path.relpath(self.data_src_abspath, src_root)
        data_dir_relpath = os.path.dirname(data_src_relpath)
        self.storage_dir_relpath = os.path.normpath(data_dir_relpath)
        self.storage_dir_abspath_src, fname_ext = \
            os.path.split(self.data_src_abspath)

        # Details
        self.basename = os.path.basename(data_src_abspath)
        stats = os.stat(self.data_src_abspath)
        self.id = stats.st_ino
        self.owner_id = int(stats.st_uid)
        self.owner = pwd.getpwuid(stats.st_uid).pw_name
        self.group_id = int(stats.st_gid)
        self.group_name = grp.getgrgid(stats.st_gid).gr_name
        # self.perm = int(utils.convert_st_mode(stats.st_mode))
        # use this to know beforehand if the file could be later removed
        # self.rights = utils.group_can_write(stats.st_mode)
        self.stats = stats
        # self.stats_info = stats_info
        self.size = stats.st_size
        self.access_date = stats.st_atime
        self.change_date = stats.st_ctime
        self.modif_date = stats.st_mtime

        # Main properties that matter for transferability
        self.valid_location = False
        self.valid_extension = False
        self.has_info_file = False
        self.rights = False
        # Properties set up when processing for transfer
        self.transferable = False
        self.transfered = False
        self.aborted = False
        # self.transfer_status = check_transfer_status(False, False, False)
        self.transfer_status = None
        self.tsfr_err_code = ""
        self.tsfr_err_msg = ""
        logger.debug("Created file instance for {0} ({1})"
                     .format(self.basename, self.storage_dir_relpath))

    def __getitem__(self, attr):
        attr_val = None
        try:
            attr_val = getattr(self, attr)
        except AttributeError as attr_exc:
            logger.debug("{0}".format(attr_exc))
        finally:
            return attr_val

    def __repr__(self):
        instname = self.__dict__['basename']
        lftnm = instname[:20] or ""
        rgtnm = instname[-20:] or ""
        return "DatawarehouseFile:{}[...]{}".format(lftnm, rgtnm)

    def __str__(self):
        ret_str = self.__repr__()
        sorted_keys = sorted(self.__dict__.keys())
        for k in sorted_keys:
            v = self.__dict__[k]
            if k is not None:
                ret_str += "\n- {0:.<24} {1:.^} {2}".format(k, '.', v)
        return ret_str

    def introspect(self, allowed_zip_exts, dst_root,
                   clone_locs, exec_user, requested_removal, ini_config):
        ''' DatawrehouseFile instrospection. This file instance discovers things about itself by analysing its full path
        '''
        self.introspect_zip(allowed_zip_exts)
        # self.introspect_format()  # could have real format checker
        self.introspect_names()
        self.introspect_annotation()
        self.introspect_removal_rights(exec_user, requested_removal)
        self.introspect_storage_destination(dst_root)
        self.introspect_duplication(allowed_zip_exts)
        self.introspect_location(clone_locs)
        self.introspect_location_details(ini_config)
        self.introspect_extension(ini_config)

    def introspect_zip(self, allowed_zip_extensions):
        ''' Identify if the file is zipped, the zip extension if zipped '''
        self.zipped, self.zip_ext = \
            identify_zip_format(self.basename, allowed_zip_extensions)

    def introspect_names(self):
        ''' Discover all the needed file names and extension (e.g. full name, display name ...)
        '''
        self.fname_ext =                                         \
            self.basename[:-len(self.zip_ext)].strip('.')  \
            if self.zipped                                  \
            else self.basename

        fname_noext, extension = self.fname_ext.rsplit('.', 1)
        self.fname_noext = self.fname_ext.strip('.')
        self.extension = extension.strip('.')

        self.display_name = " ".join(fname_noext.split('_'))

        self.full_ext =                         \
            self.extension + "." + self.zip_ext \
            if (self.zipped and self.zip_ext)   \
            else self.extension

    def introspect_annotation(self):
        ''' Sets the boolean attribute *self.has_info_file* to True if an info file exists, that is a file with the exact same absolute path with an ".info" extension
        '''
        self.has_info_file = os.path.isfile(self.info_src_abspath)

    def introspect_removal_rights(self, exec_user, requested_removal):
        ''' Sets the boolean attribute *self.valid_rights* to True if no requested removal is needed (then any right wil make it). In the case where a removal is actually needed (default case in command line parsing) then check the file write rights (so see if executing *exec_user* can remove it)
        TODO improve utils.check_file_write_rights()
        '''
        logger.debug("requested_removal {}".format(requested_removal))
        if requested_removal is True:
            valid_rights_data = utils.check_rights(self.stats.st_mode,
                                                   self.owner,
                                                   self.group_name,
                                                   exec_user)
            try:
                stat_info = os.stat(self.info_src_abspath)
            except FileNotFoundError as no_metadata:
                logger.debug("Metadata file does not exist (yet). {}"
                             .format(no_metadata))
                valid_rights_meta = True
            else:
                valid_rights_meta = \
                    utils.check_rights(stat_info.st_mode,
                                       self.owner,
                                       self.group_name,
                                       exec_user)
            logger.debug("valid_rights_data :: {}".format(valid_rights_data))
            logger.debug("valid_rights_meta :: {}".format(valid_rights_meta))
            self.valid_rights = (valid_rights_data and valid_rights_meta)
        else:
            self.valid_rights = True
        logger.debug("introspect_removal_rights -- self.valid_rights {}"
                     .format(self.valid_rights))

    def introspect_location(self, clone_valid_locations):
        self.valid_location = self.storage_dir_relpath in clone_valid_locations

    def introspect_storage_destination(self, dst_root):
        self.storage_dir_abspath_dst = os.path.join(dst_root,
                                                    self.storage_dir_relpath)
        self.data_dst_abspath = os.path.join(self.storage_dir_abspath_dst,
                                             self.basename)
        self.info_dst_abspath = self.data_dst_abspath + ".info"

    def introspect_duplication(self, allowed_zip_exts):
        ''' Check the warehouse to see if the file already exists at identified storage destination.

        This function consider a duplicate by comparing names, no matter if the file is zipped or not

        Args:
            allowed_zip_exts (list): the list of zip extension that are allowed. It is obtained from the configuration file

        Note:
            file1.file_extension will be considered as a duplicate of file1.file_extension, file1.file_extension.zip or file1.file_extension.gz


        '''
        # The path of the dataset that already exists at end storage location
        file_dup_path_in_warehouse = ''
        # Build a list containing all variations of the storage destination
        # path. More than 1 because the duplicated data could not be zipped
        # or have a different zip extension
        storage_abs_dst_nozip = os.path.join(self.storage_dir_abspath_dst,
                                             self.fname_ext)
        potential_dup_files = \
            [storage_abs_dst_nozip] + \
            [storage_abs_dst_nozip + zip_ext
                for zip_ext in allowed_zip_exts]

        for potential_file_dup_path in potential_dup_files:
            if os.path.isfile(potential_file_dup_path):
                file_dup_path_in_warehouse = potential_file_dup_path
                logger.info("{0} already exists in warehouse at {1}"
                            .format(os.path.basename(storage_abs_dst_nozip),
                                    potential_file_dup_path))
                break

        self.new_file = False if file_dup_path_in_warehouse else True
        self.duplic_data_dst_abspath = file_dup_path_in_warehouse
        self.duplic_info_dst_abspath =              \
            file_dup_path_in_warehouse + ".info"    \
            if self.duplic_data_dst_abspath         \
            else ""

    def introspect_location_details(self, ini_config):
        ''' Validate a file by checking its location in the clone and its
        format (based on the extension only)
        '''
        # Check if the file is stored in a valid location
        # Get information from where the file is stored
        self.repository, self.organism, self.platform,      \
            self.experiment_type, self.sublevels,           \
            self.data_type, self.buildversion =             \
            get_storage_location_information(ini_config,
                                             self.storage_dir_relpath)
        # Get further trying to identify each sub-levels part from
        # the pattern in configuration
        sublevels_ids_val = get_sublevels_information(ini_config,
                                                      self.sublevels,
                                                      self.platform,
                                                      self.experiment_type)
        for sub_id, sub_val in sublevels_ids_val.items():
            # logger.debug("{} :: {}".format(sub_id, sub_val))
            setattr(self, sub_id, sub_val)

    def introspect_extension(self, ini_config):
        # Check if valid file format
        self.valid_extension = is_valid_file_extension(ini_config,
                                                       self.platform,
                                                       self.experiment_type,
                                                       self.data_type,
                                                       self.extension)


    def get_file_descriptors_combination(self):
        ''' Get a list of file descriptor in a specific order. This allows to determine which metadata attributes should be used for annotating that file once the global set of rules has been identified. (Because some annotation rules apply for one type of processed data, e.g. alignment files, and not to other types, e.g. peaks, signal)
        '''
        return \
            [
                self.platform,
                self.experiment_type,
                self.data_type,
                self.extension
            ]



    def check_main_properties(self, requested_removal=True):
        ''' Returns a boolean that tells wether the file is transferable. It is True when the files has an info file and when *valid_properties()* returns True as well
        '''
        # Now we also check that the code can remove the file
        valid_props = (self.valid_properties() and self.has_info_file)
        logger.debug("file props OK == [{tf}] for {f}"
                     .format(tf=valid_props, f=self.fname_ext))

        # if self.has_info_file is False:
        #     if not self.tsfr_err_code:
        #         self.tsfr_err_code = "missing-info-file"

        if requested_removal is False:
            # no requested removal of files from clone after transfer
            return valid_props
        else:
            logger.debug("file props OK == [{tf}] AND "
                         .format(tf=valid_props) +
                         "valid rights OK == [{tf}] for {f}"
                         .format(tf=self.valid_rights, f=self.fname_ext))
            return (valid_props and self.valid_rights)


    def read_metadata(self):
        ''' Read the metadata file (infofile) associated with file instance and sets the metadata file attribute. metadata is a dictionary which keys are infofiles attributes (and vlaues their values)
        '''
        ##########################
        try:
            metadata = utils.get_info_file_dic(self.info_src_abspath)
        except FileNotFoundError as read_meta_err:
            logger.debug("{fname} does not exist and will be created"
                         .format(fname=self.storage_dir_relpath))
            try:
                self.create_metadata_file()
            except IOError:
                metadata = None
            else:
                metadata = {}
        return metadata
        ########################
        # return utils.get_info_file_dic(self.info_src_abspath)

    def create_metadata_file(self):
        try:
            open(self.info_src_abspath, 'w').close()
        except IOError as create_file_err:
            logger.error("Cannot create infofile {}. {}"
                         .format(self.info_src_abspath, create_file_err))
            raise


    # def get_metadata(self, read_file=False):
    #     ''' Get the metadata from file object

    #     Args:
    #         read_file (bool): if True, reads the info file. If False just return the metadata attribute of the instance
    #     '''
    #     if read_file is True:
    #         return self.read_metadata()
    #     return self.metadata


    # def init_metadata_check(self, annotation_rules):
        ''' Identify the set of relevant annotation for this file.

        It returns a dictionary with annotation attribute names as keys and associated constraints as values.

        Args:
            annotation_rules (dict): A dictionary created from the metadata rules file and containing all its rules

        Returns:
            (dict) relevant_annotation:  The annotations rules for attributes that apply to this file object extracted from **annotation_rules**

        Example:
            This is the example of the format that **annotation_rules** and **relevant_annotation** dictionaries follow

                {
                    attr1: {
                        'mandatory' : '1',
                        'validation' : 'REGEX_STR'
                    }, ...
                }
        '''
        ####################################
        # use the given metadata unless it's empty
        # this is to not have to write the file twice when a new set of
        # # metadata needs to be checked for the file.
        # metadata = self.get_metadata(read_file=True)
        # # get the relevant annotation rules for file
        # relevant_annotation = \
        #     get_relevant_annotation_attributes(

        #         annotation_rules, metadata)
        # self.relevant_annotation = relevant_annotation
        # return relevant_annotation
        ####################################

    # def read_metadata()
    #     metadata = self.get_metadata(read_file=True)


    def get_relevant_annotation(self, metadata_rules):
        relevant_annotation = \
            conf_parser.get_annotation_attributes(
                self.get_file_descriptors_combination(),
                metadata_rules)
        return relevant_annotation


    def get_file_attributes_metadata(self, relevant_annotation_keys):
        ''' Get a dictionary of file properties

        Specific files properties are of interest when the metadata needs to be validated ("buildversion", "tissue", "experiment_type", "condition", "organism"). Those are identified programmatically by analysing the structure hierarchy the file resides in to be used later for comparison with the annotation given by user. It is used to avoid, for example, cases where the user puts a file in the BUILDVERSION_V1 folder and annotates it ad BUILDVERSION_V2 in the metadata.
        '''
        # TODO need to better handle variable sublevels
        # FIXME no real best way to handle variable sublevels
        file_attributes = \
            [
                "buildversion",
                "tissue",
                "experiment_type",
                "condition",
                "organism",
                "targeted_entity"
            ] # this will fail in other setup, but it is not critical
        return {
            file_attr: file_attr_val
            for (file_attr, file_attr_val) in {
                # get file attribute/key couple
                # if the key exists in keys list.
                mkey: str(getattr(self, mkey))
                for mkey in relevant_annotation_keys
                if mkey in file_attributes
            }.items()
            # select couples were value is non null
            if file_attr_val
        }

    def set_properties_metadata(self, updated_metadata, relevant_annotation):
        attributes_meta = \
            self.get_file_attributes_metadata(relevant_annotation.keys())

        for attr, attr_value in attributes_meta.items():
            attr_constraints = relevant_annotation.get(attr, {})
            # mandatoriness = attr_constraints.get('mandatory')
            validation_regex = attr_constraints.get('validation')
            # replace in metadata **only** if existing value
            # is N/A, empty or regex
            existing_value = self.metadata.get(attr, "")
            is_re   = (existing_value == validation_regex)
            is_na   = (bool(check_na.match(existing_value)) is True)
            is_null = (not existing_value)
            if is_re or is_na or is_null:
                updated_metadata.update(
                    {attr: attr_value}
                )

        # udpate comment needed ?
        upd_comment_constraints = \
            relevant_annotation.get("update_comment", {})
        if self.new_file is False:
            upd_comment_constraints['mandatory'] = "1"
        else:
            relevant_annotation.pop('update_comment', None)
            updated_metadata.pop('update_comment', None)
        logger.debug("'update_comment' in updated_metadata {}".format('update_comment' in updated_metadata))



    def set_default_metadata(self, updated_metadata, relevant_annotation,
                             metadata_rules, add_all):
        # default non existing in info file
        logger.debug("updated_metadata :: {}".format(updated_metadata))
        logger.debug("relevant_annotation :: {}".format(relevant_annotation))

        conditional = {}
        for (attr, constraints) in relevant_annotation.items():
            mandatoriness = constraints.get('mandatory')
            if "=" in mandatoriness:
                cond_attr, cond_mandat = mandatoriness.split("=", 1)
                conditional[attr] = cond_attr
        logger.debug("conditional {}".format(conditional))


        for attr, constraints in relevant_annotation.items():
            logger.debug("attr --> {}".format(attr))
            mandatoriness = constraints.get('mandatory')
            validation_regex = constraints.get('validation')

            logger.debug("mandatoriness :: {}".format(mandatoriness))
            if add_all is False and mandatoriness == "0":
             # and attr not in conditional.keys():
                # ignore non mandatory
                continue
            elif "=" in mandatoriness:  # conditional attributes
                cond_attr, cond_mandat = mandatoriness.split("=", 1)
                cond_value = self.metadata.get(cond_attr, "")
                condition_valid = (cond_value == cond_mandat)
                logger.debug("condition_valid {} [{} == {}]".format(condition_valid, cond_value, cond_mandat))
                if condition_valid is True:
                    # it's kept and flagged as mandatory
                    constraints['mandatory'] = '1'

                # the considered attribute
                attr_value = self.metadata.get(attr, "")
                logger.debug("{} == {}".format(attr, attr_value))
                # if attr_value is not None:
                    # most likely -- >regx
                is_re   = (attr_value == validation_regex)
                is_na   = (bool(check_na.match(attr_value)) is True)
                is_null = (not attr_value)

                cond_constraint = relevant_annotation.get(cond_attr)
                cond_validation_regex = cond_constraint.get('validation')
                cond_is_re = (cond_value == cond_validation_regex)
                cond_is_null = (not cond_value)
                cond_is_na = (bool(check_na.match(cond_value)) is True)

                # logger.debug("is_re :: {}".format(is_re))
                # logger.debug("is_na :: {}".format(is_na))
                # logger.debug("is_null :: {}".format(is_null))
                # logger.debug("cond attr :: {}".format(cond_attr))
                # logger.debug("cond val :: {}".format(cond_value))
                # logger.debug("cond_is_re :: {}".format(cond_is_re))
                if not (cond_is_re or cond_is_null or cond_is_na) and \
                        condition_valid is False:
                    updated_metadata.pop(attr, None)
                    continue
            updated_metadata.setdefault(attr, validation_regex)
        logger.debug("updated_metadata :: {}".format(updated_metadata))


    def update_metadata(self, add_missing_attr=False):
        ''' Write a metadata dictionnary to file adding or updating the timestamp key *last_update*
        '''
        file_props = \
            self.get_file_attributes_metadata(
                self.relevant_annotation.keys())
        metadata, refined_annotation = \
            info_file_checker.refine_metadata(self.metadata,
                                              self.relevant_annotation,
                                              file_props,
                                              add_missing_attr)
        ####################################################################
        # TODO: Add a function here that compare existing set of k:v to the new
        #       set of k:v
        #       This would avoid rewriting the exact same file!
        # NB: If the timestamp is always updated, we should ignore it...
        # OR: Update the timestamp only if the dic is updated as well -- better
        ####################################################################
        metadata.update({'last_update':  utils.get_datestamp()})
        self.metadata = metadata
        self.relevant_annotation = refined_annotation
        logger.debug("self.metadata ---------------\n{}".format(self.metadata))
        # self.write_metadata(write_on_disk)
        # self.introspect_annotation()
        return refined_annotation


    def add_metadata_from_table(self, updated_metadata,
                                intput_table_f_metadata,
                                known_metadata_rules_keys):
        """
        Add all, it does no matter. Overwrite existing
        """
        # filter relevant for this file
        # get relevant + unknown keys
        relevant_keys = self.relevant_annotation.keys()
        input_table_keys = intput_table_f_metadata.keys()
        # unknown_keys = set() # in table keys but not in in all known
        unknown_keys = \
            set(input_table_keys).difference(set(known_metadata_rules_keys))
        # set(known_metadata_rules_keys)
        # iter_keys = unknown_keys.union(input_table_keys)
        # updated_kvs = {}
        logger.debug(input_table_keys)
        for key in input_table_keys:
            # if unknown: keep
            if key in unknown_keys:
                updated_metadata.update({key: intput_table_f_metadata[key]})
            elif key not in relevant_keys:
                # the column is most likely here for another file
                # a value check could be done and the k=v could be added
                # if not n/a, not regex and not empty
                continue
            else:
                # Check the value
                # Add k=v if value is not N/A or empty
                kvalue = intput_table_f_metadata[key]
                is_na = (bool(check_na.match(kvalue)) is True)
                is_null = (not kvalue)
                if is_null or is_na:
                    if is_null: kvalue = "empty"
                    logger.warning(
                        "Ignoring input table metadata key '{}'".format(key) +
                        "for {fname}".format(fname=self.storage_dir_relpath) +
                        "because its value is null [{}]".format(kvalue)
                    )
                else:
                    updated_metadata.update({key: intput_table_f_metadata[key]})

        # ignore N/a values + empty values

        # updated_metadata.update(intput_table_f_metadata)
        # all_keys = self.metadata.keys()
        # meta_keys = intput_table_f_metadata.keys()
        # relevant_keys = self.relevant_annotation.keys()

        # for k in meta_keys:
        #     fvalue = intput_table_f_metadata[k]
        #     # NA values are skipped, if attr not relevant
        #     is_na = (bool(check_na.match(fvalue)) is True)
        #     if is_na and k in relevant_keys:
        #         logger.warning("An N/A value was given for the needed "
        #                        "annotation {k} and ".format(k=k) +
        #                        "will be copied into infofile regardless.")
        #     elif is_na and k not in relevant_keys:
        #         continue
        #     self.metadata.update({
        #         k: intput_table_f_metadata[k]
        #     })


    # def cleanup_metadata(self):
    #     ''' Remove the non mandatory attributes which values are the regex

    #     The validation is sometimes added as the value of the attribute in the annotation, this is to assist a user filling annotations on how it should be done. However, no error should happen upon e.g. transfer if the attribute is not mandatory, therefor this function pops those from the metadata dictionary
    #     '''

    def cleanup_metadata(self, updated_metadata, relevant_annotation,
                         all_meta):
        logger.debug("\n---------check meta for {}".format(self.fname_ext))
        for (attr, value) in list(updated_metadata.items()):
            logger.debug("----- {} :: {} ----".format(attr, value))
            constraints = relevant_annotation.get(attr, {})
            mandatoriness = constraints.get('mandatory')
            validation_regex = constraints.get('validation')
            existing_value = updated_metadata.get(attr, "")
            mandatory = (mandatoriness == "1")
            maybe_mandatory = ("=" in mandatoriness)
            is_re   = (existing_value == validation_regex)
            is_na   = (bool(check_na.match(existing_value)) is True)
            is_null = (not existing_value)
            logger.debug("mandatory :: {}".format(mandatory))
            logger.debug("maybe_mandatory :: {}".format(maybe_mandatory))
            logger.debug("is_re :: {}".format(is_re))
            logger.debug("is_na :: {}".format(is_na))
            logger.debug("is_null :: {}".format(is_null))
            if (maybe_mandatory and is_re):
                logger.debug("continue -->")
                continue
            if all_meta is False:
                # should remove non mandatory key with null val
                if (mandatory) is False and (is_re or is_na or is_null):
                    logger.debug("popping {}".format(attr))
                    updated_metadata.pop(attr, None)


    def write_metadata_to_disk(self, metadata):
        try:
            utils.write_sorted_info_file(self.info_src_abspath, metadata)
        except IOError as wmeta_err:
            err_msg = \
                "Writing metadata to disk failed. {err}".format(err=wmeta_err)
            raise IOError(err_msg)
        else:
            self.metadata = self.updated_metadata
        finally:
            self.introspect_annotation()


    def valid_properties(self):
        ''' Returns a boolean that tells wether the extension and the location are valid '''
        logger.debug("self.valid_location : {}" .format(self.valid_location))
        logger.debug("self.valid_extension : {}" .format(self.valid_extension))

        valid_prop = (self.valid_location and self.valid_extension)
        if valid_prop is False:
            logger.warning("File location and/or file extension are not"
                           " allowed or not compatible "
                           "[{}].".format(self.fname_ext))
        return valid_prop


    def check_metadata(self, metadata_to_validate):
        ''' Check a file metadata (infofile) using *relevant_annotation* the dictionary of annotation rules.

        **relevant_annotation** associates each relevant infofile attributes (keys) to an anonymous dictionary that contains 2 keys: A *mandatory* flag ('0', '1' or key=value for attributes that depends on others), and a *validation* regular expression.

        Args:
            relevant_annotation (dict): A dictionary created from the metadata rules file for each file instance using its descriptor combination.

        Returns:
            3-element tuple containing boolean values (or None) corresponding to different levels of validation

            - **missing_metadata** (*bool*): True when some annotations are simply missing.
            - **re_invalid_meta** (*bool*): True when some annotations are not validated by the available regex.
            - **loc_invalid_meta** (*bool*): True when some annotations are not aggreeing with file properties derived from file location.

        Note:
            **missing_metadata**, **re_invalid_meta**, **loc_invalid_meta** are also set up as DatawarehouseFile attributes for the creation of a global error dictionary later on (see *self.get_global_errors()*)
        '''
        self.missing_metadata = None
        self.re_invalid_meta = None
        self.loc_invalid_meta = None
        self.metadata_error_details = {}

        missing_mandatory_meta = \
            info_file_checker.get_missing_attr(metadata_to_validate.keys(),
                                               self.relevant_annotation, True)
        missing_annot = \
            info_file_checker.get_missing_attr(metadata_to_validate.keys(),
                                               self.relevant_annotation, False)
        file_properties = \
            self.get_file_attributes_metadata(
                metadata_to_validate.keys())
        prop_validated_meta = \
            info_file_checker.validate_annotation_with_file_props(
                metadata_to_validate,
                file_properties)
        regex_validated_meta = \
            info_file_checker.validate_annotation_with_regex(
                metadata_to_validate,
                self.relevant_annotation)

        self.metadata_error_details = \
            info_file_checker.format_info_file_error_dictionary(
                self.relevant_annotation,
                metadata_to_validate,
                missing_mandatory_meta,
                missing_annot,
                prop_validated_meta,
                regex_validated_meta
            )

        return                      \
            missing_mandatory_meta, \
            regex_validated_meta,   \
            prop_validated_meta
        # self.missing_metadata = True if missing_mandatory_meta else False
        # self.re_invalid_meta = not all(list(regex_validated_meta.values()))
        # self.loc_invalid_meta = not all(list(prop_validated_meta.values()))

        # logger.debug("FILE: {}".format(self))
        # logger.debug("prop_validated_meta :: {}".format(prop_validated_meta))

        # return \
        #     self.missing_metadata, \
        #     self.re_invalid_meta, \
        #     self.loc_invalid_meta

    def get_global_errors(self, allow_upd=False):
        ''' Get a dictionary with boolean flag reporting wether critical errors occured when processing the file.

        The global error dictionay is set as an attribute of the file instance (self.file_errors) and contains:
        - **Location**, reports wether the location of the file in the datawarehouse hirarchy is valid
        - **Extension**, reports wether the extension of the file is accepted (kind of a format check...) at that specific location
        - **Update**, reports wether the update is allowed or not in case the file is not a new file but a new version of an existing one
        - **Metadata**, reports wether the file has associated metadata (info file)
        - **Metadata-complete**, reports wether the metadata has all the mandatory attributes keys in the file
        - **Metadata-valid**, reports wether all the values of known annotation attrributes are validated by the validation regex
        '''
        self.file_errors = {
            'location': not self.valid_location,
            'extension': not self.valid_extension,
            'rights': not self.valid_rights,
            'update':
                True if (not self.new_file and not allow_upd)
                else False,
            'metadata': not self.has_info_file,
            'metadata-complete':
                True if self.missing_metadata else False,
            'metadata-valid':
                True if (self.re_invalid_meta or self.loc_invalid_meta)
                else False
        }
        return self.file_errors

    # def get_metadata_errors(self):
    #     return self.metadata_error_details

    def get_detailed_json(self):
        ''' Returns a dictionary used for outputting JSON '''
        return {
            'name':                     self.basename,
            'file_source':              self.data_src_abspath,
            'file_dest':                self.data_dst_abspath,
            'metadata_source':          self.info_src_abspath,
            'metadata_dest':            self.info_dst_abspath,
            'owner':                    self.owner,
            'repository':               self.repository,
            'organism':                 self.organism,
            'platform':                 self.platform,
            'experiment_type':          self.experiment_type,
            'condition':                self.sublevels,
            'data_type':                self.data_type,
            'buildversion':             self.buildversion,
            'is_zipped':                self.zipped,
            'zip_extension':            self.zip_ext,
            'size':                     self.size,
            # Transfer information
            'transfer_status':          self.transfer_status,
            'transfered':               self.transfered,
            'transferable':             self.transferable,
            'transfer_aborted':         self.aborted,
            'transfer_error_code':      self.tsfr_err_code,
            'transfer_error_string':    self.tsfr_err_msg,
            # Error information  # now added later on in main
            # 'errors_dataset_file':      self.file_errors,
            # 'errors_metadata_file':     self.metadata_error_details
        }


#######
#######

def identify_zip_format(bname, zip_extensions):
    '''
    Identified the file longest zipped ext from our list of zip_extensions
    When check the file file.tar.gz, with zip_extensions = ['gz', 'tar.gz']
    Returns 'tar.gz'
    '''
    zipped = False
    zip_ext = ''
    for zip_ext in sorted(zip_extensions, key=len, reverse=True):
        if bname.endswith(zip_ext):
            return True, zip_ext.strip('.')
    else:
        return zipped, zip_ext


def get_storage_location_information(ini_config, relative_clone_storage_dir):
    ''' Identify the different part of the path using
    identify_a_path_pattern()
    '''
    _, repo, organ, pltfrm, exp_type, sublvls, data_type, buildvers = \
        identify_a_path_pattern(ini_config, relative_clone_storage_dir)
    return repo, organ, pltfrm, exp_type, sublvls, data_type, buildvers


def get_sublevels_information(ini_config, sublevels,
                              platform, experiment_type):
    # This function associate different parts of the sublevels using keyword
    # contained in the configuration sublevels prototypes
    # Not always accurate i.e. in the case of more then one protortpe of
    # the same length it would take one, and associate the keyword with
    # maybe not a meaningful value
    sublevels_dic = {}
    sublevels = sublevels.split(os.sep)
    sublevels_pattern = \
        find_sublevels_pattern(ini_config, platform,
                               experiment_type, len(sublevels))
    if sublevels_pattern:
        sublevels_kw = exctract_helpers_sublevels_keyword(sublevels_pattern)
        sublevels_dic = \
            exctract_sublevels_identification(sublevels_kw, sublevels)
    return sublevels_dic


def is_valid_file_extension(ini_config, platform, experiment_type, data_type,
                            format_ext):
    ''' Check the configuration to find out if the extension of the file is
    right according to its location. The extension is by default not valid
    if the file isn't located at a clone valid storage location.
    ---
    TODO: Have a real format checker that doesn't check only the extension of
    the file but use a dedicated parser to identify if the format is indeed
    valid
    '''
    # The set of valid format differs from location to location
    # In any case, if the location is not valid, the format cannot be tested
    extension_section, extension_key = "", ""
    if ini_config.has_section(platform):  # == features
        extension_section = platform
        extension_key = ".".join([platform, "exptypes", "formats"])
    else:
        extension_section = experiment_type
        extension_key = \
            ".".join([platform, experiment_type, data_type, "formats"])

    extension_list = conf_parser.get_config_values_as_list(extension_section,
                                                           extension_key,
                                                           ini_config)
    extensions_list = [x.strip('. ').lower() for x in extension_list]

    return True if format_ext.lower() in extensions_list else False


def get_relevant_annotation_attributes(model, annot_rules):
    # relevant_annotation =
    # Need to double check for attributes interdependencies
    # FIXME
    # relevant_annotation = \
    #     info_file_checker.check_attr_dependencies(known_annot_dict,
    #                                               relevant_annotation)
    # print(relevant_annotation)
    return relevant_annotation


def ensure_permission(transfered_data_file, transfered_info_file):
    utils.chmod(transfered_data_file, mkmode=0o444)
    utils.chmod(transfered_info_file, mkmode=0o444)


def move_dataset(data_file, info_file, dest_directory,
                 dest_data_name="", dest_info_name=""):
    ''' Move a dataset to the destination directory *dest_directory

    Copy and paste the *data_file* to its destination directory with its new name *dest_data_name* (or its actual name if empty). Does the same for the info file except that it is not copied but read+write using make_sorted_keyval_copy(); this overwrite any existing metadata adding missing keys if any and allows to sort keys before writing the metadata. This function is used both for transfering file from the clone to the  warehouse and to save an old version of the file (when attempting to transfer a file that already exists)

    Args:
        data_file (str): Absolute path of the source file
        info_file (str): Absolute path of the source file
        dest_directory (str): Absolute path of the destination directory
        dest_data_name (str): A file name to be use for the data copy
        dest_info_name (str): A file name to be use for the metdata copy

    Returns:
        4-element tuple containing boolean values (or None) corresponding to different levels of validation

        - **moved** (*bool*): True when the file was successfully moved
        - **copied_data** (*str*): The absolute path of the data copy
        - **copied_info** (*str*): The absolute path of the metadata copy
        - **unremoved_trsfrd_files** (*list*): A list containing the path the data and/or metadata if one of them was not properly removed after it has been copied at dest

    '''
    unremoved_trsfrd_files = []
    moved = False
    if not dest_data_name:
        os.path.basename(data_file)
    if not dest_info_name:
        os.path.basename(info_file)

    copied_data = ""
    copied_info = ""
    try:
        utils.makedir(dest_directory)
        copied_data = make_copy(data_file, dest_directory, dest_data_name)
        copied_info = make_sorted_keyval_copy(info_file, dest_directory)

    except (OSError, IOError, ValueError) as copy_err:
        logger.error("Error occured when making a copy of the dataset to " +
                     "{}. {}".format(dest_directory, copy_err))
        # rm them if copy started.
        # f.
        logger.warn("[transfer-files] [FAILED] {0}"
                    .format(tfile.basename))
        if copied_data: utils.rm_file(copied_data)
        if copied_info: utils.rm_file(copied_info)
        # Remove the eventual destination directory if the transfer
        # fails. Of course, it will not remove the directory unless its
        # empty
        os.removedirs(dest_directory)
        raise copy_err
    else:
        # Moved is considered done before the src file is removed.
        moved = True
        utils.silent_rm_file(data_file)
        utils.silent_rm_file(info_file)
        unremoved_trsfrd_files = [f for f in [data_file, info_file]
                                  if os.path.isfile(f)]
    return moved, copied_data, copied_info, unremoved_trsfrd_files


def make_copy(src_file_path, dest_file_dir, dest_file_name=""):
    ''' Copy **src_file_path** to **dest_file_dir** directory

    Copy the src file to destination and produce and validate a checksum to check if the copy was okay. The name of the file can be changed using **dest_file_name** which is set to the **src_file_name** name if not given

    Args:
        src_file_path (str): Absolute path of the source file
        dest_file_dir (str): Absolute path of the destination directory
        dest_file_name (str): A file name to be use for the copy

    Returns:
        (str) The absolute path of destination file copy

    '''
    if not dest_file_name:
        dest_file_name = os.path.basename(src_file_path)
    dest_file_path = os.path.join(dest_file_dir, dest_file_name)
    utils.copy_file(src_file_path, dest_file_path)
    validate_checksums(src_file_path, dest_file_path)
    return dest_file_path


def make_sorted_keyval_copy(info_file, dest_directory, dest_file_name=""):
    ''' Read+Write a dictionary sorted by keys to file

    after sorting its keys **src_file_path** to **dest_file_dir** directory

    Copy the src file to destination and produce and validate a checksum to check if the copy was okay. The name of the file can be changed using **dest_file_name** which is set to the **src_file_name** name if not given

    Args:
        src_file_path (str): Absolute path of the source file
        dest_file_dir (str): Absolute path of the destination directory
        dest_file_name (str): A file name to be use for the copy

    Returns:
        (str) The absolute path of destination file copy

    '''
    if not dest_file_name:
        dest_file_name = os.path.basename(info_file)
    dest_file_path = os.path.join(dest_directory, dest_file_name)
    info_key_values = utils.get_info_file_dic(info_file)
    utils.write_sorted_info_file(dest_file_path, info_key_values)
    return dest_file_path


def validate_checksums(file1, file2):
    cs1 = utils.get_checksum(file1)
    cs2 = utils.get_checksum(file2)
    if not (cs1 == cs2):
        err_str = "Checksum of {} and {} differ ".format(file1, file2) + \
                  "({} is different from {})".format(cs1, cs2)
        raise ValueError(err_str)


def transfer_data_and_metadata(tfile, allowed_updates):
    ''' Transfer file + infofile from clone to the warehouse. The transfer itself is just copying and pasting both files from source to destination endpoint. '''
    logger.debug("[transfer-files] [...] {0}".format(tfile.basename))
    # if not a new file, then we need to make a backup/version of it
    # (incl. the info file)
    # version_file_path, version_info_file_path = '', ''
    # is_versionned = False
    is_versionned = None
    unrmd_files = []
    # Transfered data + metadata
    tsfrd = False
    abort = False
    tsfr_errco = ''
    tsfr_errst = ''
    tsfr_dir = os.path.dirname(tfile.data_dst_abspath)

    # Save previous version in the ".old" folder
    if (not tfile.new_file and not allowed_updates):
        tsfr_errco = "update-not-allowed"  # could change to integers
        tsfr_errst = "File is an update of an existing file but version"\
                     + " update is not allowed (see command line"\
                     + " parameters)"
        tfile.tsfr_err_code = tsfr_errco
        tfile.tsfr_err_msg = tsfr_errst
        logger.warn("[transfer-files] [FAILED] {0}"
                    .format(tfile.basename))
    else:
        if not tfile.new_file:  # make version
            timestamp = utils.get_timestamp()
            version_storage_dir = os.path.join(
                os.path.dirname(tfile.data_dst_abspath),
                tfile.fname_noext + ".old"
            )
            base_version_data = \
                os.path.basename(tfile.duplic_data_dst_abspath)
            base_version_info = \
                os.path.basename(tfile.duplic_info_dst_abspath)
            version_data_name = '_'.join([base_version_data, timestamp])
            version_info_name = '_'.join([base_version_info, timestamp])

            try:
                is_versionned, version_data, version_info, _ = \
                    move_dataset(tfile.duplic_data_dst_abspath,
                                 tfile.duplic_info_dst_abspath,
                                 version_storage_dir,
                                 version_data_name,
                                 version_info_name)
            except (OSError, IOError, ValueError):
                # except IOError as ioerr_rm:
                # For clone to warehouse transfer: It's OK if we cannot remove
                # the moved file. e.g. files have been moved to the warehouse
                # but they cannot be removed from the clone, it's OK.
                # However in the case of versionning, if files have been moved
                # to version dir but cannot be removed it's a problem because
                # the old version will still be in place of the new.
                # logger.warn("Could not remove dataset after moving it. {}"
                #             .format(ioerr_rm))
                # raise

                abort = True
                # maybe here add more info in the error code
                # e.g. version failed because cannot create a folder
                # or because checksums differ, etc...
                tsfr_errco = "versionning-failed"
                tsfr_errst = "An error occured when trying to version"\
                             + " the file."
                tfile.tsfr_err_code = tsfr_errco
                tfile.tsfr_err_msg = tsfr_errst
                # os.removedirs(version_storage_dir)
                # removedirs is now done directly by move_dataset()

            else:
                logger.info("The previous version of {}"
                            .format(tfile.basename) +
                            " has been saved at {}"
                            .format(version_storage_dir) +
                            " with the timesamp {}"
                            .format(timestamp))
    # Here transfer error code should be empty except if there was
    # an error with needed file versionning
    if (not abort) and (tfile.new_file or
                    (not tfile.new_file and is_versionned)):
        try:
            tsfrd, _, _, unrmd_files = \
                move_dataset(tfile.data_src_abspath,
                             tfile.info_src_abspath,
                             tsfr_dir)
            ensure_permission(tfile.data_dst_abspath, tfile.info_dst_abspath)
        except (OSError, IOError, ValueError):
            logger.debug("ABORTED !!!")
            logger.debug("tsfrd {}".format(tsfrd))
            logger.debug("tsfr_dir {}".format(tsfr_dir))
            abort = True
            tsfr_errco = "transfer-failed"
            tsfr_errst = "An error occured when trying to transfer"\
                         + " the file {}".format(
                             tfile.data_src_abspath)

            tfile.tsfr_err_code = tsfr_errco
            tfile.tsfr_err_msg = tsfr_errst
            if is_versionned is not None:
                # move versionned file back to original trsfr directory
                # reuse original basename and not the timestamped names
                move_dataset(version_data,
                             version_info,
                             tsfr_dir,
                             base_version_data,
                             base_version_info)
                os.removedirs(version_storage_dir)
            # os.removedirs(tsfr_dir)
            # removedirs is now done directly by move_dataset()
    return tsfrd, abort, unrmd_files


def check_transfer_status(transferable, transfered, aborted):
    ''' Compares boolean to identify a human redable status for the file '''
    status = "UNKNOWN"
    if (transferable and transfered):
        status = "TRANSFERED"
    elif (transferable and aborted):
        status = "ABORTED"
    elif (transferable and not transfered):
        status = "TRANSFERABLE"
    elif (not transferable and not transfered):
        status = "ERROR"
    elif (not transferable and transfered):
        status = "!!IMPOSSIBLE!!"
    return status


def identify_a_path_pattern(ini_config, file_path):
    path_repository, path_organism, p_platform, path_exptype, path_sublevels, \
        path_datatype, path_buildversion = '', '', '', '', '', '', ''

    warehouse_repositories = \
        DatawarehouseStructure.get_repositories_from_conf(ini_config)
    valid, valid_begining, path_repository, end_part = \
        DatawarehouseStructure.check_path_repository(file_path,
                                                     warehouse_repositories)

    datawarehouse_organisms = \
        DatawarehouseStructure.get_all_organisms_from_conf(ini_config)
    valid, valid_begining, path_organism, end_part = \
        DatawarehouseStructure.check_path_organism(file_path,
                                                   datawarehouse_organisms,
                                                   valid_begining)

    datawarehouse_platforms = \
        DatawarehouseStructure.get_all_platforms_from_conf(ini_config)
    valid, valid_begining, path_platform, end_part = \
        DatawarehouseStructure.check_path_platform(file_path,
                                                   datawarehouse_platforms,
                                                   valid_begining)

    valid, valid_begining, path_exptype, end_part = \
        DatawarehouseStructure.check_path_experiment_type(ini_config,
                                                          file_path,
                                                          path_platform,
                                                          valid_begining)

    warehouse_buildversions = \
        DatawarehouseStructure.get_all_buildversions_from_conf(ini_config)
    valid, valid_begining, path_sublevels, end_part = \
        DatawarehouseStructure.check_path_sublevels(ini_config,
                                                    file_path,
                                                    warehouse_buildversions,
                                                    path_platform,
                                                    path_exptype,
                                                    valid_begining)

    if not ini_config.has_section(p_platform):  # it's NOT a feature file path
        valid, valid_begining, path_datatype, end_part = \
            DatawarehouseStructure.check_path_datatype(ini_config,
                                                       file_path,
                                                       path_platform,
                                                       path_exptype,
                                                       valid_begining)

    valid, valid_begining, path_buildversion, end_part = \
        DatawarehouseStructure.check_path_buildversion(file_path,
                                                       warehouse_buildversions,
                                                       valid_begining)

    len_pattern = \
        sum(1 for pattern_elem in
            [
                path_repository,
                path_organism,
                path_platform,
                path_exptype,
                path_sublevels,
                path_datatype,
                path_buildversion
            ]
            if pattern_elem)
    return                                                              \
        len_pattern, path_repository, path_organism, path_platform,     \
        path_exptype, path_sublevels, path_datatype, path_buildversion


def find_sublevels_pattern(ini_config, platform, exp_type, len_known_sublvls):
    ''' Identify the sublevel pattern used for this file.

    A given combination of **platform** and **exp_type** migh have more than one possible subdirecotry combination, for example it could be platform/exp_type/A/B/C or platform/exp_type/A/B only
    '''
    if ini_config.has_section(platform):
        # it's features
        sublevels_patterns = \
            conf_parser.get_config_values_as_list(
                platform,
                platform + ".exptypes.sublevels",
                ini_config)
    else:
        sublevels_patterns = \
            conf_parser.get_config_values_as_list(
                exp_type,
                platform + "." + exp_type + ".sublevels",
                ini_config)

    kept_sublevel_pattern = ''
    if sublevels_patterns:
        sublevels_patterns = \
            [
                sublpat.split(os.sep)
                for sublpat in sublevels_patterns
            ]
        for sublevels_pattern in sublevels_patterns:
            if len(sublevels_pattern) == len_known_sublvls:
                kept_sublevel_pattern = sublevels_pattern
                break
    return kept_sublevel_pattern


def match_side_double_underscores(sl_string):
    ''' Check if **sl_string** is flanked with double underscores. If it is returns *sublevel_id*: the part in between the flanking double underscores.
    '''
    sublevel_id = ''
    match = re.match('__(.*)__', sl_string)
    if match:
        sublevel_id = match.group(1)
    return sublevel_id


def exctract_helpers_sublevels_keyword(sublevels_pattern):
    # exctract info like __trarget_entity_ with regexp, #return a dictionnary with found kw
    '''
    Parse something like __tissue__/__targeted_entity__/__condition__.
    return ('tissue', 'targeted_entity', 'condition')
    '''
    if isinstance(sublevels_pattern, str):
        sublevels_pattern = sublevels_pattern.split(os.sep)
    subl_keywords = []
    for layer in sublevels_pattern:
        pattern_lay = match_side_double_underscores(layer)
        if pattern_lay:
            subl_keywords.append(pattern_lay)
        else:
            subl_keywords.append('')
    return tuple(subl_keywords)


def exctract_sublevels_identification(sublevels_kw, real_sublevels):
    '''
    using real sublevels e.g. whole_embryo/Mef2/6-8h, exctract information by
    comparing sublevels patterns to real sublevels.
    return { 'tissue' : 'tissue_1', 'targeted_entity' : 'target_1' ,
    'condition': 'condition_1'}
    '''
    sublevels_dic = {}
    len_real = len(real_sublevels)
    len_subl = len(sublevels_kw)
    if not len_real == len_subl:
        logger.warn("The real sublayers do not correspond to the pattern " +
                    "found in config (length pattern = %s and length real " +
                    "sublayers = %s)", len_subl, len_real)
        return {}
    else:
        for subl_kw, real_sub in zip(sublevels_kw, real_sublevels):
            sublevels_dic[subl_kw] = real_sub
        return sublevels_dic
    return {}

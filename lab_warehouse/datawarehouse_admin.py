#!/usr/bin/env python3
import sys
import os
from argparse import ArgumentError as ArgumentError

import lab_warehouse.datawarehouse_manager as datawarehouse_manager
# from lab_warehouse import datawarehouse_manager as datawarehouse_manager
import lab_warehouse.classes.DatawarehouseFile as DatawarehouseFile
import lab_warehouse.classes.DatawarehouseStructure as DatawarehouseStructure
import lab_warehouse.modules.file_hunter as file_hunter
import lab_warehouse.modules.output_formatter as output_formatter
import lab_warehouse.modules.utils as utils
import lab_warehouse.modules.parsers.ini_parser as conf_parser
import lab_warehouse.modules.logging.logger as custom_logger
logger = custom_logger.get_default()


def init_logger(lvl, json=False):
    global logger
    logger = custom_logger.get_logger(__name__, lvl, json, True)


# Generate structures
#######################

def create_structure(warehouse, clone, config, parser_arguments, json_output):
    subparser_name = parser_arguments.get("subparser", None)
    add_help_dirs = parser_arguments.get("bool_add_opt_repos", False)
    add_cnf_dirs = parser_arguments.get("bool_add_conf_repos", False)

    if subparser_name == "create-warehouse":
        generate_warehouse(warehouse, add_help_dirs)
    elif subparser_name == "generate-clone":
        generate_clone(warehouse, clone, add_help_dirs, add_cnf_dirs)
    else:
        raise ValueError("Not a valid subparser name ({})"
                         .format(subparser_name))


def generate_warehouse(warehouse, add_help_dirs=False):
    # valid_locs are valid locations paths from the warehouse object needed
    # when generating the warehouse clone (a copy of all valid locations).
    # it's yet an empty set until the warehouse is generated.
    try:
        warehouse.create_root_dir()
    except OSError as crwh_err:
        raise("Cannot create warehouse at {}".format(warehouse.root) +
              "{}".format(crwh_err))
    else:
        wh_relpths = \
            warehouse.get_valid_relpaths(
                valid_locs=set(),
                inv_prfxs=warehouse.invariable_prefixes,
                add_helps=add_help_dirs,
                help_pths=warehouse.helpers_paths,
                add_cnfs=True)
        wh_abspaths = DatawarehouseStructure.get_root_abspaths(warehouse.root,
                                                               wh_relpths)
        utils.makedirs(wh_abspaths, mkmode=0o755)
        warehouse.ensure_folders_permissions()


def generate_clone(warehouse, clone, add_help_dirs=False, add_cnf_dirs=False):

    if warehouse.root_exists:
        clone.create_root_dir()
        clone_relpths = \
            clone.get_valid_relpaths(
                valid_locs=warehouse.valid_storage_locations,
                inv_prfxs=warehouse.existing_valid_invariable_prefixes,
                add_helps=add_help_dirs,
                help_pths=warehouse.existing_helpers_paths,
                add_cnfs=add_cnf_dirs)

        clone_abspaths = \
            DatawarehouseStructure.get_root_abspaths(clone.root,
                                                     clone_relpths)
        utils.makedirs(clone_abspaths, mkmode=0o770)
        clone.ensure_folders_permissions(0o770)
    else:
        raise OSError("A warehouse needs to be created cloning it" +
                      ". {} does not exists." .format(warehouse.root))


def check_warehouse(warehouse, clone, config, parser_arguments, json_output):
    datawarehouse_manager.check_clone(warehouse, clone, config, parser_arguments, json_output)
    warehouse.ensure_folders_permissions()


def check_clone(warehouse, clone, config, parser_arguments, json_output):
    datawarehouse_manager.check_clone(warehouse, clone, config, parser_arguments, json_output)
    clone.ensure_folders_permissions(perms=0o770)


# Templates directories
#########################

def add_template_directories(warehouse, clone, config,
                             parser_arguments, json_output):
    # warehouse, clone, config, kwarguments, json_output
    ''' Add template directories to clone and or warehouse

    Note:
        For adding template directories to clone it uses the datawarehouse_manager function
    '''
    add_in_wh = parser_arguments.get("warehouse_tmplts", False)
    add_in_clone = parser_arguments.get("clone_tmplts", False)
    add_conf_tpts = parser_arguments.get("bool_add_conf_repos")
    wh_tmplts = set()

    if (add_in_wh and add_in_clone) is False:
        raise ValueError('Admin has to explicitely use "c" and/or "w" ' +
                         'flag(s) to add templates in clone and/or warehouse')

    if add_in_wh is True:
        wh_tmplts = warehouse.get_template_dirs(add_conf_tpts)
        ndir = len(wh_tmplts)
        sfx = "ies" if ndir > 1 else "y"
        if ndir < 1:
            logger.info("[add templates] No templates directory to " +
                        "create in warehouse")
            return
        logger.info("[add templates] Creating {} template director{}"
                    .format(ndir, sfx) + " in warehouse")
        for d in wh_tmplts:
            utils.makedirs(d, 0o755)
        else:
            logger.info("[add templates] Added warehouse template director{}".
                        format(sfx))
    if add_in_clone is True:
        datawarehouse_manager.add_template_directories(warehouse,
                                                       clone,
                                                       config,
                                                       parser_arguments,
                                                       json_output)


def remove_template_directories(warehouse, clone, config,
                                parser_arguments, json_output):
    rm_in_clone = parser_arguments.get("rm_clone_opt", False)
    rm_in_wh = parser_arguments.get("rm_warehouse_opt", False)
    if not (rm_in_clone or rm_in_wh):
        raise ValueError("Use -c and/or -w to remove templates from clone " +
                         "and/or warehouse")

    clone_tmplt_subdirs = set()
    wh_tmplt_subdirs = set()
    if rm_in_clone:
        clone_tmplt_subdirs = clone.get_template_root_subdir()
    if rm_in_wh:
        wh_tmplt_subdirs = warehouse.get_template_root_subdir()

    tmplt_subdirs = clone_tmplt_subdirs | wh_tmplt_subdirs

    # Check that they contain no files
    tmplt_dirs_to_rm = set()
    for tdir in tmplt_subdirs:
        t_files = file_hunter.select_datasets_files_only(
            file_hunter.walk_and_find_all_files(tdir))
        if not t_files:  # is empty
            tmplt_dirs_to_rm.add(tdir)
    # remove tmplt_dirs_to_rm
    for d in tmplt_dirs_to_rm:
        utils.remove_dir_tree_from_root(d)

# File Transfer
###############
#     files_collection = \
#         datawarehouse_manager.init_files_processing(warehouse,
#                                                     clone,
#                                                     config,
#                                                     parser_arguments)

#     metadata_rules = get_metadata_rules(parser_arguments)

#     # all_meta = parser_arguments.get("add_all_missing", False)
#     set_files_metadata(files_collection, metadata_rules)
# #     # update with table if needed
# #     update_metadata_with_input_table(files_collection,
# #                                      input_table_metadata,
# #                                      metadata_rules)
# #     write_updated_metadata_files(files_collection)

# #     output_table_filepath = parser_arguments.get("output_table_path")
# #     make_output_table_metadata(files_collection, output_table_filepath, all_meta)

#     check_metadata(files_collection)
# #     # for transfer files only
#     required_removal = parser_arguments.get("required_removal", False)
#     check_transferability(files_collection, required_removal)


def check_transferability(files_collection, requested_removal):
    for f in files_collection:
        f.transferable =                                    \
            f.check_main_properties(requested_removal)      \
            and (f.missing_meta     is not (None or True))  \
            and (f.re_invalid_meta  is not (None or True))  \
            and (f.loc_incalid_meta is not (None or True))

        logger.debug("{} :: transferable == {}"
                     .format(f.basename, f.transferable))


def transfer_files(warehouse, clone, config, parser_arguments, json_output):
    ''' Search clone for files and transfer them to the warehouse '''
    allow_upd = parser_arguments.get("bool_update_version")
    username = parser_arguments.get("unix_name", False)

    files_collection = \
        datawarehouse_manager.init_files_processing(warehouse,
                                                    clone,
                                                    config,
                                                    parser_arguments)

    metadata_rules = datawarehouse_manager.get_metadata_rules(parser_arguments)
    datawarehouse_manager.set_files_metadata(files_collection, metadata_rules)
    datawarehouse_manager.check_metadata(files_collection)
    required_removal = parser_arguments.get("required_removal")
    logger.debug("required_removal {}".format(required_removal))
    check_transferability(files_collection, required_removal)

    dry_run = parser_arguments.get("dry_run")
    dr_prfx = "[transfer-files]{} ".format(" [DRY RUN]" if dry_run else "")

    unremoved_trsfrd = []
    if any(f.transferable is True for f in files_collection):
        logger.info("{}Starts".format(dr_prfx))
        for f in files_collection:
            tlogger = logger.info
            print_prefx = "{}{:.<83} ".format(dr_prfx, f.basename[:80])
            dr_been = "should be"
            dr_verb = "transfered"
            tsfrd, aborted = False, False
            if not dry_run and f.transferable is True:
                logger.info("[transfer-files] Transfering {:.<91}"
                            .format(f.basename[:88]))
                tsfrd, aborted, unrmd_files = \
                    DatawarehouseFile.transfer_data_and_metadata(f, allow_upd)
                logger.debug("tsfrd {}".format(tsfrd))
                logger.debug("aborted {}".format(aborted))
                logger.debug("unrmd_files {}".format(unrmd_files))
                unremoved_trsfrd = unremoved_trsfrd + unrmd_files
            elif f.transferable is False:
                dr_been = "[NOT"
                dr_verb = "TRANSFERABLE]"
                tlogger = logger.warning

            f.transfered = tsfrd
            f.aborted = aborted
            f.transfer_status = \
                DatawarehouseFile.check_transfer_status(f.transferable,
                                                        tsfrd,
                                                        aborted)
            dr_been = "has been" if (tsfrd or aborted) else dr_been
            logger.debug("Tr:{} -- Ab:{} -- St:{}"
                         .format(tsfrd, aborted, f.transfer_status))
            if aborted:
                dr_verb = "aborted"
                tlogger = logger.error
            tlogger("{}".format(print_prefx) +
                    "{} ".format(dr_been) +
                    "{}".format(dr_verb))
            # print("")  # Give me some space dude

    transfer_output(files_collection,
                    warehouse,
                    username,
                    unremoved_trsfrd,
                    parser_arguments,
                    json_output)


def transfer_output(files_collection, warehouse, username, unremoved_trsfrd,
                    parser_arguments, json_output):

    allow_upd = parser_arguments.get('bool_update_version')
    # json = parser_arguments.get('json_output')
    # Human readable log (stdout/file) data objects

    # Both HR reports and JSON data objects
    all_annot_err = {}
    files_info = []
    transfered_list = []
    untransfered_list = []
    json_output['LOG'].update({
        "files_info": files_info,
        "transfered_list": transfered_list,
        "untransfered_list": untransfered_list,
        "metadata_errors": all_annot_err
    })
    n_nottrsfd = len(unremoved_trsfrd)
    if n_nottrsfd > 0:
        err_str = "{} transfered file could not ".format(n_nottrsfd) + \
                  "be removed from clone. File should be manually removed" + \
                  "from clone"
        logger.warn("[transfer-files] {}".format(err_str))
        json_output['WARN'].update({'DESC': err_str})
        rmtrsfrd_flpth = '/tmp/' + utils.get_datestamp() + \
                         '_warehouse_unremoved_transfered_files.txt'
        try:
            with open(rmtrsfrd_flpth, 'rw+') as rmf:
                for file_pth in unremoved_trsfrd:
                    rmf.write("{}\n".format(file_pth))
        except OSError as rmwrtfile_err:
            wrn_str = "Then: Failed writing unremoved transfered file " + \
                      "paths at {}. {}".format(rmtrsfrd_flpth, rmwrtfile_err)
            logger.warn("[transfer-files] {}".format(wrn_str))
        else:
            wrn_str = "Then: All the path of unremoved transfered files " + \
                      "to be removed from clone have been written at {}." + \
                      " Please remove them manually."
            logger.warn("[transfer-files] {}".format(wrn_str))
        finally:
            json_output['WARN'].update({'DESC': err_str + wrn_str})

    if len(files_collection) < 1:
        logstring_usr = \
            "belonging to {} ".format(username) if username else ""
        logstring_msg = \
            "[transfer-files] No files {}found.".format(logstring_usr)
        logger.info(logstring_msg)
        # if json: output_formatter.dump_json_to_stdout(json_output)
        return

    all_global_errs = {}
    all_annot_err = {}
    for f in files_collection:
        # logger.debug(f)
        # Add to list of transfered files (JSON and STDOUT output)
        # if f.transfered is False:
        #     transfered_list.append(
        #         os.path.relpath(f.data_dst_abspath, warehouse.root))

        # if parser_arguments.get("dry_run")

        # Get a sumup of the errors for each file (STDOUT output)
        f_global_errs = f.get_global_errors(allow_upd)

        all_reasons = [
            err_key
            for (err_key, err_flag) in f_global_errs.items()
            if err_flag is True
        ]

        drun = parser_arguments.get("dry_run", False)
        if drun is True: all_reasons.insert(0, "dry-run")
        f.reason_str = " && ".join(all_reasons)

        all_global_errs.update({f.data_src_abspath: f_global_errs})
        # Get annotation related errors for each file (STDOUT output)
        # metadata_errors = f.get_metadata_errors()
        all_annot_err.update({f.info_src_abspath: f.metadata_error_details})
        # Get a detailed json of each file (JSON output)
        f_detailed_json = f.get_detailed_json()
        # Add the global errors and the annotation errors to the detailed json
        f_detailed_json.update(
            {
                'errors_dataset_file':  f_global_errs or {},
                'errors_metadata_file': f.metadata_error_details or {}
            }
        )
        files_info.append(f_detailed_json)
        # # ---
        # relative_path = os.path.relpath(f.data_dst_abspath, warehouse.root)
        # if f.transfered is True:
        #     transfered_list.append(relative_path)
        # elif f.transfered is False:
        #     untransfered_list.append(relative_path)

    # Output nice reports for stdout (when no JSON) and logfile (always)
    files_formated_report = \
        output_formatter.format_files_errors_report(all_global_errs)

    info_files_formated_report = \
        output_formatter.format_info_file_error_report(all_annot_err)

    transfered_file_report = \
        output_formatter.format_transfer_report(files_collection,
                                                warehouse.root)

    logger.info("{0}".format(info_files_formated_report))
    logger.info("{0}".format(files_formated_report))
    logger.info("{0}".format(transfered_file_report))


def init_admin_args_parser(arg_parser, non_admin_subparsers,
                           parent_check_clone, parent_check_warehouse):

    arg_parser.description += " -- /!\ ADMIN TOOL /!\ "

    arg_parser.add_argument(
        # level of the log, see custom_logger module and logging python lib
        '-l',
        '--log',
        action='store',
        dest='log_level', default="info",
        metavar='LOGLEVEL', type=str,
        choices=[k.lower()
                 for k in custom_logger.logLevels_strk.keys()
                 if not k == 'NOTSET'],
        help='Decide which loglevel should be applied to all modules')

    arg_parser.add_argument(
        # Json flag for outputting JSON in stdout (mainly used for the UI)
        '-j',
        '--json',
        action='store_true',
        dest='json_output',
        help='If the flag is NOT set (default), outputs report on stdout. ' +
             'If -j is set the output format becomes json.')

    arg_parser.add_argument(
        # Owner of the files that need to be processed
        '-o',
        '--owner',
        action='store',
        dest='unix_name', metavar="FILE_OWNER",
        help='By default, will consider all files belonging to all owners. ' +
             'If set, will consider only files belonging to FILE_OWNER')

    remove_helpers_dir = \
        non_admin_subparsers.add_parser(
            'rm-template-dirs',
            help='[ADMIN] Remove variable sublevels template directories.')

    remove_helpers_dir.add_argument(
        '-w',
        '--warehouse',
        action='store_true',
        dest='rm_warehouse_opt',
        default=False,
        help='Remove variable sublevels template directories in warehouse.')

    remove_helpers_dir.add_argument(
        '-c',
        '--clone',
        action='store_true',
        dest='rm_clone_opt', default=False,
        help='Remove variable sublevels template directories in clone.')
    remove_helpers_dir.set_defaults(call_func=remove_template_directories)

    for check_parser in [parent_check_clone, parent_check_warehouse]:
        check_parser.add_argument(
            '--clean',
            action='store_true',
            dest='clean_structure',
            default=False,
            help='[ADMIN] Clean identified invalid folders.')

    create_warehouse_parse = \
        non_admin_subparsers.add_parser(
            'create-warehouse',
            help='[ADMIN] Create datawarehouse structure from scratch using ' +
                 'warehouse_config.ini.')

    create_warehouse_parse.add_argument(
        '-d',
        '--template-dirs',
        action='store_true',
        dest='bool_add_opt_repos',
        default=False,
        help='Build optional template directories based on configuration ' +
             'sublevels.')

    create_warehouse_parse.set_defaults(call_func=create_structure)

    generate_clone_parse = \
        non_admin_subparsers.add_parser(
            'generate-clone',
            help='[ADMIN] Update/Create clone from scratch using the ' +
                 'existing warehouse.')

    generate_clone_parse.add_argument(
        '-r',
        '--conf-repos',
        action='store_true',
        dest='bool_add_conf_repos',
        default=False,
        help='Build optional template directories based on configuration ' +
             'sublevels.')
    generate_clone_parse.add_argument(
        '-d',
        '--template-dirs',
        action='store_true',
        dest='bool_add_opt_repos',
        default=False,
        help='Build optional template directories which exist in the ' +
             'warehouse.')

    generate_clone_parse.set_defaults(call_func=create_structure)

    transfer_parse = \
        non_admin_subparsers.add_parser(
            'transfer-files',
            help='[ADMIN] Transfer data from the clone to the warehouse.')

    transfer_parse.add_argument(
        '-u',
        '--update',
        action='store_true',
        dest='bool_update_version',
        help='Allow the transfer of new versions of files. If -u is set,' +
             ' the new version of a file will be transfered after the' +
             ' previous version has been saved in a "filename.old" folder,' +
             ' created alongside the new version at transfer destination.')

    transfer_parse.add_argument(
        # syntaxic switch there --no-rm sets required_removal to false
        '--no-rm',
        action='store_false',
        dest='required_removal',
        default=True,
        help='Allow the transfer of files to the warehouse without ' +
             'requiring their removal from clone. If --no-rm is set ' +
             'the transfer of a file is possible even if it cannot be ' +
             'removed from clone, if --no-rm is not set then it will ' +
             'produce a file related error if the file (UNIX) rights does ' +
             'not have write rights for the group [OR eventually if the ' +
             'executing user does not belong to the group]')

    transfer_parse.add_argument(
        '-d',
        '--dry-run',
        action='store_true',
        dest='dry_run',
        help='Fake file transfer, just gives the output')

    transfer_parse.set_defaults(call_func=transfer_files)
    parent_check_clone.set_defaults(call_func=check_clone)
    parent_check_warehouse.set_defaults(call_func=check_warehouse)
    return arg_parser



# Main
########

def main(argv=None):
    # print(parent_add_tmplt_dirs.call_func)
    parser_arg, non_admin_subparsers,               \
        check_clone, check_wh =    \
        datawarehouse_manager.init_non_admin_args_parser()

    datawarehouse_manager.init_templt_dirs_args_parser(
        non_admin_subparsers,
        add_template_directories,
        admin=True)

    parser_arg = init_admin_args_parser(parser_arg, non_admin_subparsers,
                                        check_clone, check_wh)

    kwarguments = {}
    if (len(sys.argv) < 1): parser_arg.parse_args(['-h'])  # Help that guy!
    lab_warehouse_args = parser_arg.parse_args(sys.argv[1:])
    kwarguments = vars(lab_warehouse_args)
    json_output = {
        "INFO": kwarguments,
        "ERROR": {},
        "WARN": {},
        "LOG": {},
    }
    kwarguments.update({'is_admin': True})
    main_func = kwarguments.pop('call_func', None)

    if main_func is None:
        logger.info("Nothing to do. Throwing help -h")
        parser_arg.parse_args(['-h'])  # Help that guy!

    loglevel = kwarguments.get("log_level")
    json = kwarguments.get("json_output", False)
    init_logger(loglevel, json)
    datawarehouse_manager.init_logger(loglevel, json, is_admin=True)
    datawarehouse_manager.init_logging_modules(loglevel, json, is_admin=True)
    logger.info("[init] datawarehouse_admin ({0})"
                .format(kwarguments.get("subparser")))

    config = datawarehouse_manager.get_config(kwarguments, json_output, json)

    adm_user = utils.get_executing_username()
    logger.debug("User: {0}".format(adm_user))
    kwarguments.update({"exec_user": adm_user})

    auth_groups = datawarehouse_manager.get_auth_groups(config)
    admin_granted = utils.grant_user_access(adm_user, auth_groups)

    # file_owner = kwarguments.get("unix_name", '')
    # owner_granted = utils.grant_user_access(file_owner, auth_groups)
    if admin_granted is True:  # and (owner_granted or not file_owner):
        conf_wroot = \
            conf_parser.get_config_values_as_string("main_datawarehouse",
                                                    "root",
                                                    config)
        conf_croot = \
            conf_parser.get_config_values_as_string("main_datawarehouse",
                                                    "clone_root",
                                                    config)
        try:
            warehouse_root, clone_root = \
                datawarehouse_manager.get_structures_roots(conf_wroot,
                                                           conf_croot,
                                                           kwarguments)
            logger.debug(warehouse_root + " " + clone_root)
            datawarehouse_manager.roots_are_different(warehouse_root,
                                                      clone_root)
        except ValueError as valerr_stroots:
            return datawarehouse_manager.exit_dump(valerr_stroots,
                                                   json_output, json)

        else:
            logger.info("{}: {}".format("warehouse root", warehouse_root))
            logger.info("{:.>14}: {}".format("clone root", clone_root))

    else:
        return \
            datawarehouse_manager.exit_dump(
                "{} does not belong to an authorised group.".format(adm_user),
                json_output, json)

    warehouse = DatawarehouseStructure.DatawarehouseStructure("warehouse",
                                                              warehouse_root,
                                                              config)
    clone = DatawarehouseStructure.DatawarehouseStructure("clone",
                                                          clone_root,
                                                          config)

    try:
        main_func(warehouse, clone, config, kwarguments, json_output)
    except ArgumentError as main_err:
        return datawarehouse_manager.exit_dump(main_err, json_output, json)
    else:
        if json: output_formatter.dump_json_to_stdout(json_output)

if __name__ == "__main__":
    sys.exit(main())

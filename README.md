# Lab Storage Warehouse

This software aims at giving the ability to a laboratory to organise its data in a well thought out and structured, yet flexible, folder framework. It enables deployment and maintenance of a stable and accessible data repository, or *datawarehouse*, to store immutable datasets.

## Project overview

The *datawarehouse* is a __protected__ collection of datasets that have to be *immutable*. What is referred to here as *immutable data* is the final output data of more or less complex analysis pipelines. It is final because it is not going to change and become outdated. *Immutable data* is __published__ or in a __stable state__ and therefore can be taken as a reference by others. This type of data is typically used on a regular basis by members of a lab, *e.g.* to perform complementary analysis or for visual comparison purposes with newly produced datasets.

![assets/docs/overview_structure.png](assets/docs/overview_structure.png)

One of the goal is to improve lab members interaction with the data previously generated and published by the lab by:

1. Giving them more autonomy when it comes to find and browse the data (folder framework)
2. Allowing them to have a better intuitive understanding what the data is  (structured hierarchy)
3. Furnishing a controlled set of detailed annotations for each dataset (mandtory controlled annotations)

In addition, when such data storage framework is not available, it is often that the same input dataset gets duplicated by different people and for different purposes, ultimately leading to a significant loss of storage space as well as an unclear trace back of files. A datawarehouse allows to access data in a more efficient way that saves both time and storage space.

## Folder hierarchy

The folder __hierarchy__ was though out carefully to have an intrinsic descriptive overview of the data exposed when browsing. By reaching a hierarchy endpoint, the user will already have a clear idea of the type of data he has found. For example, the structure describes the organism the data was produced, as well as the type of experimental techniques that was used to produce the data, see example

### Single dataset storage

~~~bash
└── my_lab
    └── dmelanogaster  # organism
        └── sequencing
            └── ChIP-seq  # experimental technique
                └── mesoderm
                    ├── H3
                    │   └── 6-8h
                    │       └── signal
                    │           └── dm3
                    └── K27Ac
                        └── 6-8h
                            ├── peaks
                            │   ├── dm3
                            │   └── dm6
                            └── signal
                                └── dm3
                                      └── [data]
~~~


The whole structure is built and hierarchised around key intrinsic datasets properties *e.g.* 
1.the __data origin__ (*my_lab*), 
2.the __organism__ from which the considered sample, at the origin of the data, was extracted (*d. melanogaster*, *d. virilis* ...), 
3.the __experimental assay__ (*ChIP-seq*, *ChIP-chip*...), 
4.the __entity targeted__ by the protocol (Transcription factor, chromatin mark *K27Ac*...), etc.


This structure is suited to store single datasets at an endpoint, or very similar types of dataset. It is less suitable to store data that was produced merging , it is not possible to store in the same structure data derived from 2 or more datasets (*e.g.* data that would contain different timepoint information). 

__NB:__ Here the folders *mesoderm* (the *tissue*), *Histone 3* - *H3* - or *K27Ac (the *targeted entity*) and *6-8h* (the *condition*) are all folders that are prone to vary a lot, relatively to folders like sequencing which is a technique that will be use for the years to come in genomics. Folders prone to a lot of change don't have a specific value bound to them in the configuration and thus can be modified by the user. See the configuration section


### Features, data derived from 2 datasets or more

Data derived from 2 or more datasets describe genomic features at the organism level and will be stored in a dedicated folder.

~~~bash
└── my_lab
    └── dmelanogaster 
        └── features
            └── crms
                └── Insulators_ChIP-chip
                    └── dm6
                        └── [data]
~~~

Here, the taken example is cis-regulatory modules (*crms*). CRMs are *genomic features* at the *organism level* that are derived from ChIP-chip data produced on more than one insulator protein. The above structure example shows where such data will be stored.

--------






## Hierarchy configuration

This hierarchy is defined by structuring rules found in a configuration file. Top and bottom layers of the hierarchy are invariable (Top: __origin__ :: __organism__ :: __platform__ :: __experimental assay__; Bottom: __data type__ :: __buildversion__), that is strictly limited by configuration rules (defined vocabulary and architecture). On the other hand, the intermediate layers located between the top and bottom of the structure are variable, that is, the user can build a set of sub-layers with their own keyword. Only the number of sub-layers is controlled using patterns defined in the configuration. Patterns vary from a combination of *platform* and *experimental assay* to another.

### Users vs. Admin

Part of the structure is meant to be expanded by the user himself to fit his needs. Part can be modified only by the admin. The definitions of what can the user, or the admin, modify and what not, is mostly based on empirical observations. 

![assets/docs/users_vs_admin_folders_modif.png](assets/docs/users_vs_admin_folders_modif.png)

For example, sequencing is the main experimental area in genomics today (with microarrays), will still be used in the years to come and is not expected to change soon. 

In the other hand, experimental targets and conditions do vary for each project, *e.g.* experiments can target *Histone 3* as well as *K27Ac*, but maybe also newly discovered DNA bound protein tomorrow. A condition might be a timepoint, like *6-8h* but might as well be a drug treatment with a drug which is yet unknown.

#### Admin configuration

Here, the structure related part of the configuration will be described and explained. This is a documented snippet of the configuration file example available in this repository: [lab_warehouse/lab_storage_warehouse/config/config-files/datawarehouse-conf-complete-example.ini](lab_warehouse/lab_storage_warehouse/config/config-files/datawarehouse-conf-complete-example.ini)


##### Main Structure

~~~
[structure]

### A datawarehouse is constituted with 2 main repositories. An internal and an external one.
### The internal repository is meant to be use for storing data that are produced in your lab.
### The external repository is meant to be used for storing data that are produced outside your lab,
### and that you're staff has been conscientiously accumulating thoughout years, downloading different
### copies of different versions, from different sources and storing each of those in a closed folder with no
### annotation.

repositories = internal_data, external_data

### The external data have been downloaded from somewhere (e.g. from one of the famous biology database, from a paper)

external_data.origins = modENCODE,REDfly,FlyBase,UCSC,Thomas_et_al

### People need to store both different types of datasets and feature dataset. The main difference between
### both datasets and features is that, in your warehouse, datasets are stored in a single sample based folder,
### which means one path leads to a specific dataset - or a group of very close datasets - that all share very
### similar characteristics. On the other hand the "features" folder allows one to store datasets that have been
### obtained through meta-analysis of many samples together.

datatypes = features,sequencing,microarrays

### Each type of data have been obtained using very different procedures
### For feature, one would name each folder with the common name to refer to a common type of analysis
features.exptypes=crms,genome_annotations,count_tables,diff_expression,diff_binding,structural_variants

### For sequencing or other, it's definitely going to depend on the different protocols and their variaction
sequencing.exptypes=ChIP-seq,DNase-seq,FAIRE-seq,4C-seq,Hi-seq,DNA-seq,RNA-seq,TAG-seq,CAGE-seq
microarrays.exptypes=ChIP-chip

### The model organism from which the ***sample has been obtained*** (vertebrates, mammals or cell strains)
organisms = dmelanogaster,dvirilis

### For each of the model organism or other, see note below, the genome build version.
### Here are used as example common name to refer to main Drosophila melanogaster genome releases
organisms.dmelanogaster.buildversions=dm2,dm3,dm6
organisms.dvirilis.buildversions=drovir3

### Important note: The build version refers to the genome release build version on which (in the case of a typical
### sequencing experiment) your reads have been aligned to. For example, if you obtain your biological sample from
### e.g. a primate, *** but *** aligned those raw reads to the human genome the organism will be Primate (or more
### precise...), however the build version related to the dataset will be e.g. hg19, hg38 (common names for human
### genome releases)
~~~


##### Assay-Specific Structure


Further description single experimental assays repository are needed. 

Specifically, the type of features described by a dataset will be different depending on the performed experimental assay (for example, ChIP-seq experiment often lead to *peak calling* and thus the generation of *peak* files, which can be formatted as bed files or other derived format).

Describing expected output data types (*signal*, *peaks*, etc.) and formats (*bed*, *gff*, etc.) allows for standardisation of results and improves data sharing, avoiding cryptic formats that are tool or user specific.

The configuration describing the expected outcomes of each assay is overviewed below.

~~~bash

###### SEQUENCING ######
[DNA-seq]
sequencing.DNA-seq.featuretypes=alignment
sequencing.DNA-seq.alignment.formats=bam
sequencing.DNA-seq.sublevels=__technology__/__all__

[ChIP-seq]
sequencing.ChIP-seq.featuretypes=peaks,signal,alignment
sequencing.ChIP-seq.peaks.formats=gff,bed,broadPeak,narrowPeak
sequencing.ChIP-seq.signal.formats=bigwig,bw,sgr,wig
sequencing.ChIP-seq.alignment.formats=bam
sequencing.ChIP-seq.sublevels=__tissue__/__targeted_entity__/__condition__

###### FEATURES ######
[features]
features.exptypes.sublevels=__targeted_entity__,__origin__/__subgroups__,__project__/__subgroups__,__exp__/__subgroups__
features.exptypes.formats=txt,csv,bed,broadPeak,narrowPeak,gff,fasta,fa

~~~


##### Last Layer

The last layer is the __build version__ of the considered genome. It is one of all the genome build versions available in the configuration, at the *structure* section:


#### User layers

The user can expand the structure to fit his needs, but only in a defined set of layers that are refereed to as sublevels. 

The configuration for sublevels is just placeholders that describe an expected structure skeleton. The user can create their own directory in place of these placeholders and name those directories with free text.

--------

### Write protected structure

The warehouse is a read-only/write-protected structure for everyone else than the administrator user (files are owned by the admin and permissions are set so that other users can only read files), preventing the unwanted modification of datasets or structure by users. 


![assets/docs/dw_useradddata.png](assets/docs/dw_useradddata.png)

--------------------------------------------------------------------------- 
TODO IMPROVE THIS IMAGE 

---------------------------------------------------------------------------


The lab warehouse package allows users to add their files and metadata to a clone of the warehouse. This clone is the folder hierarchy backbone without data files - except during the short process of transferring files to the warehouse.

All users have the write access in that clone structure, therefore can add to it, files they want to transfer. 

This software provides administration tools in the form of a command line interface, administration tools include deploying a warehouse and its clone(s) as well as transferring the files from the clone to the actual warehouse. The files are are transfered only if a set of condition is met, *e.g.* validity of the user created folder structure, presence and validity of annotations, etc.

-------------

### Controlled Mandatory Annotations

Files annotations (*i.e.* metadata) are essential to keep track of the data about the data. The more the merrier.

Metadata can for example be appropriate file naming (which the user has full control on). Here, the metadata is stored as an plain text file file with the same file name as the file it annotates and the «*.info*» extension. It is located in the same folder, and appears right below it when files are alphabetically sorted.


The format itself is an human-readable alphabetically sorted collection of *key=value* pairs, 1 per line, that describe intrinsic file properties, as shown in the following info file example.

*H3_G-H_6-8h_rpgc_Bonn2012.bigwig.info*
~~~
analysis_info=Reads aligned with bowtie. HistoneChIPseq R package used to correct samples to the mappable genome size (135 Mb), generating RPGC - Read Per Genomic Coverage - scores. Merged replicates G and H.
bin_size=50
buildversion=dm3
condition=6-8h
description=Genomewide mapping of D. melanogaster H3 Histone3 modifications in mesoderm during embryonic development at 6-8h after egg-laying.
display_name=H3 G-H 6-8h rpgc Bonn2012
experiment_type=ChIP-seq
normalization=background-NormDiff
organism=dmelanogaster
owner_name=bonn
pubmed_id=22231485
score_type=rpgc
signal_type=raw
status=published
targeted_entity=H3
tissue=mesoderm
~~~

This metadata is mandatory because it is part of the check that will allow file transfer if passed.

Each of these key value is validated against a regular expression (RE), i.e:
* The *description* of the dataset is a free text field wrote by the file owner and validated against the free text RE `re(^.+$)`
* The *analysis_info* field is also a free text field, meant to be more technical about the processing the dataset went through,
* The *score_type* is validated against a stringent RE, defining a list of accepted values, which is i.e. for a sequencing __signal__ file `re(^(rpm|rpgc|rpkm|fpkm|scaled-counts|raw-counts|pvalue|qvalue|fdr|enrichment|log2ratio)$)` and for a sequencing __peak__ file `re(^(pvalue|qvalue|fdr|enrichment|log2ratio)$)`.

The combination of structural information, file naming and metadata allows the precise identification of the lab data through years and by anyone.

#### Selection of Relevant Annotations

As part of the software configuration is a file that describes rules to decide about the relevant metadata for each file. Theses rules are stored in a plain text tab delimited file that can easily be expanded by the administrator, see [lab_warehouse/lab_storage_warehouse/config/metadata-rules/warehouse_metadata_infofile.txt](lab_warehouse/lab_storage_warehouse/config/metadata-rules/warehouse_metadata_infofile.txt)


A rule is built for each attribute considering 4 key intrinsic aspect of a given file, that is the main property describing a given file. The 4 considered aspect are:
1. the main family of experiment to which this file belongs (*e.g.* sequencing, microarrays)
2. the experimental assay that lead to the production of this file (*e.g* ChIP-seq, RNA-seq)
3. the type of feature the file describes (*e.g.* alignment, signal, peaks)
4. the format used to store the described features (*e.g.* bam, bigwig, bed)

Rules are ranked by order of importance, and the implemented logic is to take the first applying rule. This means that different rules can be defined for the same metadata attribute key. The file also contains additional information to flag a rule as mandatory and give it a validation regex


Using this organised set of rules allows to programmatically identify mandatory annotation for each type of file stored in the warehouse


| attribute | platform | exp_type | data_type | file_format mandatory | validation |
| --- | --- | --- | --- | --- | --- |
| description   | * | * | * | * | 1 | ^.+$ |
| analysis_info | * | * | * | * | 1 | ^.+$ |

These two rules apply for all platforms, all experimental assays, all types of described features and all formats, as well as flagged as mandatory. This means they will be requested for each and every file added to the warehouse. They are free text fields validated by this regular expression ^.+$ matching at least 1 character or more.

An other example

| attribute | platform | exp_type | data_type | file_format mandatory | validation |
| --- | --- | --- | --- | --- | --- |
| duplicates_removal | sequencing | * | alignment | bam | 1 | ^(yes|no)$ |
| unmapped_removal | sequencing | * | alignment | bam | 1 | ^(yes|no)$ |

Those are two mandatory flag rules (validated by yes or no) that are mandatory for alignment files in the bam format produced by sequencing. It describe two of the most common bam files modification, that is: 
1. the removal of sequencing duplicates
2. the removal of non aligned reads

Other rules can be defined as conditionally mandatory for example 

| attribute | platform | exp_type | data_type | file_format mandatory | validation |
| --- | --- | --- | --- | --- | --- |
| status | * | * | * | * | 1 | ^(published|unpublished)$ |
| pubmed_id | * | * | * | * | status=published | ^[0-9]+$ |

The pubmed id is requested for all only when the file is containing published data.

Those files are programmatically created by the software and in each created info file, a set of applying key=value pairs are added (the programmatically added value is either deducted from the folder structure or is validation regular expression). The user adding the data has to fill up those files.


#### Filling up metadata

Once the files have been added in the clone, one can create the metadata files using one of the software subcommand (create-infofiles). This command outputs a table that can be filled up by the user and then furnished as an input of the same command. See #create-infofiles for detailed information



## Package Description

The Lab Storage Warehouse package is composed by two main python modules: the manager module (`datawarehouse_manager`) and the admin module (`datawarehouse_admin`). Both modules propose a command line interface to manage all aspect of a warehouse, from its creation to the transfer of files. The `datawarehouse_admin` module aims at being used only by the administrator, it contains functions that allow datawarehouse creation from scratch (`create-warehouse`), the generation of a datawarehouse « clone » as a copy of the warehouse folder structure (`generate-clone`), or file transfer from the clone to the warehouse. The datawarehouse_manager modules contains functions that are made for the user to interact with the warehouse - or more specifically, mainly with the warehouse clone, e.g. `create-infofiles` to create preformated metadata files to ease data annotation. All of those command can be called with `-h` to get help and optional parameters information.

### Create the datawarehouse

[Admin] Create a datawarehouse based on the information given in the configuration file

Command line call is `datawarehouse_admin -c <config.ini> [--warehouse PATH]create-warehouse`

The root folder of the datawarehouse is fetch from within the ini config file indicated with the *requested* positional argument `-c`/`--configuration`, or fetched from the command line when using the positional argument `--warehouse`, in which case it overwrite the warehouse root path from the config file.

It creates a structure based on the structural and assays information in the configuration file. Additionally, the user can add template directories (layers with leading and trailing double underscore *e.g.* «\_\_template_dir\_\_»), using the `-d`/`--template-dirs` argument. Templates directories show the structure that can be modified by the user to add sublevels that fit their needs.

### Generate (update) a clone

[Admin] Generate **a** warehouse clone, that is, the exact copy of the warehouse folder structure (but empty of files)

Command line call is `datawarehouse_admin -c <config.ini> [--clone PATH] generate-clone [-h]`, get help and optional parameters information using `-h`

Create the clone as a copy of the datawarehouse. The warehouse should be created beforehand. The clone root is fetch in a similar way then the warehouse root (**i.e.** from the config file or from the command line, using `--clone PATH` )


### Create preformated file metadata

[Manager] Create preformated metadata text files next to data files located in the clone

Command line call is `datawarehouse_manager -c <config.ini> create-infofiles`

Once user added datasets to the clone, associated metadata can be created using the create-infofiles function. This also delivers informative output through stdout about the info files and potential errors related to them. Errors can appear if *e.g.* a mandatory key=value pair is missing in an info file or the value associated to a key is not validated by the validation regular expression. Each error for each file is reported to the user in an easy to read table.

### Files transfer

[Admin] Transfer files and their metadata from the clone to the warehouse if all checks are passed

Command line call is `datawarehouse_manager -c <config.ini> transfer-files`

Once datasets and metadata are in the clone, files can be transfered. Every file where no check yields an error is transfered. An error can occur if for example, the file format (file extension) is not allowed at this location in the warehouse (*e.g.* a peak file in at a location where only signal files can be stored) or if the associated metadata (**e.g.** missing mandatory key=values, values not validated by its validation regex). Errors related to datasets are reported in stdout, in a similar way than `create-infofile`. In addition, this function delivers the output from `create-infofiles` if errors have been found when validation metadata. 


## Workflow

### Adding file

User would drop files in one of the existing location in the clone, that's it!
If such location does not exist because associated sub-levels are missing, it can be created in the clone, referring to the available sublevel pattern.
If it misses *e.g.* the repository (for *e.g.* a file related to a new external database) or the organism, the user would have to contact to admin to modify the configuration file.

As a concrete example a «__Genomewide mapping of D. melanogaster Histone3 modifications in mesoderm during embryonic development at 6-8h after egg-laying.__», a bigwig signal file from the Furlong laboratory will go to the structure example shown in the Project overview section (__furlong_data/dmelanogaster/sequencing/ChIP-seq/mesoderm/H3/6-8h/[...]__).
Once the location is created, user drop the file there.

* Create missing directories in the appropriate experimental assay layer. Better than doing it from scratch, user should copy/paste an existing layer as following (assuming *__furlong_data/dmelanogaster/sequencing/ChIP-seq/\_\_tissue\_\_/\_\_targeted_entity\_\_/\_\_condition\_\_/[...] __* exists because admin called `$ datawarehouse_admin generate-clone -r` -- and signal is a known featuretype in the ChIP-seq section -- and dm3 id a known build version for drosophila melanogaster) :

`$ cd <DATAWAREHOUSE_ROOT>`

`$ cp -r furlong_data/dmelanogaster/sequencing/ChIP-seq/__tissue__ furlong_data/dmelanogaster/sequencing/ChIP-seq/mesoderm`

`$ mv mesoderm/__targeted_entity__ mesoderm/K4me1`

`$ mv mesoderm/K4me1/__condition__ mesoderm/K4me1/6-8h`

* Drop the __signal bigwig__ file to *<DATAWAREHOUSE_ROOT>__/furlong_data/dmelanogaster/sequencing/ChIP-seq/mesoderm/K4me1/6-8h/signal/dm3__*


<img src="docs/media/drag_and_drop_file_to_warehouse.png" title="Drag and drop a file to the datawarehouse clone" alt="drag and drop file to warehouse clone" height="300"/>


### Create the metadata

Once the user added files to the warehouse, he needs to give the associated metadata. One metadata file is created by dataset. To ease this process, user can use the following command:

`$ datawarehouse_manager create-infofiles`

As a result, on infofile is created per dataset. The infofile is a plain text file located alongside the dataset, it has the exact same name than the data file, including the extension and an extra ".info" extension.

In the files created programmatically, values are already indicated either with the actual value of a field when it can be identified from the structure (*e.g.* buildversion, experiment_type) or the file itself (*e.g.* "owner_name"). Other fields are filled with the regular expression used for value validation. This regular expression indicates either a free text field, or a list of keyword to be used (*e.g.* *description* : a short *free text* description of what the file is, *status*: a value that can be validated by regex `^(published|unpublished)$`, so either the *published* keyword or the *unpublished* one).

Some info file attribute are conditional, which means it make sense to indicate them if another attribute is set to its truthy value. As a concrete example, the pubmed_id of the publication has to be in the metadata if the file is part of the data of a published work. If it is not published, the pubmed_id line can be remove from the medatada

Considering the example from last section, we would have:

~~~
[...]
buildversion=dm6
[...]
description=^.+$
[...]
pubmed_id=^[0-9]+$
[...]
status=^(published|unpublished)$
[...]
~~~

Should be filled by user as *e.g.*:

~~~
[...]
buildversion=dm6
[...]
description=Genomewide mapping of D. melanogaster K4me1 Histone3 modifications in mesoderm during embryonic development at 6-8h after egg-laying.
[...]
status=unpublished
[...]
~~~


### Transfer files

Once the file and metadata are present, administrator has to execute the following command:

`$ datawarehouse_admin transfer-files`

The script will process the information in order to identify if a file is suitable for transfer, and transfer it if so. Reports come out the stdout (terminal) to report transfered files and remaining errors.

### Update a file

A basic versioning system has been implemented to take care of file update. That is: if a user tries to transfer a file with the exact same name and location to the warehouse, the existing version will be previously saved within the warehouse in a **.old** folder created in the folder where the file resides.
The default behavior of the datawarehouse_manager is to not allow updates, but instead report errors for files that already exist in the warehouse. In order to allow update of datasets, the user has to add the __-u__ flag to the transfer-files command line argument, as so:

`$ datawarehouse_admin transfer-files -u`
or 
`$ datawarehouse_admin transfer-files --update`

The info file has to contain an "update_comment" in such situation.



## Package management

### Package install

- Download the source code.

- Unzip it and move to the inner directory where __setup.py__ is located. 

- Run pip to install package requirements with the available requirement file (Here, it might be good to use a dedicated virtual environment), take a look at [python's virtualenv package](https://virtualenv.pypa.io/en/latest/ "python's virtualenv package") or the anaconda/miniconda project)

~~~
pip install -r requirements.txt 
~~~

- Run the setup command, make sure to run the setup script with a version of python that is __>=2.7__.

~~~
python setup.py install [--user]
~~~
The __--user__ flag install dependencies locally to ~/.local/lib/ and the executable datawarehouse_manager to ~/.local/bin/. If you wish to install the package globally, remove the __--user__ flag

- Check if nothing went wrong by launching the datawarehouse manager help.

~~~
datawarehouse_manager
~~~
or 
~~~
~/.local/bin/datawarehouse_manager
~~~


### Package removal

- Run the install command with file recording to extract a list of all the files installed by this package 

```
python setup.py install --record installfiles.txt
```

- Remove all the installed files

```
cat installfiles.txt | xargs rm -f
```

:warning: This command is not extremely safe as it will not work as expected on files with spaces, see [https://stackoverflow.com/a/25209129](https://stackoverflow.com/a/25209129)

### Updates

When the core code is updated, one needs to run the install script again after activating the virtual envrionment

```
source activate <VIRTUAL_ENV>
python setup.py install
```



Authors
======================

Matthias Monfort,  
Furlong group - EMBL, Heidelberg  
mail1 : monfort@embl.de  
mail2 : monfort.matthias@gmail.com  

Charles Girardot  
European Molecular Biology Laboratory, Heidelberg  
mail1: charles.girardot@embl.de  
web : http://gbcs.embl.de
